# Living Memories

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). 

## Tech stack
The stack we used consists of the following items:
1. MERN (M is MySQL in this case)
2. Plesk + Vultr for the hosting and database (PhpMyAdmin)


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

***IMPORTANT: when starting up localhost:3000, it's also important to start the backend (which will be on localhost:8080). The script is the same, but it will have to be done after going into the 'API' folder. Obviously, if the backend isn't started, the routes to the database will not work.***

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test` 

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Auth0 

In order to understand how the auth0 was fully implemented, check this link which we also followed to make it work in our project: [https://auth0.com/blog/complete-guide-to-react-user-authentication/](https://auth0.com/blog/complete-guide-to-react-user-authentication/).

In short, we started off by creating a separate folder `src/auth` which contains the file `auth0-provider-with-history`. After creating the needed script and component in this file, the `Auth0ProviderWithHistory` is used in `App.js`, which finally sets our project up with auth0.

After setting it up, we made a login and logout button which you can find in `src/utils`. The logout button (which is a function from useAuth0) is used in the file `utils/NavDropdown.js`.

### private routes 
In order to keep pages protected from logged out users, we created a file which you can find in`src/routers/PrivateRoute.js`. This is basically the same function you use when implementing `react-router` that makes sure that these protected routes can only be seen when logging in our site.

### protected api calls in the backend (NodeJS)
**Again, please check the link given above to see how we implemented the backend in detail.**
In order to make protected calls, we created a script which implements `JWT` for the access token. This is basically it when we're using api calls which have to be protected instead of public.
