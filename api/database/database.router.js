const express = require("express");
const { checkJwt } = require("../authz/check-jwt");
const db = require("../database/db");

const dbRouter = express.Router();

dbRouter.get('/protected', checkJwt, (req, res) => {
  res.send('This is a protected message!');
});

dbRouter.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

module.exports = {
  dbRouter,
};