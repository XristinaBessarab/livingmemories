import React from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';
import ScrollToTop from './routers/ScrollToTop';
import Spinner from '../src/components/Spinner';
import ContactPage from './pages/ContactPage';
import HomePage from './pages/HomePage';
import HerdenkingsPage from './pages/HerdenkingsPage';
import OnsVerhaalPage from './pages/OnsVerhaalPage';
import FAQPage from './pages/FAQPage';
import PrivateRoute from './routers/PrivateRoute';
import PrivacyBeleidPage from './pages/PrivacyBeleidPage';
import Auth0ProviderWithHistory from './auth/auth0-provider-with-history';
import MijnGegevensPage from './pages/MijnGegevensPage.js';
import AanmakenGegevens from './pages/AanmakenGegevens';
import UpdateGegevens from './pages/UpdateGegevens';
import CreateInvoicePage from './pages/CreateInvoicePage';
import UpdateInvoicePage from './pages/UpdateInvoicePage';
import PageMenu from "./components/pageMenu/PageMenu";
import MemorialPageForm1 from './pages/MemorialPageForm1';
import MemorialPageForm2 from './pages/MemorialPageForm2';
import MemorialPageForm3 from './pages/MemorialPageForm3';
import MemorialPageForm4 from './pages/MemorialPageForm4';
import MemorialPageUpdateForm from './pages/MemorialPageUpdateForm';
import AllMemorialPages from './pages/AllMemorialPages';
import MyMemorialPages from './pages/MyMemorialPages';
import DynamicForm from './components/herdenkingsPagina/updateForm/DynamicForm';
import ContributionsPage from './pages/ContributionsPage';
import AlgemeneVoorwaardenPage from './pages/AlgemeneVoorwaardenPage';
import SearchPage from "./pages/SearchPage";
import NotFound from './components/404';
import BegrafenisOndernemerPage from "./pages/BegrafenisOndernemerPage";
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.scss';

function App() {
  const { isLoading } = useAuth0();

  if (isLoading) <Spinner animation='border' />
  return (
    <div className="App">
      <Router>
        <Auth0ProviderWithHistory>
          <ScrollToTop />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/contact" component={ContactPage} />
            <Route path="/faq" component={FAQPage} />
            <Route path="/privacybeleid" component={PrivacyBeleidPage} />
            <Route path="/algemene-voorwaarden" component={AlgemeneVoorwaardenPage} />
            <Route path="/ons-verhaal" component={OnsVerhaalPage} />
            <Route path="/herdenkings/:Id" component={HerdenkingsPage} />
            <Route path="/herdenkingspaginas" component={AllMemorialPages} />
            <Route path="/search/:Search" component={SearchPage}/>
            <PrivateRoute path="/aanmaken-gegevens" component={AanmakenGegevens} />
            {/* <PrivateRoute path="/update-gegevens" component={UpdateGegevens} /> */}
            <PrivateRoute path="/mijn-gegevens" component={MijnGegevensPage} />
            {/* <PrivateRoute path="/aanmaken-factuur" component={CreateInvoicePage} /> */}
            {/* <PrivateRoute path="/update-factuur" component={UpdateInvoicePage} /> */}
            <PrivateRoute path="/mijn-paginas" component={PageMenu} />
            <PrivateRoute path="/memorialpage-form-1" component={MemorialPageForm1} />
            <PrivateRoute path="/memorialpage-form-2" component={MemorialPageForm2} />
            {/* <PrivateRoute path="/memorialpage-form-3" component={MemorialPageForm3} /> */}
            <PrivateRoute path="/memorialpage-form-4" component={MemorialPageForm4} />
            <PrivateRoute path="/wijzig-herdenkingspagina" component={MemorialPageUpdateForm} />
            <PrivateRoute path="/mijn-herdenkingspaginas" component={MyMemorialPages} />
            <PrivateRoute path="/wijzig-herdenkingspagina-apart" component={DynamicForm} />
            <Route path="/mijn-bijdragen" component={ContributionsPage} />
            <Route path="/begrafenisondernemer/:Id" component={BegrafenisOndernemerPage}/>
            <Route component={NotFound} />
          </Switch>
        </Auth0ProviderWithHistory>
      </Router>
    </div>
  );
}

export default App;
