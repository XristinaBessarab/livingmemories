import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

import '../styles/NavbarDropDown.css'

export default function LoginButton() {
  // foto
  const uitgelogd = 'https://lh3.googleusercontent.com/u/0/d/11uvSLfUmDZIJxU3lYVicHc2Uv0O05V8n=w1280-h885-iv1'

  //When logging in, auth0 redirects you to the homepage
  const { loginWithRedirect, isAuthenticated } = useAuth0();

  return (
    !isAuthenticated && (
      <div className="navbar-button-login">
        <button 
       className="" 
       onClick={() => loginWithRedirect()}> 
        <img 
          className="navbar-login" 
          src={uitgelogd} 
          alt="Uitgelogde gebruiker" 
        />
        Log in</button>
      </div>
    )
  );
}
