import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import Button from "react-bootstrap/Button";

export default function RegisterButton() {
  //When logging in, auth0 redirects you to the homepage
  const { loginWithRedirect, isAuthenticated } = useAuth0();

  return (
    !isAuthenticated && (
      <Button className='register_button' style={{ backgroundColor:"#faf3ec", color:'#335278',borderColor:'#faf3ec '}} onClick={() => loginWithRedirect({ screen_hint: 'signup' })}>Registreer</Button>
    )
  );
}
