import React, { useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import useSession from "../auth/set-session";
import { Link } from "react-router-dom";
import { Dropdown } from "react-bootstrap";
import app from "../helpers/axiosConfig";

import '../styles/NavbarDropDown.css';

export default function NavDropdown() {
  const [naam, setNaam] = useState("");

  //foto's
  const ingelogd = 'https://lh3.googleusercontent.com/u/0/d/1Ix35l0U-oW7-u8QH-jEArf0HxW5DLINR=w1280-h885-iv1';
  const userFoto = 'https://lh3.googleusercontent.com/u/0/d/17kBtclKUFTAOje04fn-CWrk1iaXS-W4C=w1280-h885-iv1';
  const profielAanmaken = 'https://lh3.googleusercontent.com/u/0/d/1rnha69V-AZZSOOxiSdmkopn-1Z2YLtVj=w1280-h885-iv1';
  const beheer = 'https://lh3.googleusercontent.com/u/0/d/1v3N-vsDsCe0d-PYQHEfuH342OEVoUjPZ=w1280-h885-iv1';
  const uitloggen = 'https://lh3.googleusercontent.com/u/0/d/1oJWp-9yNk4yUFn1_gZlW8mImEzFdah2D=w1280-h885-iv1';

  const { logout, isAuthenticated, user } = useAuth0();

  const [route, setRoute] = useState(true);
  const userId = useSession();

  // mobile 
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  //checks if user has already made data
  //if so, then 'aanmaken gegevens' will be removed from the dropdown
  useEffect(() => {
    (async () => {
      try {
        setRoute(true);
        if (userId !== null) {
          setRoute(false);
        }
      } catch {}
      if (user){
        let a = await app.get(`/get/auth/${user.sub}`)
        await setNaam(a.data.length > 0 ? a.data[0].FirstName : "")
      }
    })();
  }, [userId, route]);

  //Call this useEffect everytime the user object changes
  //This will set the state which is shown in the dropdown toggle
  useEffect(() =>{
    (async() =>{
      if (user){
        let a = await app.get(`/get/auth/${user.sub}`)
        setNaam(a.data.length > 0 ? a.data[0].FirstName: "")
      }
    })()
    // console.log("hier?")
  }, [user])

  return (
    isAuthenticated && (
      <div className="navbar-menu">
        {/* beheer pagina's button */}
        <Link
          to="/mijn-paginas"
          onClick={closeMobileMenu}
          className="text-decoration-none"
        >
          <div className="navbar-button-beheer-pagina">
            <img
              className=""
              src={beheer}
              alt="beheer pagina's"
            />
            <button>Beheer pagina's</button>
          </div>
        </Link>
        {/* welcome button */}
        <div className="container welcome-container ps-0 ">
          <Dropdown
            className="d-flex justify-content-end"
            focusFirstItemOnShow="true"
          >
            <div className="welcome-btn">
              <Dropdown.Toggle variant="none" className="welcome-username text-start pe-4 pt-2">
                <img 
                  src={ingelogd} 
                  alt="ingelogd"
                /> 
                Hallo {naam && naam }
              </Dropdown.Toggle>
            </div>
            <Dropdown.Menu className="drop-down-menu" >
              <Link to="/mijn-gegevens" className="justify-content-start navbar-link">
                <img
                  className="mx-1 text-left"
                  src={userFoto}
                  alt="logout"
                  width="23rem"
                />
                Profiel
              </Link>
              {route && (
                <Link to="/aanmaken-gegevens" className="justify-content-start navbar-link">
                  <img
                    className="mx-1 float-left"
                    src={profielAanmaken}
                    alt="gegevens aanmaken"
                    width="23rem"
                  />
                  Profiel aanmaken
                </Link>
              )}
              <Link to="/mijn-paginas" className="justify-content-start navbar-link">
                <img
                  className="mx-1 float-left text-left"
                  src={beheer}
                  alt="beheer pagina's"
                  width="23rem"
                />
                Beheer pagina's
              </Link>
              <Link
                onClick={() => {
                  localStorage.removeItem("@id");
                  logout({ returnTo: window.location.origin });
                }}
                className="justify-content-start navbar-link"
              >
                <img
                  className="mx-1 float-left text-left"
                  src={uitloggen}
                  alt="logout"
                  width="23rem"
                />
                Uitloggen
              </Link>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    )
  );
}