import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { Link } from "react-router-dom";
import "../styles/utils/pageMenuButton.scss";

export default function PageMenuButton() {
  //When logging in, auth0 redirects you to the homepage
  const { isAuthenticated } = useAuth0();

  return (
    isAuthenticated && (
      <div className="">
        <button className='btn btn-primary d-flex justify-content-center mt-1 ml-1' style={{}}>
          <Link to="/mijn-paginas" className="page-menu-link">
            <span className="text-light" style={{fontSize: 16}}>Mijn Pagina's</span>
          </Link>
        </button>
      </div>
    )
  );
}
