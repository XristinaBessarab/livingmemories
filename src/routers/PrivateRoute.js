import React from "react";
import { Route } from "react-router-dom";
import { withAuthenticationRequired } from "@auth0/auth0-react";
import Spinner from '../components/Spinner';

const PrivateRoute = ({ component, ...args }) => (
  //PrivateRoute = only visible when you are Authenticated
  <Route
    component={withAuthenticationRequired(component, {
      onRedirecting: () => <Spinner />,
    })}
    {...args}
  />
);

export default PrivateRoute;