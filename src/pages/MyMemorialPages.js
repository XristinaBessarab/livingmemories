import React from 'react'
import NavBar from '../components/NavBar';
import MemorialPages from '../components/myMemorialPages/MyMemorialPages';
import Footer from '../components/Footer';
import FooterCopyright from '../components/FooterCopyright';

import '../styles/form/formMemorial.scss';

export default function MyMemorialPages() {
  return (
    <div>
      <NavBar />
      <MemorialPages />
      <Footer />
      <FooterCopyright />
    </div>
  )
}
