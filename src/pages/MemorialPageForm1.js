import React from 'react'
import NavBar from '../components/NavBar';
import FormPage1 from '../components/herdenkingsPagina/FormPage1';
import FooterCopyright from '../components/FooterCopyright';
import Footer from '../components/Footer';

export default function MemorialPageForm1() {
  return (
    <div>
      <NavBar />
      <FormPage1 />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
