import React from 'react'
import FormPage3 from '../components/herdenkingsPagina/FormPage3';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

export default function MemorialPageForm3(props) {
  return (
    <div>
      <NavBar />
      <FormPage3 props={props}/>
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
