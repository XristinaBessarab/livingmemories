import React from 'react'
import Header from '../components/Header'
import MainContact from '../components/contactPage/MainContact'
import FooterCopyright from '../components/FooterCopyright'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer';
import bg from '../images/bg-lv.jpg';

export default function ContactPage() {

  const styling = {
    background: `linear-gradient( rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2) ), url(${bg})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  }


  return (
    <div>
      <NavBar />
      <Header styling={styling} height={'400px'} />
      <MainContact />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
