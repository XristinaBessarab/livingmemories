import React from 'react';
import UpdateGegevens from '../components/updateGegevens/UpdateGegevens';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';

export default function UpdateGegevensPage(props) {
  return (
    <div>
      <NavBar />
      <UpdateGegevens props={props}/>
      <FooterCopyright />
    </div>
  )
}
