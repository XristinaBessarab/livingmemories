import React from 'react';
import Header from '../components/Header';
import FooterCopyright from '../components/FooterCopyright';
import Footer from '../components/Footer';
import NavBar from '../components/NavBar';
import '../styles/HomePage.scss';
import bg from '../images/bg-lv.jpg';
import MainVerhaal from '../components/onsVerhaalPage/MainVerhaal';
import { Nav } from 'react-bootstrap';

export default function OnsVerhaalPage() {
  const styling = {
    background: `linear-gradient( rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2) ), url(${bg})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    
  }
  return (
    <div>
      <NavBar >
         <Nav.Link href="#over-ons">Over Ons</Nav.Link>
         <Nav.Link href="#getuigenissen">Getuigenissen</Nav.Link>
      </NavBar>
      <Header styling={styling} height={'400px'} />
      <MainVerhaal />
      <Footer />
      <FooterCopyright />
    </div>
  )
}
