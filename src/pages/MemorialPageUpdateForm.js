import React from 'react'
import UpdateForm from '../components/herdenkingsPagina/updateForm/UpdateForm';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';

export default function MemorialPageUpdateForm(props) {
  return (
    <div>
      <NavBar />
      <UpdateForm props={props}/>
      <FooterCopyright />
    </div>
  )
}
