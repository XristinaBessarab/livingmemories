import React from 'react';
import NavBar from '../components/NavBar';
import MainAlgemeneVoorwaarden from '../components/algemeneVoorwarden/MainAlgemeneVoorwaarden';
import Footer from '../components/Footer';
import FooterCopyright from '../components/FooterCopyright';

export default function AlgemeneVoorwaardenPage() {
  return (
    <div>
      <NavBar />
      <MainAlgemeneVoorwaarden />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
