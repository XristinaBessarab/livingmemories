import React from 'react';
import Search from '../components/SearchBarSearch';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

export default function SearchPage(props) {

  return (
    <div>
      <NavBar />
      <Search props={props} />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
