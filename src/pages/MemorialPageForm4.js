import React from 'react'
import FormPage4 from '../components/herdenkingsPagina/FormPage4';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

export default function MemorialPageForm4(props) {
  return (
    <div>
      <NavBar />
      <FormPage4 props={props}/>
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
