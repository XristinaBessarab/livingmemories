import React from 'react';
import FooterCopyright from '../components/FooterCopyright';
import MijnGegevens from '../components/mijnGegevens/MijnGegevens';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

export default function MijnGegevensPage() {
  return (
    <div>
      <NavBar />
      <MijnGegevens />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
