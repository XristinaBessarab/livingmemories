import React from 'react';
import { Nav } from 'react-bootstrap';
import NavBar from '../components/NavBar';
import Main from '../components/homePage/Main';
import Footer from '../components/Footer';
import FooterCopyright from '../components/FooterCopyright';

// import '../styles/HomePage.scss';

export default function HomePage() {
  //{children} in NavBar.js is used in order for the nav to receive additional components
  //3 Nav.Link's were added here in order to get them only to show up on the homepage
  return (
    <div>
      <NavBar>
        <Nav.Link href="#aan-de-slag">Aan de slag</Nav.Link>
        <Nav.Link href="#getuigenissen">Getuigenissen</Nav.Link>
        <Nav.Link href="#over-ons">Over Ons</Nav.Link>
        <Nav.Link href="#contact">Contact</Nav.Link>
      </NavBar>
      <Main />
      <Footer />
      <FooterCopyright />
    </div>
  )
}
