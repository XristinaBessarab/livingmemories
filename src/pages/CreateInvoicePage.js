import React from 'react';
import CreateInvoice from '../components/invoice/CreateInvoice';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';

export default function CreateInvoicePage() {
    return (
      <div>
        <NavBar />
        <CreateInvoice />
        <FooterCopyright />
      </div>
    )
  }