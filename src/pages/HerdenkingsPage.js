import React from 'react'
import FooterCopyright from '../components/FooterCopyright'
import MainHerdenkings from "../components/herdenkingsPagina/MainHerdenkings";
import NavBar from '../components/NavBar'
import Footer from '../components/Footer';

export default function HerdenkingsPage(props) {
  return (
    <div>
      <NavBar sticky='none' />
      <MainHerdenkings props={props}/>
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
