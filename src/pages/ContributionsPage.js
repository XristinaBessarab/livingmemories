import React from 'react'
import FooterCopyright from '../components/FooterCopyright';
import Contributions from '../components/contributions/Contributions';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer'
import "../styles/herdenking/Mainherdenkings.scss";

export default function ContributionsPage() {
  return (
    <div>
      <NavBar />
      <Contributions />
      <Footer /> 
      <FooterCopyright />
    </div>
  )
}
