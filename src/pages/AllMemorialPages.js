import React from 'react'
import NavBar from '../components/NavBar';
import MemorialPages from '../components/allMemorialPages/MemorialPages';
import Footer from '../components/Footer'
import FooterCopyright from '../components/FooterCopyright';

export default function AllMemorialPages() {
  return (
    <div>
      <NavBar />
      <MemorialPages />
      <Footer /> 
      <FooterCopyright />
    </div>
  )
}
