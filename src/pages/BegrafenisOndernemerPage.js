import React from 'react'
import NavBar from '../components/NavBar';
import Footer from '../components/Footer'
import FooterCopyright from '../components/FooterCopyright';
import BegrafenisondernemerPagina from '../components/begrafenisondernemer/BegrafenisondernemerPagina';

export default function BegrafenisOndernemerPage(props) {
  return (
    <div>
      <NavBar />
      <BegrafenisondernemerPagina props={props}/>
      <Footer /> 
      <FooterCopyright />
    </div>
  )
}
