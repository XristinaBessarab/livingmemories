import React from 'react'
import FormPage2 from '../components/herdenkingsPagina/FormPage2';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

export default function MemorialPageForm2(props) {
  return (
    <div>
      <NavBar />
      <FormPage2 props={props}/>
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
