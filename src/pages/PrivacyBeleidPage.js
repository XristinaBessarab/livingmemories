import React from 'react';
import NavBar from '../components/NavBar';
import MainPrivacyBeleidPage from '../components/privacyBeleidPage/MainPrivacyBeleid';
import Footer from '../components/Footer';
import FooterCopyright from '../components/FooterCopyright';
import '../styles/privacyBeleid/privacyBeleid.css';

export default function PrivacyBeleidPage() {
  return (
    <div>
      <NavBar className='navbar'/>
      <MainPrivacyBeleidPage />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}
