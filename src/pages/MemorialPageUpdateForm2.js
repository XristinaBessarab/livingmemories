import React from 'react'
import UpdateForm2 from '../components/herdenkingsPagina/updateForm/UpdateForm2';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';

export default function MemorialPageUpdateForm2(props) {
  return (
    <div>
      <NavBar />
      <UpdateForm2 props={props}/>
      <FooterCopyright />
    </div>
  )
}
