import React from 'react';
import UpdateInvoice from '../components/invoice/UpdateInvoice';
import FooterCopyright from '../components/FooterCopyright';
import NavBar from '../components/NavBar';

export default function UpdateInvoicePage() {
  return (
    <div>
      <NavBar />
      <UpdateInvoice />
      <FooterCopyright />  
    </div>
  )  
}