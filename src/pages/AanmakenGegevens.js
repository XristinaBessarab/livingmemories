import React from 'react';
import AanmakenGegevens from '../components/aanmakenGegevens/AanmakenGegevens';
import FooterCopyright from '../components/FooterCopyright';
import Footer from '../components/Footer';
import NavBar from '../components/NavBar';
import '../styles/form/form.scss';

export default function AanmakenGegevensPage() {
  return (
    <div>
      <NavBar />
      <AanmakenGegevens />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}