import React from 'react'
import NavBar from '../components/NavBar'
import MainFAQ from '../components/faqPage/MainFAQ'
import Footer from '../components/Footer'
import FooterCopyright from '../components/FooterCopyright'

export default function FAQPage() {
  return (
    <div>
      <NavBar />
      <MainFAQ />
      <Footer/>
      <FooterCopyright />
    </div>
  )
}