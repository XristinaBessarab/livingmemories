import { useState, useEffect } from "react";

import { useAuth0 } from "@auth0/auth0-react";
import app from "../helpers/axiosConfig";

export default function useSession() {
  const [userId, setUserId] = useState(null);

  const { isAuthenticated, getAccessTokenSilently, user } = useAuth0();
  useEffect(() => {
    (async () => {
      const authId = localStorage.getItem(`@auth0id`);
      const userId = localStorage.getItem(`@id`);
      if (authId && userId) {
        setUserId(parseInt(userId));
        return () => userId;
      }

      try {
        const token = await getAccessTokenSilently();
        const authId = user.sub;

        localStorage.setItem(`@auth0id`, authId);

        if (authId) {
          // let data = await app.get(`get/${authId}`, {
          //   headers: {
          //     Authorization: `Bearer ${token}`,
          //   },
          // });
          let data = await app.get(`get/auth/${authId}`);
          localStorage.setItem(`@id`, data.data[0].Id.toString());
          setUserId(parseInt(data.data[0].Id.toString()));
        }
      } catch {}

      return () => localStorage.getItem(`@id`);
    })();
  }, [getAccessTokenSilently, isAuthenticated]);
  return userId;
}
