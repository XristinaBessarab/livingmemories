import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import app from "../../helpers/axiosConfig";
import { useHistory } from "react-router-dom";
import Spinner from "../Spinner";
import useSession from "../../auth/set-session";

export default function UpdateInvoice() {
  const [isLoading, setIsLoading] = useState(false);
  const [buyerData, setBuyerData] = useState({});
  const history = useHistory();
  const { register, handleSubmit } = useForm();
  const userId = useSession();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Go back to dashboard
  const goBack = () => {
    history.goBack();
  };

  //Getting data from id
  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);
        if (userId) {
          const data = await app.get(`get/buyerdata/${userId}`);
          setBuyerData(data.data[0]);
          setIsLoading(false);
        }
      } catch {}
    })();
  }, [userId]);

  //On submit sending data to endpoint
  const onSubmit = (data) => {
    data.Id = buyerData.Id;
    app.put(`/put/invoice/${data.Id}`, data, axiosConfig);
    history.push("/");
  };

  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }
  return (
    <div className="form-wrapper">
      <form className="form-info" onSubmit={handleSubmit(onSubmit)}>
        <h1>Wijzig uw Bedrijfsgegevens</h1>
        <label>Bedrijfsnaam </label>
        <input
          type="text"
          defaultValue={buyerData.CompanyName}
          // name="CompanyName"
          // ref={register({ maxLength: 45 })}
          {...register('CompanyName', { maxLength: 45 })}
        />

        <label>Bedrijfsmail</label>
        <input
          type="text"
          defaultValue={buyerData.InvoiceMail}
          // name="InvoiceMail"
          // ref={register({ maxLength: 45 })}
          {...register('InvoiceMail', { maxLength: 45 })}
        />

        <label>BTW-nummer</label>
        <input
          type="text"
          defaultValue={buyerData.VAT_Number}
          // name="VAT_Number"
          // ref={register({ maxLength: 45 })}
          {...register('VAT_Number', { maxLength: 45 })}
        />

        <label>Woonplaats</label>
        <input
          type="text"
          defaultValue={buyerData.Residence}
          // name="Residence"
          // ref={register({ maxLength: 45 })}
          {...register('Residence', { maxLength: 45 })}
        />

        <label>Postcode</label>
        <input
          type="text"
          defaultValue={buyerData.ZipCode}
          // name="ZipCode"
          // ref={register({ maxLength: 10 })}
          {...register('ZipCode', { maxLength: 10 })}
        />

        <label>Straat</label>
        <input
          type="text"
          defaultValue={buyerData.StreetName}
          // name="StreetName"
          // ref={register({ maxLength: 45 })}
          {...register('StreetName', { maxLength: 45 })}
        />

        <label>Huisnummer</label>
        <input
          type="text"
          defaultValue={buyerData.HouseNumber}
          // name="HouseNumber"
          // ref={register({ maxLength: 10 })}
          {...register('HouseNumber', { maxLength: 10 })}
        />

        <label>Busnummer</label>
        <input
          type="text"
          defaultValue={buyerData.BusNumber}
          // name="BusNumber"
          // ref={register({ maxLength: 10 })}
          {...register('BusNumber',{ maxLength: 10 })}
        />

        <label>Land</label>
        <input
          type="text"
          defaultValue={buyerData.Country}
          // name="Country"
          // ref={register({ maxLength: 45 })}
          {...register('Country', { maxLength: 45 })}
        />

        <div className="button-group">
          <button onClick={goBack} className="cancel-btn">
            Annuleren
          </button>
          <button type="submit" className="submit-btn">
            Wijzigen
          </button>
        </div>
      </form>
    </div>
  );
}
