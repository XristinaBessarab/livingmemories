import React from "react";
import { useForm } from "react-hook-form";
import app from "../../helpers/axiosConfig";
import { useHistory } from "react-router-dom";
import useSession from "../../auth/set-session";

export default function CreateInvoice() {
  const history = useHistory();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //Getting user_id from sessionstorage and parsing to int
  const userId = useSession();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //On submit sending data to endpoint
  const onSubmit = (data) => {
    data.Users_Id = userId;
    app.post(`/post/invoice`, data, axiosConfig);
    history.push("/");
  };

  //Go back to dashboard
  const goBack = () => {
    history.goBack();
  };

  return (
    <div className="form-wrapper">
      <form className="form-info" onSubmit={handleSubmit(onSubmit)}>
        <h1>Vul uw bedrijfsgegevens in</h1>
        <label>Bedrijfsnaam </label>
        <input
          type="text"
          placeholder="Bedrijfsnaam"
          // name="CompanyName"
          // ref={register({ maxLength: 45 })}
          {...register('CompanyName', { maxLength: 45 })}
        />

        <label>Bedrijfsmail</label>
        <input
          type="text"
          placeholder="Bedrijfsmail"
          // name="InvoiceMail"
          // ref={register({ maxLength: 45 })}
          {...register('InvoiceMail', { maxLength: 45 })}
        />

        <label>BTW-nummer</label>
        <input
          type="text"
          placeholder="BTW-nummer"
          // name="VAT_Number"
          // ref={register({ maxLength: 45 })}
          {...register('VAT_Number', { maxLength: 45 })}
        />

        <label>Woonplaats</label>
        <input
          type="text"
          placeholder="Woonplaats"
          // name="Residence"
          // ref={register({ maxLength: 45 })}
          {...register('Residence', { maxLength: 45 })}
        />

        <label>Postcode</label>
        <input
          type="text"
          placeholder="bv. 2000"
          name="ZipCode"
          // ref={register({ maxLength: 10 })}
          {...register('ZipCode', { maxLength: 10 })}
        />

        <label>Straat</label>
        <input
          type="text"
          placeholder="Straat"
          // name="StreetName"
          // ref={register({ maxLength: 45 })}
          {...register('StreetName', { maxLength: 45 })}
        />

        <label>Huisnummer</label>
        <input
          type="text"
          placeholder="Huisnummer"
          // name="HouseNumber"
          // ref={register({ maxLength: 10 })}
          {...register('HouseNumber', { maxLength: 10 })}
        />

        <label>Busnummer</label>
        <input
          type="text"
          placeholder="Busnummer"
          // name="BusNumber"
          // ref={register({ maxLength: 10 })}
          {...register('BusNumber', { maxLength: 10 })}
        />

        <label>Land</label>
        <input
          type="text"
          placeholder="Land"
          // name="Country"
          // ref={register({ maxLength: 45 })}
          {...register('Country', { maxLength: 45 })}
        />

        <div className="button-group">
          <button onClick={goBack} className="cancel-btn">
            Annuleren
          </button>
          <button type="submit" className="submit-btn">
            Aanmaken
          </button>
        </div>
      </form>
    </div>
  );
}
