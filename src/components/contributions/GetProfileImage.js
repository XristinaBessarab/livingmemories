import React, { useEffect, useState } from "react";
import app from "../../helpers/axiosConfig";

export default function GetProfileImage (props){
    const [profileImage, setProfileImage] = useState()

    useEffect(() =>{
        (async() =>{
            const data = await app.get(`/api/visualcontent/${props.Id}/Profile`);
            setProfileImage(data.data[0].Source)
        })()
    }, [])

    return(
        (profileImage 
            ? <img src={profileImage} alt="Profiel Foto" style={{objectFit: 'cover', height: '15rem', width:'12rem', borderRadius: 40, alignSelf: 'center'}}/>
            : <div style={{height: '15rem', width:'12rem'}}/>)
    )
}