import React, { useEffect, useState } from "react";
import app from "../../helpers/axiosConfig";
import useSession from "../../auth/set-session";
import Spinner from "../../components/Spinner";
import GetProfileImage from "./GetProfileImage";
import GetContributionImage from "./GetContributionImage";
import Pagination from "../Pagination";

export default function Contributions() {
  const [contributions, setContributions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const userId = useSession();

  // Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [cardsPerPage] = useState(5);
  // Get current cards
  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentContributions = contributions.slice(
    indexOfFirstCard,
    indexOfLastCard
  );

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    (async () => {
      if (userId) {
        setIsLoading(true);
        const data = await app.get(`/api/all-user-contributions/${userId}`);
        setContributions(data.data);
        setIsLoading(false);
      }
    })();
  }, [userId]);

  useEffect(() => {
    (async () => {
      if (userId) {
        setIsLoading(true);
        const data = await app.get(`/api/all-user-contributions/${userId}`);
        setContributions(data.data);
        setIsLoading(false);
      }
    })();
  }, []);

  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }

  return (
    <div>
      <div className="my-5 py-5 d-flex justify-content-center">
        <h1>Mijn bijdragen aan herdenkingsruimtes</h1>
      </div>
      {currentContributions.length > 0 ? currentContributions.map((x) => {
        return (
          <div className="d-flex justify-content-center mt-5 mb-5">
            <div
              className="d-flex flex-column justify-content-around align-items-center w-75"
              style={{
                backgroundColor: "white",
                height: "20rem",
                borderRadius: 25,
              }}
            >
              <div className="d-flex flex-row justify-content-around align-items-center w-100 pt-3">
                <GetProfileImage Id={x.MemorialPages_Id} />
                <div
                  style={{ width: 500 }}
                  className="show-contribution-text text-justify mb-3"
                >
                  <p>{x.Title}</p>
                  <p className="overflow-auto">{x.Description}</p>
                </div>
                <GetContributionImage Id={x.Contribution_Id} />
              </div>
              <div className="d-flex flex-row justify-content-end align-items-center w-100 mr-5 pr-5 pb-3">
                <a href={`herdenkings/${x.MemorialPages_Id}`}>
                  Ga naar de herdenkingsruimte
                </a>
              </div>
            </div>
          </div>
        );
      }):
      <div className="my-5 py-5 d-flex justify-content-center" style={{height: 220}}>
        <h1>U hebt nog geen bijdragen gemaakt</h1>
      </div>
      }
      <div className="d-flex pagination">
        <Pagination
          className="col justify-self-center"
          cardsPerPage={cardsPerPage}
          totalCards={contributions.length}
          paginate={paginate}
        />
      </div>
    </div>
  );
}
