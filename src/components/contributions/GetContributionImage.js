import React, { useEffect, useState } from "react";
import app from "../../helpers/axiosConfig";

export default function GetContributionImage (props){
    const [contributionImage, setContributionImage] = useState()

    useEffect(() =>{
        (async() =>{
            const data = await app.get(`/api/visualcontent-contribution/${props.Id}`);
            setContributionImage(data.data.length > 0 && data.data[0].Source)
        })()
    }, [])

    return(
        (contributionImage 
            ? <img src={contributionImage} alt="Profiel Foto" style={{objectFit: 'cover', height: '15rem', width:'12rem', borderRadius: 40, alignSelf: 'center'}}/>
            : <div style={{height: '15rem', width:'12rem'}}/>)
    )
}