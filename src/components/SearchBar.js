import React, { useEffect, useState } from "react";
// import { useHistory } from 'react-router';
import app from "../helpers/axiosConfig";
import Fade from "react-reveal/Fade";
import { Link } from "react-router-dom";
import { BsSearch } from "react-icons/bs";
import { IoDocumentsOutline } from "react-icons/io5";

// import Search from "../images/search.png";
import Forms from "../images/forms.png";

import "../styles/SearchBar.css";

const SearchBar = (props) => {

  // fotos
  const searchIcon = 'https://lh3.googleusercontent.com/u/0/d/1TO208Id0BxcwGvaSWnZCacvIRV6i6i8E=w1920-h942-iv1';
  const paginas = 'https://lh3.googleusercontent.com/u/0/d/16FtoCaL_UJ5tZvxgwg-5p-e130s03B4g=w1920-h942-iv1';


  const [memorial, setMemorial] = useState([]);
  const [search, setSearch] = useState("");
  const [searchName, setSearchName] = useState("");

  const handleChange = (e) => {
    const name = e.target.value.toLowerCase();
    setSearchName(name);
    let result = [];
    result = memorial.filter((data) => {
      if (name === "") {
        return setSearch(null);
      }
      let lastname = data.LastName.toLowerCase().search(name) !== -1;
      let firstname = data.FirstName.toLowerCase().search(name) !== -1;
      if (firstname && lastname) {
        return firstname + " " + lastname;
      } else if (firstname) {
        return firstname;
      } else return lastname;
    });
    setSearch(result);
  };

  useEffect(() => {
    (async () => {
      const data = await app.get("/api/memorialpages");
      setMemorial(data.data);
    })();
  }, []);

  return (
    <div class="searchbar row row-cols-auto justify-content-center">
      <div class="searchbar-container">
        <div class="searchbar-content">
          {/* input */}
          <div>
            <input
              className="pt-1 pt-lg-2"
              type="search"
              placeholder="Vind een herdenkingsruimte"
              onChange={handleChange}
            />
          </div>
          {/* result of search */}
          {search &&
            search.map((data) => {
              return (
                <Fade duration={500}>
                  <Link
                    to={{
                      pathname: `/herdenkings/${data.Id}`,
                      memorial: data.Id,
                    }}
                    className="text-decoration-none text-dark"
                  >
                    <li className="dropdown-item" key={data.Id}>
                      {data.FirstName} {data.LastName}
                    </li>
                  </Link>
                </Fade>
              );
            })
          }
        </div>
        {/* buttons */}
        <div className="buttons d-flex">
          {/* search */}
          <div className="d-flex">
            <Link
              className="text-decoration-none"
              to={{
                pathname: `/search/${(searchName ? searchName : undefined)}`,
              }}
            >
              <div className="search-btn d-flex">
                <img
                  src={searchIcon}
                  alt=""
                  className=""
                  />
                <button>Zoeken</button>
              </div>
            </Link>
          </div>
          {/* all pages */}
          <div className="d-flex">
            <Link
              to="/herdenkingspaginas"
              className="text-decoration-none"
            >
              <div className="pages-btn d-flex">
                <img
                  src={paginas}
                  alt=""
                  className=""
                />
                <button>Alle Pagina's</button>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchBar;
