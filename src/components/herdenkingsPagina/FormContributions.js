import React, { useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { useForm } from "react-hook-form";
import app from "../../helpers/axiosConfig";
import { Link } from "react-router-dom";
import useSession from "../../auth/set-session";
import ProgressBar from "react-bootstrap/ProgressBar";
import { storage } from "../../firebase/firebase-config";
import { useHistory } from "react-router-dom";
import imageCompression from "browser-image-compression";

import '../../styles/herdenking/FormContribution.scss'

let axiosConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Access-Control-Allow-Origin": "*",
  },
};

const FormContributions = (props) => {
  const [image, setImage] = useState(null);
  const [imageUrlForm, setImageUrlForm] = useState("");
  const [imageControl, setImageControl] = useState([]);
  const [progress, setProgress] = useState(0);
  const [errorFileType, setErrorFileType] = useState(false);
  const [errorFileSize, setErrorFileSize] = useState(false);
  const [firstPost, setFirstPost] = useState(true);
  const history = useHistory();

  //Used images
  const Send = "https://lh3.googleusercontent.com/u/0/d/13egvs94_l0uU4357vrivrTCfUnqkOqGx=w1241-h938-iv1";

  let contributionId;

  //Update the progress state everytime the progress in handle upload is changed
  useEffect(() => {
    setProgress(progress);
  }, [progress]);

  //Getting user info from Auth0
  const { user } = useAuth0();
  const userId = useSession();
  const [userData, setUserData] = useState({});

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //Sets the uploaded image into the image state
  const handleChange = async (e) => {
    setImageUrlForm("");
    setImage(null);
    const img = e.target.files[0];
    //Here you can set the options for the new compressedImg
    const options = {
      maxSizeMb: 20,
    };
    if (img) {
      //Uploaded img max file size set to 20 MB
      //20e6 === 20 MB
      if (
        img.type === "image/jpg" ||
        img.type === "image/png" ||
        img.type === "image/jpeg"
      ) {
        setErrorFileType(false);
        if (img.size > 20e6) {
          setErrorFileSize(true);
        } else {
          setErrorFileSize(false);
          (async () => {
            try {
              const compressedImg = await imageCompression(img, options);
              await setImage(compressedImg);
              //Get the image url in a different state and show it when uploaded to form
              await setImageUrlForm(URL.createObjectURL(compressedImg));
            } catch {}
          })();
        }
      } else {
        setErrorFileType(true);
        setErrorFileSize(false);
      }
    }
  };

  //Handles uploading the contribution image to firebase
  //which then gives us the url to store in our database
  const handleUpload = () => {
    const uploadTask = storage
      .ref(
        `${props.MemorialPages_Id}/contribution/${contributionId}/${image.name}`
      )
      .put(image);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
      },
      (error) => {
        console.log(error);
      },
      () => {
        storage
          .ref(`${props.MemorialPages_Id}/contribution/${contributionId}/`)
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            console.log(contributionId, "testid");
            let visualContent = {
              Name: image.name,
              Source: url,
              Route: "Contribution",
              MemorialPages_Id: props.MemorialPages_Id,
              Contribution_Id: contributionId,
            };
            //Post visualcontent to the database
            app
              .post(`/post/visualcontent-memorial/`, visualContent, axiosConfig)
              .then(window.location.reload())
              .catch((err) => console.log(err));
          });
      }
    );
  };

  const onSubmit = async (data) => {
    if (firstPost === true) {
      if (data.Public === "0") data.Public = parseInt(data.Public);
      else data.Public = 1;
      setFirstPost(false);
      data.MemorialPages_Id = props.MemorialPages_Id;

      if (data.Relation === "true") {
        data.Relation = null;
      }

      await app
        .post(`/post/form-contribution`, data, axiosConfig)
        .then((res) => (contributionId = res.data.insertId));

      var hadContributions = {
        Users_Id: userId,
        Contribution_Id: contributionId,
        MemorialPages_Id: props.MemorialPages_Id,
      };
      await app
        .post(`/post/users-has-contributions`, hadContributions, axiosConfig)
        .then(() => {
          if (image != null) {
            handleUpload();
          } else window.location.reload();
        });
    }
  };

  useEffect(() => {
    (async () => {
      try {
        if (userId) {
          const data = await app.get(`/get/userdata/${userId}`);
          setUserData(data.data[0]);
        }
      } catch {}
    })();
  }, [userId]);

  return (
    <div className="d-flex justify-content-center">
      {userId ? (
        <form className="contributions-making-form d-flex p-4" onSubmit={handleSubmit(onSubmit)}>
          {/* contribution-send-img */}
          <div className="contribution-send-img-container">
            <input
              type="file"
              onChange={handleChange}
              accept=".jpg, .jpeg, .png"
              className=""
            />
            {/* img-errors */}
            <div className="img-errors">
              <span style={{ color: "red ", display: "inline-block" }}>
                {errorFileSize &&
                  "Oesje toch, dit bestand is te groot. De maximum bestandsgrootte is 20 MB"}
              </span>
          
              <span style={{ color: "red ", display: "inline-block" }}>
                {errorFileType &&
                  "Gelieve een bestand op te laden van het type: .jpg, .jpeg of .png"}
              </span>
            </div>
            {/* img-result */}
            <div className="img-result">
              {progress >= 1 && (
                <ProgressBar
                  style={{ minWidth: "100%", width: 0 }}
                  value={progress}
                  now={progress}
                  label={`${progress}%`}
                />
              )}
              <br />
              {imageUrlForm && (
                <img
                  src={imageUrlForm}
                  alt="profile memorial page"
                />
              )}
            </div>
          </div>
          <div className="form-info-container">
            {/* contribution-send-btn */}
            <div className="send-btn-container d-flex justify-content-end ">
              <div className="send-btn">
                <img 
                  src={Send} 
                  alt="Verstuur" 
                  className=""
                />
                <button>Verhaal insturen</button>
              </div>
            </div>
            {/* contribution-dropdown */}
            <div className="contribution-dropdown-container row justify-content-between py-2">
              <select
                className="contribution-dropdown"
                {...register('Relation')}
              >
                <option defaultChecked value="Vader">Vader</option>
                <option value="Moeder">Moeder</option>
                <option value="Broer">Broer</option>
                <option value="Zus">Zus</option>
                <option value="Vriend">Vriend(in)</option>
                <option value="Tante">Tante</option>
                <option value="Oom">Oom</option>
                <option value="Kennis">Kennis</option>
                <option value="Collega">Collega</option>
              </select>
            </div>
            {/* contribution-card-title */}
            <div className="contribution-card-title-container row py-2 px-0">
              <input
                type="text"
                {...register('Title', { required: true })}
                className="contribution-card-title text-wrap"
                placeholder="Schrijf een pakkende title voor jouw verhaal. bv: 'Onvergetelijke tijden in de strafstudie'"
              />
            </div>
            {/* contribution-card-text */}
            <div className="contribution-card-text-container row py-2 px-0">
              <textarea
                className="contribution-card-text"
                placeholder={`Deel een verhaal, herinnering, gedicht over ${props.MemorialPageName}. \n“Ik was heel close met mijn grootmoeder, helaas stierf ze in 2014. Ik heb veel foto's van haar in huis en ik deel verhalen over haar met mijn kinderen. Ze herinneren zich haar nog een beetje, maar het is gewoon zo leuk om die bijzondere verhalen te delen.”`}
                {...register("Description", { required: true })}
              />
            </div>
            {/* checkbox */}
            <div className="form-checkbox-container d-flex mt-1">
              <input
                type="checkbox"
                className=" contribution-checkbox"
                name="Public"
                {...register('Public')}
                value={0}
              />
              <label className="">
                Dit verhaal mag - enkel - zichtbaar zijn voor de beheerders (de
                nabestaanden) van de herdenkingsruimte
              </label>
            </div>
          </div>
        </form>
      ) : (
        <div className="text-center">
          <h4
          className="mt-5"
          >
            Indien u een bericht wil posten, gelieve eerst aan te melden{" "}
            <Link
              to={{ pathname: "/aanmaken-gegevens" }}
              className="text-decoration-none"
            >
              en gegevens aan te maken
            </Link>
          </h4>
        </div>
      )}
    </div>
  );
};

export default FormContributions;
