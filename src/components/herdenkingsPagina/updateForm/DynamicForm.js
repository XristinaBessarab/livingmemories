import React, { useState, useEffect, useRef } from "react";
import app from "../../../helpers/axiosConfig";
import Spinner from "../../Spinner";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import FooterCopyright from "../../FooterCopyright";
import NavBar from "../../NavBar";
import IntroText from "./DynamicComponents/IntroText";
import LatLong from "./DynamicComponents/LatLong";
import Obituary from "./DynamicComponents/Obituary";
import Quote from "./DynamicComponents/Quote";
import "../../../styles/form/formMemorial.scss";

let axiosConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Access-Control-Allow-Origin": "*",
  },
};

export default function DynamicForm(props) {
  const [memorialId, setMemorialId] = useState(props.location.params.memorialPageId);
  const [formName, setFormName] = useState(props.location.formName);
  const [dataMemorial, setDataMemorial] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const childRef = useRef();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // Go back to previous page
  const goBack = async () => {
    history.goBack();
  };

  //Gets the corresponding component if the object equals the state
  //The object comes from the Link component whenever the admin clicks on the edit icon
  const switchForm = () => {
    switch (formName) {
      case "IntroText":
        return (
          <IntroText
            ref={childRef}
            dataMemorial={dataMemorial}
            register={register}
            memorialPageId={props.location.params.memorialPageId}
          />
        );
        break;
      case "LatLong":
        return <LatLong dataMemorial={dataMemorial} register={register} />;
        break;
      case "Obituary":
        return (
          <Obituary
            ref={childRef}
            dataMemorial={dataMemorial}
            register={register}
            memorialPageId={props.location.params.memorialPageId}
          />
        );
        break;
      case "Quote":
        return <Quote dataMemorial={dataMemorial} register={register} />;
        break;
      default:
        return false;
    }
  };

  const onSubmit = (data) => {
    let memorialPage = {
      Id: memorialId,
      FirstName: dataMemorial.FirstName,
      LastName: dataMemorial.LastName,
      BirthDate: dataMemorial.BirthDate,
      DateOfDeath: dataMemorial.DateOfDeath,
      Obituary: dataMemorial.Obituary,
      Quote: dataMemorial.Quote,
      QuoteAuthor: dataMemorial.QuoteAuthor,
      IntroText: dataMemorial.IntroText,
      Latitude: dataMemorial.Latitude,
      Longitude: dataMemorial.Longitude,
      IsUndertaker: dataMemorial.IsUndertaker,
      Undertakers_Id: dataMemorial.Undertakers_Id,
    };

    switch (formName) {
      case "IntroText":
        memorialPage.FirstName = data.FirstName;
        memorialPage.LastName = data.LastName;
        memorialPage.BirthDate = data.BirthDate;
        memorialPage.DateOfDeath = data.DateOfDeath;
        memorialPage.IntroText = data.IntroText;
        // this will handle the profile img
        childRef.current.handleUpload();
        break;
      case "LatLong":
        memorialPage.Latitude = data.Latitude;
        memorialPage.Longitude = data.Longitude;
        break;
      case "Obituary":
        memorialPage.Obituary = data.Obituary;
        // this will handle the obituary img
        childRef.current.handleObituary();
        break;
      case "Quote":
        memorialPage.Quote = data.Quote;
        break;
      default:
        return false;
    }

    //Request object to update database
    app
      .put(
        `/api/update-memorialpage-with-id/${memorialId}`,
        memorialPage,
        axiosConfig
      )
      .then(
        history.push({
          pathname: `/herdenkings/${memorialId}`,
          memorial: memorialId,
        })
      );
  };

  // formName that was received will be set in localStorage
  // this prevents the page from breaking when refreshing
  useEffect(() => {
    if (formName !== undefined) {
      localStorage.setItem("@formName", formName);
    }
  }, []);

  // Getting data with the memorial id stored in localstorage
  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);

        const formNameFromLocal = localStorage.getItem("@formName");
        setFormName(formNameFromLocal !== undefined && formNameFromLocal);

        if (memorialId !== undefined) {
          const data = await app.get(`/api/memorialpages/${memorialId}`);
          setDataMemorial(data.data[0]);
        }

        setIsLoading(false);
      } catch {}
    })();
  }, [memorialId]);

  // If data is still getting fetched
  // Show a spinner untill the data gets stored in the useStates
  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }
  return (
    <>
      <NavBar />
      <div
        className="create-memorial-container"
        style={{
          minHeight: "75vh",
        }}
      >
        <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
          {switchForm()}

          <button
            type="button"
            onClick={goBack}
            className="btn-form btn-cancel"
          >
            Terug
          </button>

          <button type="submit" className="btn-form btn-submit">
            Wijzigen
          </button>
        </form>
      </div>
      <FooterCopyright />
    </>
  );
}
