import React, { useState, useEffect } from "react";
import app from "../../../helpers/axiosConfig";
import moment from "moment";
import Spinner from "../../Spinner";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import '../../../styles/form/formMemorial.scss';
import Geocode from "react-geocode";

//Identical to IntroText.js
export default function UpdateForm(props) {
  //memorialPage gets the Id when user has visited the page via a Link

  const [memorialId, setMemorialId] = useState(props.props.location.memorial);
  const [dataMemorial, setDataMemorial] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();

  const linkToLatLong = (
    <a
      href="https://www.latlong.net/"
      rel="noopener noreferrer"
      target="_blank"
    >
      <b>latlong.net</b>
    </a>
  );

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch
  } = useForm();

  //This will always be equal to the BirthDate input, with a new date as a default.
  const dateOfBirth = watch("BirthDate", new Date())

  const onSubmit = async (data) => {

    //Transforming the address so that we get the latitude and longitude
    Geocode.setApiKey(process.env.REACT_APP_GOOGLE_API_KEY);
    Geocode.setLanguage('nl');
    Geocode.setRegion('be');
    //Google geocoder returns more than one address for given lat/lng, according to google docs, ROOFTOP param returns most accurate result
    Geocode.setLocationType('ROOFTOP');

    try {
      const response = await Geocode.fromAddress(data.Adres)
      const { lat, lng } = response.results[0].geometry.location;

      //Object for memorialPage
      let memorialPage = {
        Id: memorialId,
        FirstName: data.FirstName,
        LastName: data.LastName,
        BirthDate: data.BirthDate,
        DateOfDeath: data.DateOfDeath,
        Obituary: data.Obituary,
        Quote: data.Quote,
        QuoteAuthor: data.QuoteAuthor,
        IntroText: data.IntroText,
        Latitude: lat,
        Longitude: lng,
        IsUndertaker: dataMemorial.IsUndertaker,
        Undertakers_Id: dataMemorial.Undertakers_Id,
      };
      //Request object to update database
      app
        .put(`/api/update-memorialpage-with-id/${memorialId}`, memorialPage, axiosConfig)
        .then(res => console.log(res))
        .then(history.push({
          pathname: `/herdenkings/${memorialId}`,
          memorial: memorialId
        }));

    } catch (error) {
      console.log(error)
    }
  }

  // Go back to previous page
  const goBack = async () => {
    history.goBack();
  };

  // memorial id that was received will be set in localStorage
  // this prevents the page from breaking when refreshing
  useEffect(() => {
    if (memorialId !== undefined) {
      localStorage.setItem("@update_memorial_id", memorialId.toString());
    }
  }, []);

  // Getting data from memorial id
  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);

        const idFromLocal = parseInt(localStorage.getItem("@update_memorial_id"));
        setMemorialId(idFromLocal !== undefined && idFromLocal);

        if (memorialId !== undefined) {
          const data = await app.get(`/api/memorialpages/${memorialId}`);
          setDataMemorial(data.data[0]);
        }

        setIsLoading(false);
      } catch { }
    })();
  }, [memorialId]);


  const birthDate =
    dataMemorial.BirthDate !== undefined &&
    moment(dataMemorial.BirthDate).format("YYYY-MM-DD");

  const dateOfDeath =
    dataMemorial.DateOfDeath !== undefined &&
    moment(dataMemorial.DateOfDeath).format("YYYY-MM-DD");

  // If data is still getting fetched
  // Show a spinner untill the data gets stored in the useStates
  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }
  return (
    <div
      className="create-memorial-container"
      style={{
        minHeight: "75vh",
      }}
    >
      <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
        <label>
          Dit is een herdenkingspagina ter ere van:
        </label>
        <div className="fn-ln">
          <input
            type="text"
            name="FirstName"
            defaultValue={dataMemorial.FirstName}
            ref={register}
          />
          <br />
          <input
            type="text"
            name="LastName"
            defaultValue={dataMemorial.LastName}
            ref={register}
          />
        </div>

        <label>Geboortedatum:</label>
        <input
          max={moment().format("YYYY-MM-DD")}
          type="date"
          name="BirthDate"
          defaultValue={birthDate}
          ref={register}
        />
        <label>Overlijdensdatum:</label>
        <input
          min={dateOfBirth}
          max={moment().format("YYYY-MM-DD")}
          type="date"
          name="DateOfDeath"
          defaultValue={dateOfDeath}
          ref={register}
        />

        <label className="textarea-label">
          Herinnering: deel een inspirerende, zinvolle of humoristische
          herinnering, citaat of gedicht.
        </label>
        <textarea name="Quote" defaultValue={dataMemorial.Quote} ref={register}></textarea>

        <label>Van wie komt die quote/citaat:</label>
        <input
          type="text"
          name="QuoteAuthor"
          defaultValue={dataMemorial.QuoteAuthor}
          ref={register}
        />

        <label>Wie was {dataMemorial.FirstName} in zijn/haar leven? / Intro text:</label>
        <input
          type="text"
          name="IntroText"
          defaultValue={dataMemorial.IntroText}
          ref={register}
        />

        <p
          className="visit-latlong"
          style={{
            marginTop: 15,
            marginBottom: 0,
          }}
        >
          * Als u graag heeft dat er een link is naar de locatie van de begraafplaats gelieve de naam van de begraafplaats in te vullen.
        </p>

        <label>Naam begraafplaats:</label>
        <input
          type="text"
          ref={register}
          name="Adres"
          placeholder="Gelieve opnieuw in te vullen."
        />

        <label>Overlijdensbericht:</label>
        <textarea
          name="Obituary"
          ref={register({ maxLength: 1000 })}
          defaultValue={dataMemorial.Obituary}
          style={{
            height: '150px'
          }}
        />

        <button type="button" onClick={goBack} className="btn-form btn-cancel">
          Terug
        </button>

        <button type="submit" className="btn-form btn-submit">
          Wijzigen
        </button>
      </form>
    </div>
  )
}
