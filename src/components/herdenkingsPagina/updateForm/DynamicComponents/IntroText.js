import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import { storage } from "../../../../firebase/firebase-config";
import ProgressBar from "react-bootstrap/ProgressBar";
import app from "../../../../helpers/axiosConfig";
import moment from "moment";
import imageCompression from 'browser-image-compression';

//Identical to UpdateForm.js
//We need to wrap component in `forwardRef` in order to gain
//access to the ref object that is assigned using the `ref` prop.
//This ref is passed as the second parameter to the function component.
const IntroText = forwardRef((props, ref) => {
  const [image, setImage] = useState(null);
  const [imageUrlForm, setImageUrlForm] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [imageControl, setImageControl] = useState([]);
  const [progress, setProgress] = useState(0);

  const [memorialId, setMemorialId] = useState(props.memorialPageId);

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const birthDate =
    props.dataMemorial.BirthDate !== undefined &&
    moment(props.dataMemorial.BirthDate).format("YYYY-MM-DD");

  const dateOfDeath =
    props.dataMemorial.DateOfDeath !== undefined &&
    moment(props.dataMemorial.DateOfDeath).format("YYYY-MM-DD");

  //Sets the uploaded image into the image state
  const handleChange = async(e) => {
    const img = e.target.files[0];
    const options= {
      maxSizeMb: 20
    }
    const compressedImg = await imageCompression(img, options);
    if (compressedImg) {
      //Uploaded img max file size set to 10 MB
      //10e6 === 10 MB
      if (compressedImg.size > 10e6) {
        window.alert("Upload een bestand kleiner dan 10 MB");
        return false;
      } else {
        setImage(compressedImg);
        //Get the image url in a different state and show it when uploaded to form
        setImageUrlForm(URL.createObjectURL(compressedImg));
      }
    }
  };

  // The component instance will be extended
  // with whatever you return from the callback passed
  // as the second argument
  useImperativeHandle(ref, () => ({
    // Handles uploading the image to firebase
    // which then gives us the url to store in our database
    handleUpload() {
      if(image !== null) {
        if (imageControl.length === 1) {
          // If there already is an image, delete it first
          // then upload the new image instead
          const storageRef = storage
            .ref(`${memorialId}/profile/`)
            .child(imageControl[0].Name);
          storageRef.delete().then(() => {
            const uploadTask = storage
              .ref(`${memorialId}/profile/${image.name}`)
              .put(image);
            uploadTask.on(
              "state_changed",
              (snapshot) => {
                const progress = Math.round(
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                setProgress(progress);
              },
              (error) => {
                console.log(error);
              },
              () => {
                storage
                  .ref(`${memorialId}/profile/`)
                  .child(image.name)
                  .getDownloadURL()
                  .then((url) => {
                    let visualContent = {
                      Id: imageControl[0].Id,
                      Name: image.name,
                      Source: url,
                      Route: "Profile",
                      MemorialPages_Id: memorialId,
                      Contribution_Id: null,
                    };
                    //Update visualcontent in database
                    app
                      .put(
                        `/put/updatevisualcontent-Id/${imageControl[0].Id}`,
                        visualContent,
                        axiosConfig
                      )
                      .then((res) => console.log(res));
                    setImageUrl(url);
                  });
              }
            );
          });
        }
        // If there is no image, upload the new one
        else if (imageControl.length === 0) {
          const uploadTask = storage
            .ref(`${memorialId}/profile/${image.name}`)
            .put(image);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            () => {
              storage
                .ref(`${memorialId}/profile/`)
                .child(image.name)
                .getDownloadURL()
                .then((url) => {
                  let visualContent = {
                    Name: image.name,
                    Source: url,
                    Route: "Profile",
                    MemorialPages_Id: memorialId,
                    Contribution_Id: null,
                  };
                  //Post visualcontent to the database
                  app
                    .post(
                      `/post/visualcontent-memorial/`,
                      visualContent,
                      axiosConfig
                    )
                    .then((res) => console.log(res));
                  setImageUrl(url);
                });
            }
          );
        }
      }
    },
  }));

  useEffect(() => {
    (async () => {
      try {
        // Check image in database
        const imageData = await app.get(
          `/api/visualcontent/${memorialId}/profile`
        );
        setImageControl(imageData.data !== undefined && [...imageData.data]);
      } catch {}
    })();
  }, []);

  return (
    <>
      <label>Dit is een herdenkingspagina ter ere van:</label>
      <div className="fn-ln">
        <input
          type="text"
          name="FirstName"
          defaultValue={props.dataMemorial.FirstName}
          ref={props.register}
        />
        <br />
        <input
          type="text"
          name="LastName"
          defaultValue={props.dataMemorial.LastName}
          ref={props.register}
        />
      </div>

      <label>Geboortedatum:</label>
      <input
        type="date"
        name="BirthDate"
        defaultValue={birthDate}
        ref={props.register}
      />
      <label>Overlijdensdatum:</label>
      <input
        type="date"
        name="DateOfDeath"
        defaultValue={dateOfDeath}
        ref={props.register}
      />
      <label>
        Wie was {props.dataMemorial.FirstName} in zijn/haar leven? / Intro text:
      </label>
      <textarea
        name="IntroText"
        defaultValue={props.dataMemorial.IntroText}
        ref={props.register}
        style={{
          height: "150px",
          width: "600px",
          padding: 5,
        }}
      ></textarea>
      <label>
        Voeg één nieuwe profielfoto toe van {props.dataMemorial.FirstName} aub:
      </label>
      <div>
        {progress >= 1 && (
          <ProgressBar
            style={{ minWidth: "100%", width: 0 }}
            value={progress}
            now={progress}
            label={`${progress}%`}
          />
        )}
        <br />
        <input
          type="file"
          onChange={handleChange}
          accept=".jpg, .jpeg, .png, .gif"
        />
        <br />
        {imageUrlForm && (
          <img
            style={{ height: "200px", width: "300px" }}
            src={imageUrlForm}
            alt="profile memorial page"
          />
        )}
      </div>
    </>
  );
});

export default IntroText;
