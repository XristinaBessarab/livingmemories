import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import { storage } from "../../../../firebase/firebase-config";
import ProgressBar from "react-bootstrap/ProgressBar";
import app from "../../../../helpers/axiosConfig";

const Obituary = forwardRef((props, ref) => {
  const [image, setImage] = useState(null);
  const [imageUrlForm, setImageUrlForm] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [imageControl, setImageControl] = useState([]);
  const [progress, setProgress] = useState(0);

  const [memorialId, setMemorialId] = useState(props.memorialPageId);

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Sets the uploaded image into the image state
  const handleChange = (e) => {
    const img = e.target.files[0];
    if (img) {
      //Uploaded img max file size set to 10 MB
      //10e6 === 10 MB
      if (img.size > 10e6) {
        window.alert("Upload een bestand kleiner dan 10 MB");
        return false;
      } else {
        setImage(img);
        //Get the image url in a different state and show it when uploaded to form
        setImageUrlForm(URL.createObjectURL(img));
      }
    }
  };

  // The component instance will be extended
  // with whatever you return from the callback passed
  // as the second argument
  useImperativeHandle(ref, () => ({
    // Handles uploading the image to firebase
    // which then gives us the url to store in our database
    handleObituary() {
      if ((imageControl.length === 1) && image) {
        // If there already is an image, delete it first
        // then upload the new image instead
        const storageRef = storage
          .ref(`${memorialId}/obituary/`)
          .child(imageControl[0].Name);
        storageRef.delete().then(() => {
          const uploadTask = storage
            .ref(`${memorialId}/obituary/${image.name}`)
            .put(image);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            () => {
              storage
                .ref(`${memorialId}/obituary/`)
                .child(image.name)
                .getDownloadURL()
                .then((url) => {
                  let visualContent = {
                    Id: imageControl[0].Id,
                    Name: image.name,
                    Source: url,
                    Route: "Obituary",
                    MemorialPages_Id: memorialId,
                    Contribution_Id: null,
                  };
                  //Update visualcontent in database
                  app
                    .put(
                      `/put/updatevisualcontent-Id/${imageControl[0].Id}`,
                      visualContent,
                      axiosConfig
                    )
                  setImageUrl(url);
                });
            }
          );
        });
      }
      // If there is no image, upload the new one
      else if (imageControl.length === 0) {
        const uploadTask = storage
          .ref(`${memorialId}/obituary/${image.name}`)
          .put(image);
        uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
          },
          (error) => {
            console.log(error);
          },
          () => {
            storage
              .ref(`${memorialId}/obituary/`)
              .child(image.name)
              .getDownloadURL()
              .then((url) => {
                let visualContent = {
                  Name: image.name,
                  Source: url,
                  Route: "Obituary",
                  MemorialPages_Id: memorialId,
                  Contribution_Id: null,
                };
                //Post visualcontent to the database
                app
                  .post(
                    `/post/visualcontent-memorial/`,
                    visualContent,
                    axiosConfig
                  )
                setImageUrl(url);
              });
          }
        );
      }
    },
  }));

  useEffect(() => {
    (async () => {
      try {
        // Check image in database
        const imageData = await app.get(
          `/api/visualcontent/${memorialId}/obituary`
        );
        setImageControl(imageData.data !== undefined && [...imageData.data]);
      } catch {}
    })();
  }, []);

  return (
    <>
      <label>Overlijdensbericht:</label>
      <textarea
        name="Obituary"
        ref={props.register({ maxLength: 1000 })}
        defaultValue={props.dataMemorial.Obituary}
        style={{
          height: "150px",
        }}
      />

      <label>
        Voeg één nieuwe afbeelding toe van {props.dataMemorial.FirstName} aub:
      </label>
      <div>
        {progress >= 1 && (
          <ProgressBar
            style={{ minWidth: "100%", width: 0 }}
            value={progress}
            now={progress}
            label={`${progress}%`}
          />
        )}
        <br />
        <input
          type="file"
          onChange={handleChange}
          accept=".jpg, .jpeg, .png, .gif"
        />
        <br />
        {imageUrlForm && (
          <img
            style={{ height: "200px", width: "300px" }}
            src={imageUrlForm}
            alt="obituary memorial page"
          />
        )}
      </div>
    </>
  );
});

export default Obituary;
