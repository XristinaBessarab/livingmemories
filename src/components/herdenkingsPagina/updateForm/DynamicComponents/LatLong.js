import React from "react";

export default function Quote(props) {
  const linkToLatLong = (
    <a
      href="https://www.latlong.net/"
      rel="noopener noreferrer"
      target="_blank"
    >
      <b>latlong.net</b>
    </a>
  );
  
  return (
    <>
      <p
        className="visit-latlong"
        style={{
          marginTop: 15,
          marginBottom: 0,
        }}
      >
        * Als u graag heeft dat de locatie van de begraafplaats wordt getoond,
        gelieve op {linkToLatLong} het adres in te vullen om vervolgens de
        latitude en longitude te verkrijgen. Geef deze beide in volgende twee
        velden
      </p>

      <label>Latitude begraafplaats:</label>
      <input
        type="text"
        ref={props.register}
        name="Latitude"
        defaultValue={props.dataMemorial.Latitude}
      />

      <label>Longitude begraafplaats:</label>
      <input
        type="text"
        ref={props.register}
        name="Longitude"
        defaultValue={props.dataMemorial.Longitude}
      />
    </>
  );
}
