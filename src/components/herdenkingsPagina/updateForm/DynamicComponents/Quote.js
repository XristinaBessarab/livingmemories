import React from 'react';

export default function Quote (props) {
  return (
    <>
      <label className="textarea-label">
        Herinnering: deel een inspirerende, zinvolle of humoristische
        herinnering, citaat of gedicht.
      </label>
      <textarea 
        name="Quote" 
        defaultValue={props.dataMemorial.Quote} 
        ref={props.register}
      ></textarea>
    </>
  )
}