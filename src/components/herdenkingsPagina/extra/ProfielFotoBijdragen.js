import React, { useState, useEffect } from "react";
import app from "../../../helpers/axiosConfig";

//Give a profile picture to every contribution
const ProfielFotoBijdragen = (props) => {

  //Used images
  const User = 'https://lh3.googleusercontent.com/u/0/d/1nYtNwP6aQrDdUBz2ePY488pZyz9uzvwF=w1920-h942-iv1';
  
  const [profileImage, setProfileImage] = useState();

  useEffect(() => {
    (async () => {
      const data = await app.get(
        `/api/user-authId-from-contribution/${props.Id}`
      );
      const authId = data.data[0] && data.data[0].Auth_Id;
      //The path to get or delete a user are the same, the http request makes the difference, thats why I am using the delete path here since there is no difference.
      const auth0Data = await app
        .get(process.env.REACT_APP_AUTH0_DELETE_USER + authId, {
          headers: {
            Authorization: process.env.REACT_APP_AUTH0_TOKEN,
          },
        })
        .then((res) => {
          if (res.data.picture) {
            setProfileImage(res.data.picture);
          } else setProfileImage("1");
        })
        .catch((err) => console.log("err"));
    })();
  }, []);

  return (
    <img
      key={profileImage}
      src={profileImage ? profileImage : User}
      alt="Profiel foto"
      className="mr-3 rounded-circle"
      style={{ width: "5rem" ,height: "5rem",  marginRight: "1rem"}}
    />
  );
};

export default ProfielFotoBijdragen;
