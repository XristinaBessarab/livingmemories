import React, { useState } from "react";
import { FileUploader } from "react-drag-drop-files";

const fileTypes = ["JPG", "PNG", "JPEG"];

function DragDrop() {
  const [file, setFile] = useState(null);
  const handleChange = file => {
    setFile(file);
  };

  const dropped = () =>{
      console.log("Dropped")
  }
  return (
    <FileUploader 
        handleChange={handleChange} 
        name="file" 
        types={fileTypes}
    />
  );
}

export default DragDrop;