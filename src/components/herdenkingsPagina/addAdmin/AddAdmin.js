import React, { useState, useEffect} from "react";
import Modal from "react-bootstrap/Modal";
import app from "../../../helpers/axiosConfig";
import { useForm } from "react-hook-form";
import useSession from "../../../auth/set-session";

import '../../../styles/herdenking/AddAdmin.scss'

export default function AddAdmin(props) {
  const [show, setShow] = useState(false);
  const [users, setUsers] = useState([]);
  const [admins, setAdmins] = useState([]);
  const [errorAdding, setErrorAdding] = useState(false);
  const [errorNonUser, setErrorNonUser] = useState(false);
  const [succes, setSucces] = useState(false);
  const [lastAdmin, setLastAdmin] = useState(false);
  const [mail, setMail] = useState();

  const userId = useSession();

  //Used images
  const Delete = "https://lh3.googleusercontent.com/u/0/d/1b7BtT6lO2iV8L6l-REtnibUG9GAwI8VF=w1241-h938-iv1";
  const AddUser = "https://lh3.googleusercontent.com/u/0/d/1MsYMPw7XIrdhSYxHFu_bjZiIdL6B3NCv=w1241-h938-iv1"
  const AddUser2 = "https://lh3.googleusercontent.com/u/0/d/1cz0UbN3YWCFGtnXH_iY6gjvdJU8Usgnd=w1241-h938-iv1";

  //Handles the modal opening and closing
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Handlesubmit only accepts when te
  const { handleSubmit, register } = useForm();

  //Get all users in the useEffect
  useEffect(() => {
    (async () => {
      try {
        //Get all users
        const userList = await app.get(`/api/get`, axiosConfig);
        setUsers(userList.data);

        //Get all admins of this page
        const adminList = await app.get(
          `/api/userid-of-all-admins/${props.MemorialPageId}`,
          axiosConfig
        );
        setAdmins(adminList.data);
      } catch {}
    })();
  }, []);

  //When the show state changes get all the admins again. (Currently only used by deleteAdmin)
  useEffect(() => {
    (async () => {
      //Get all admins of this page
      const adminList = await app.get(
        `/api/userid-of-all-admins/${props.MemorialPageId}`,
        axiosConfig
      );
      setAdmins(adminList.data);
    })();
  }, [show]);

  const deleteAdmin = async (user) => {
    //There should always be atleast one admin.
    if (admins.length - 1 < 1) {
      setLastAdmin(true);
    } else {
      if (userId == user.Id) {
        //Delete own admin has memorialpages reloads the page so that don't have acces anymore.
        await app.delete(
          `/delete/admin-has-memorialpage-delete/${user.AdminId}/${props.MemorialPageId}`
        );
        window.location.reload();
      } else {
        //Delete admin has memorialpages
        await app.delete(
          `/delete/admin-has-memorialpage-delete/${user.AdminId}/${props.MemorialPageId}`
        );
        //Change the state to call the useEffect again
        setShow(false);
        setShow(true);
        setErrorAdding(false);
        setErrorNonUser(false);
        setSucces(false);
      }
    }
  };

  const onSubmit = async (data) => {
    //Disable all error messages and set the mail
    setSucces(false);
    setErrorNonUser(false);
    setMail(data.Mail);
    setErrorAdding(false);
    const user = users.find((x) => x.Mail == data.Mail);
    if (user != undefined) {
      const admin = {
        Users_Id: user.Id,
      };
      const adminMemorialPage = {
        Admin_Id: 0,
        MemorialPages_Id: props.MemorialPageId,
      };
      //Get admin
      const hasAdminId = await app.get(`/api/admintable-with-id/${user.Id}`);
      if (hasAdminId.data[0]) {
        const hasAdminContribution = await app.get(
          `/api/admin-has-memorialpage/${hasAdminId.data[0].Id}/${props.MemorialPageId}`
        );
        //We check if the user already is an admin if true we give an error
        if (hasAdminContribution.data.length < 1) {
          //If this was true after correctly adding an admin set this to false to not show the text
          setErrorAdding(false);
          adminMemorialPage.Admin_Id = hasAdminId.data[0].Id;
          //Only add admin has memorialpages, the user already has an admin id
          await app.post(
            `/post/admin-has-memorialpage/`,
            adminMemorialPage,
            axiosConfig
          );
          //Change the state to call the useEffect again
          setShow(false);
          setShow(true);
          setSucces(true);
          setLastAdmin(false);
        } else setErrorAdding(true);
      } else {
        //Add admin/admin has memorialpages
        await app
          .post(`/post/form-adminpage`, admin, axiosConfig)
          .then((res) => (adminMemorialPage.Admin_Id = res.data.insertId));
        await app.post(
          `/post/admin-has-memorialpage/`,
          adminMemorialPage,
          axiosConfig
        );
        //Change the state to call the useEffect again
        setShow(false);
        setShow(true);
        setSucces(true);
        setLastAdmin(false);
      }
    } else setErrorNonUser(true);
  };

  return (
    <>
      <div className="btn-admin-add-modal-container d-flex">
        <button
          className="btn-admin-add-modal mt-4"
          style={{ fontSize: 20 }}
          onClick={handleShow}
        >
          <img
            src={AddUser2}
            alt="Icoon met gebruiker"
            width="25rem"
            className="mr-2"
          />
          Beheerder beheren
        </button>
      </div>
      <Modal
        keyboard={false}
        show={show}
        onHide={handleClose}
        size="lg"
        onExit={() => (
          setErrorAdding(false),
          setErrorNonUser(false),
          setSucces(false),
          setLastAdmin(false)
        )}
        backdrop="static"
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>Beheerder beheren</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>
              Als je toegang wenst te geven aan een kennis of familielid, dan
              dient deze persoon zich eerst te registereren. Pas - na -
              registratie kan je deze persoon toevoegen aan de hand van zijn of
              haar emailadres. Eenmaal aangeduid als beheerder krijgen ze ook
              toegang tot de verhalen die niet publiek zichtbaar zijn.
            </p>
            {/* These are the error messages depending on the situation */}
            {errorAdding && (
              <span style={{ color: "red" }}>
                <p>Deze gebruiker is al een admin!</p>
              </span>
            )}
            {errorNonUser && (
              <span style={{ color: "red" }}>
                <p>
                  Dit emailadres is niet gekend bij ons. Je kan enkel leden
                  toevoegen, als ze zich hebben aangemeld bij Viva Memoria.{" "}
                  <a
                    href={
                      "mailto:" +
                      mail +
                      "?subject=Wil jij mee de herdenkingsruimte beheren?&body=Er is een online herdenkingsruimte opgezet. Als jij je gratis registreert, dan kan je de ruimte mee beheren en toegang krijgen van boodschappen die enkel naar ons worden gericht. Ga naar  https://www.vivamemoria.be/ en klik op %22LOG IN%22. %0D%0A%0D%0AGroetjes%2C"
                    }
                  >
                    Nodig een kennis of familielid uit.
                  </a>
                </p>
              </span>
            )}
            {succes && (
              <span style={{ color: "green" }}>
                <p>De gebruiker werd succesvol toegevoegd.</p>
              </span>
            )}
            <div className="d-flex">
              <input
                style={{ width: "30rem", borderRadius: "8px", border: "1px solid #808080" }}
                className="ps-3 "
                placeholder="Zoek op e-mailadres"
                type="text"
                {...register('Mail')}
              />
              <div className="btn-admin-add-container">
                <button
                  type="submit"
                  variant="primary"
                >
                  <img
                    src={AddUser}
                    alt="Icoon met gebruiker"
                  />
                  Beheerder toevoegen
                </button>
              </div>
            </div>
          </Modal.Body>
        </form>
        <Modal.Body>
          <Modal.Title className="mb-1">Huidige beheerders</Modal.Title>
          {lastAdmin && (
            <span style={{ color: "red" }}>
              <p>Er moet minstens één beheerder zijn van een pagina.</p>
            </span>
          )}
          {admins.map((admin) => {
            const user = {
              Id: admin.Id,
              AdminId: admin.AdminId,
            };
            return (
              <p key={admin.Id}>
                <img
                  src={Delete}
                  alt="Icoon met gebruiker"
                  width="40rem"
                  className="mr-3 delete-admin-image"
                  onClick={() => deleteAdmin(user)}
                />
                {admin.FirstName} {admin.LastName}
              </p>
            );
          })}
        </Modal.Body>
      </Modal>
    </>
  );
}
