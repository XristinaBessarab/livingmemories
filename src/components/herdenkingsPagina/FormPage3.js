// import React, { useState, useEffect } from "react";
// import ProgressBar from "react-bootstrap/ProgressBar";
// import { useForm } from "react-hook-form";
// import { useHistory } from "react-router-dom";
// import { storage } from "../../firebase/firebase-config";
// import app from "../../helpers/axiosConfig";
// import Stepper from "react-stepper-enhanced";
// import imageCompression from "browser-image-compression";

// //This is not used! The order is FormPage1 -> FormPage2 -> FormPage4

// export default function FormPage3(props) {
//   const [dataMemorial, setDataMemorial] = useState(props.props.location.state);
//   const [image, setImage] = useState(null);
//   const [imageUrlForm, setImageUrlForm] = useState("");
//   const [imageUrl, setImageUrl] = useState("");
//   const [imageControl, setImageControl] = useState([]);
//   const [progress, setProgress] = useState(0);
//   //The data we use to give the input a default value when going back.
//   const [item, setItem] = useState(JSON.parse(localStorage.getItem("Form3")));
//   const {
//     register,
//     handleSubmit,
//     formState: { errors },
//   } = useForm();
//   const history = useHistory();

//   //Sets the uploaded image into the image state
//   const handleChange = async (e) => {
//     const img = e.target.files[0];
//     const options = {
//       maxSizeMb: 20,
//     };
//     const compressedImg = await imageCompression(img, options);

//     if (compressedImg) {
//       //10e6 equals 10 MB
//       if (compressedImg.size > 10e6) {
//         window.alert("Upload een bestand kleiner dan 10 MB");
//         return false;
//       } else {
//         await setImage(compressedImg);
//         //Get the image url in a different state and show it when uploaded to form
//         setImageUrlForm(URL.createObjectURL(compressedImg));
//       }
//     }
//   };

//   //On submit sending data to endpoint
//   const onSubmit = async (data) => {
//     //Object for memorialPage
//     let memorialPage = {
//       FirstName: dataMemorial.FirstName,
//       LastName: dataMemorial.LastName,
//       BirthDate: dataMemorial.BirthDate,
//       DateOfDeath: dataMemorial.DateOfDeath,
//       Obituary: data.Obituary,
//       Quote: dataMemorial.Quote,
//       QuoteAuthor: dataMemorial.QuoteAuthor,
//       IntroText: dataMemorial.IntroText,
//       Latitude: dataMemorial.Latitude,
//       Longitude: dataMemorial.Longitude,
//       IsUndertaker: null,
//       Undertakers_Id: null,
//       RightToAct: dataMemorial.RightToAct,
//       Address: dataMemorial.Address,
//       Woonplaats: dataMemorial.Woonplaats,
//       ImageProfile: dataMemorial.ImageProfile,
//       ImageObituary: image,
//     };
//     //Set the data in a localstorage to use it as defaultvalue for the inputs when the user comes back to this page.
//     localStorage.setItem("Form3", JSON.stringify(memorialPage));
//     try {
//       history.push({
//         pathname: "/memorialpage-form-4",
//         state: memorialPage,
//       });
//     } catch (error) {
//       console.log(error);
//     }
//   };

//   const goBack = async () => {
//     history.goBack();
//   };

//   return (
//     <div
//       className="f3 create-memorial-container"
//       style={{
//         minHeight: "75vh",
//       }}
//     >
//       <div className="form-custom-stepper border d-flex align-items-center justify-content-center">
//         <div className="mx-4 d-flex flex-row align-items-center">
//           <img src={Step} alt="Icoon voor editeren" width="100rem" />
//           <img
//             src={Checked}
//             alt="Icoon voor editeren"
//             width="40rem"
//             className="border-between"
//           />
//           <span className="mx-3 my-2 seperator">|</span>
//           <img src={Step2} alt="Icoon voor editeren" width="100rem" />
//           <img
//             src={Checked}
//             alt="Icoon voor editeren"
//             width="40rem"
//             className="border-between"
//           />
//           <span className="mx-3 seperator">|</span>
//           <img src={Step3} alt="Icoon voor editeren" width="100rem" />
//           <div className="mx-3">
//             <h4>Stap 3/3</h4>
//             <p>Begrafenisondernemer</p>
//           </div>
//           <img
//             src={Editing}
//             alt="Icoon voor editeren"
//             width="40rem"
//             className="border-between"
//           />
//         </div>
//       </div>

//       <form className="form-container form_1_style" onSubmit={handleSubmit(onSubmit)}>
//         <label>
//           Overlijdensbericht: <span style={{ color: "red " }}>*</span>
//         </label>
//         {errors.Name && errors.Name.type === "required" && (
//           <span style={{ color: "red " }}>Overlijdensbericht is verplicht</span>
//         )}
//         {errors.Name && errors.Name.type === "maxLength" && (
//           <span style={{ color: "red " }}>Maximum lengte overschreden</span>
//         )}
//         <textarea
//           name="Obituary"
//           ref={register({ required: true, maxLength: 1000 })}
//           placeholder="Beschrijving van het overlijdensbericht"
//           style={{
//             height: "150px",
//             width: "600px",
//             padding: 5,
//           }}
//           defaultValue={item && item.Obituary}
//         />
//         <label>Voeg één afbeelding toe van {dataMemorial.FirstName} aub:</label>
//         <div>
//           {progress >= 1 && (
//             <ProgressBar
//               style={{ minWidth: "100%", width: 0 }}
//               value={progress}
//               now={progress}
//               label={`${progress}%`}
//             />
//           )}
//           <br />
//           {/* Image is not allowed to have a defaultvalue due to security reasons, so the user has readd the image. */}
//           <input
//             type="file"
//             onChange={handleChange}
//             accept=".jpg, .jpeg, .png, .gif"
//           />
//           <br />
//           {imageUrlForm && (
//             <img
//               style={{ height: "200px", width: "300px" }}
//               src={imageUrlForm}
//               alt="obituary memorial page"
//             />
//           )}
//         </div>
//         <button type="button" onClick={goBack} className="btn-form btn-cancel">
//           Terug
//         </button>

//         <button type="submit" className="btn-form btn-submit">
//           Volgende
//         </button>
//       </form>
//     </div>
//   );
// }
