import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import app from "../../../helpers/axiosConfig";
import { useForm } from "react-hook-form";
import { storage } from "../../../firebase/firebase-config";
import ProgressBar from "react-bootstrap/ProgressBar";
import imageCompression from "browser-image-compression";
import Button from "react-bootstrap/Button";
import { FiSave } from "react-icons/fi";

import '../../../styles/herdenking/ShowContribution.scss'
import '../../../styles/herdenking/UpdateContribution.scss'


const UpdateContribution = (props) => {
  const [admin, isAdmin] = useState(false);
  const [show, setShow] = useState(false);
  const [contribution, setContribution] = useState();
  const [imageControl, setImageControl] = useState();
  const [imageControlURL, setImageControlURL] = useState();
  const [image, setImage] = useState();
  const [progress, setProgress] = useState(0);
  const [errorFileType, setErrorFileType] = useState(false);
  const [errorFileSize, setErrorFileSize] = useState(false);

  //Used images
  const Edit = "https://lh3.googleusercontent.com/u/0/d/1Kg46H5mMVHfHjxTJuBVK3y2GBhG1d6Zi=w1241-h938-iv1";

  //Handles the modal opening and closing
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const { handleSubmit, register } = useForm();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const updateBijdragen = (data) => {
    (async () => {
      //Update the contribution with the data
      //Updated contribution object
      //The private/public part is a bit confusing, we need to post the opposite of wat we get in the data to change that property.
      const updateContribution = {
        Id: props.UserContribution.Id,
        Description: data.Description
          ? data.Description
          : contribution.data[0].Description,
        Relation: data.Relation ? data.Relation : contribution.data[0].Relation,
        Public: !data.Prive,
      };
      app.put(
        `/put/update-contribution/${props.UserContribution.Id}`,
        updateContribution
      );

      //This methods either replaces the existing image or uploads a new one
      handleUpload();
    })();
    console.log(data.Prive);
  };
  //Sets the uploaded image into the image state
  const handleChange = async (e) => {
    setImage(null);
    const img = e.target.files[0];
    //Here you can set the options for the new compressedImg
    const options = {
      maxSizeMb: 20,
    };
    if (img) {
      //Uploaded img max file size set to 20 MB
      //20e6 === 20 MB
      if (
        img.type === "image/jpg" ||
        img.type === "image/png" ||
        img.type === "image/jpeg"
      ) {
        setErrorFileType(false);
        if (img.size > 20e6) {
          setErrorFileSize(true);
        } else {
          setErrorFileSize(false);
          (async () => {
            try {
              const compressedImg = await imageCompression(img, options);
              await setImage(compressedImg);
              setImageControlURL(URL.createObjectURL(compressedImg));
            } catch {}
          })();
        }
      } else {
        setErrorFileType(true);
        setErrorFileSize(false);
      }
    }
  };

  const handleUpload = () => {
    if (image !== undefined) {
      if (imageControl !== undefined) {
        // If there already is an image, delete it first
        // then upload the new image instead
        const storageRef = storage
          .ref(
            `${props.MemorialPageId}/contribution/${props.UserContribution.Id}`
          )
          .child(imageControl.Name);
        storageRef.delete().then(() => {
          const uploadTask = storage
            .ref(
              `${props.MemorialPageId}/contribution/${props.UserContribution.Id}/${image.name}`
            )
            .put(image);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            () => {
              storage
                .ref(
                  `${props.MemorialPageId}/contribution/${props.UserContribution.Id}`
                )
                .child(image.name)
                .getDownloadURL()
                .then((url) => {
                  let updatedVisualContent = {
                    Id: imageControl.Id,
                    Name: image.name,
                    Source: url,
                  };
                  //Update visualcontent in database
                  app
                    .put(
                      `/put/updatevisualcontent-Id/${imageControl.Id}`,
                      updatedVisualContent,
                      axiosConfig
                    )
                    .then(window.location.reload());
                });
            }
          );
        });
      }
      // If there is no image, upload the new one
      else if (imageControl === undefined) {
        const uploadTask = storage
          .ref(
            `${props.MemorialPageId}/contribution/${props.UserContribution.Id}/${image.name}`
          )
          .put(image);
        uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
          },
          (error) => {
            console.log(error);
          },
          () => {
            storage
              .ref(
                `${props.MemorialPageId}/contribution/${props.UserContribution.Id}`
              )
              .child(image.name)
              .getDownloadURL()
              .then((url) => {
                let visualContent = {
                  Name: image.name,
                  Source: url,
                  Route: "Contribution",
                  MemorialPages_Id: props.MemorialPageId,
                  Contribution_Id: props.UserContribution.Id,
                };
                //Post visualcontent to the database
                app
                  .post(
                    `/post/visualcontent-memorial/`,
                    visualContent,
                    axiosConfig
                  )
                  .then(window.location.reload());
              });
          }
        );
      }
    } else window.location.reload();
  };

  useEffect(() => {
    (async () => {
      //Check if the user is an admin of that contribution.
      var userContribution = await app.get(
        `/api/user-contribution/${props.UserContribution.Id}`
      );
      if (userContribution.data[0]) {
        if (userContribution.data[0].Users_Id === props.UserId) {
          isAdmin(true);
        }
      }

      try {
        //Get the current data of the contribution
        let contrib = await app.get(
          `/api/unique-contribution/${props.UserContribution.Id}`
        );
        await setContribution(contrib);

        //Get the image of contribution if exists
        const imageData = await app.get(
          `/api/visualcontent-contribution/${props.UserContribution.Id}`
        );
        setImageControl(imageData.data[0]);
      } catch {}
    })();
  }, [props.UserContribution.Id]);

  return (
    <div className="update-contribution">
      {admin && (
        <div className="edit-container">
          <div className="edit">
            <div 
              className="" 
              onClick={handleShow}
            >
              <img src={Edit} alt="Icoon editeren" />
            </div>
          </div>
          <Modal
            show={show}
            onHide={handleClose}
            onExited={() => setImageControlURL()}
            backdrop="static"
            size="lg"
          >
            <form onSubmit={handleSubmit(updateBijdragen)}>
              <Modal.Header closeButton></Modal.Header>
              <Modal.Body>
                <div className="update-form-contribution d-flex flex-column">
                  <Modal.Title className="modal-title">Verhaal aanpassen</Modal.Title>
                  <div className="teaxtarea-container d-flex flex-column">
                    <label>Beschrijving: </label>
                    <textarea
                      type="text"
                      {...register('Description')}
                      placeholder="Description"
                      defaultValue={
                        contribution && contribution.data[0].Description
                      }
                    />
                  </div>
                  
                  <div className="select-container d-flex flex-column">
                    <label className="">Relatie: </label>
                    <select
                    className="update-form-contribution-select"
                      {...register('Relation')}
                      defaultValue={contribution && contribution.data[0].Relation}
                    >
                      <option value="Vader">Vader</option>
                      <option value="Moeder">Moeder</option>
                      <option value="Zus">Zus</option>
                      <option value="Vriend">Vriend(in)</option>
                      <option value="Tante">Tante</option>
                      <option value="Oom">Oom</option>
                      <option value="Kennis">Kennis</option>
                      <option value="Collega">Collega</option>
                    </select>
                  </div>

                  <div className="mt-3 ms-5 d-flex">
                    <input
                        type="checkbox"
                        className="checkbox-input col-1 p-0 m-0"
                        {...register('Prive')}
                        defaultChecked={contribution && !contribution.data[0].Public}
                      />
                      <label className="ckeckbox-container-lable "><span className="">Dit verhaal mag - enkel - zichtbaar zijn voor de beheerders (de nabestaanden) van de herdenkingsruimte </span></label>

                  </div>
                  
                  <div className="image-container d-flex flex-column">
                    <span style={{ color: "red ", display: "inline-block" }}>
                      {errorFileSize && "Upload een bestand kleiner dan 20mb!"}
                    </span>
                    
                    <span style={{ color: "red ", display: "inline-block" }}>
                      {errorFileType && "Upload een png/jpg/jpeg bestand!"}
                    </span>
                    
                    <input
                      type="file"
                      onChange={handleChange}
                      accept=".jpg, .jpeg, .png"
                      className="mb-5 mt-4"
                    />
                    {progress >= 1 && (
                      <ProgressBar
                        style={{ minWidth: "100%", width: 0 }}
                        value={progress}
                        now={progress}
                        label={`${progress}%`}
                      />
                    )}
                    {(imageControl !== undefined ||
                      imageControlURL !== undefined) && (
                      <img
                        src={
                          imageControlURL !== undefined
                            ? imageControlURL
                            : imageControl.Source
                        }
                        alt="profile memorial page"
                        className="align-self-center"
                      />
                    )}
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button type="submit" className="bg-transparent border-0">
                    <div className="button-save-change-container d-flex justify-content-center">
                      <div className="button-save-change">
                        <FiSave className="save-img"/>
                        <button>opslaan</button>
                      </div>
                    </div>
                </Button>
              </Modal.Footer>
            </form>
          </Modal>
        </div>
      )}
    </div>
  );
};

export default UpdateContribution;
