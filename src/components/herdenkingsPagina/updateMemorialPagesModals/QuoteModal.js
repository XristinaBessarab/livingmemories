import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useForm } from "react-hook-form";
import app from "../../../helpers/axiosConfig";

const QuoteModal = (props) => {
  const [userdata, setuserdata] = useState(props.userdata);

  const { register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    let updatedMemorialPage = {
      Id: userdata.Id,
      Quote: data.Quote,
      QuoteAuthor: data.QuoteAuthor,
    };

    app.put(
      `/api/update-memorialpage-with-id/${userdata.Id}`,
      updatedMemorialPage
    )
    .then(window.location.reload())
  };

  return (
    <div>
      {/* When exiting the modal we set the preview image back to the original image.*/}
      <Modal {...props}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>IntroText Wijzigen</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label style={{ marginRight: 60 }}>Citaat auteur: </label>
            <input
              type="text"
              // name="QuoteAuthor"
              defaultValue={userdata !== undefined && userdata.QuoteAuthor}
              // ref={register({ required: true })}
              {...register('QuoteAuthor', { required: true })}
            />{" "}
            <br />
            <p>
              Herinnering: deel een inspirerende, zinvolle of humoristische
              herinnering, citaat of gedicht:{" "}
            </p>
            <textarea
              // name="Quote"
              defaultValue={userdata !== undefined && userdata.Quote}
              // ref={register({ required: true })}
              {...register('Quote',{ required: true })}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button type="submit" variant="danger">
              Wijzigen
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};
export default QuoteModal;
