import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useForm } from "react-hook-form";
import ProgressBar from "react-bootstrap/ProgressBar";
import app from "../../../helpers/axiosConfig";
import imageCompression from "browser-image-compression";
import { storage } from "../../../firebase/firebase-config";

const ObituaryModal = (props) => {
  const [userdata, setuserdata] = useState(props.userdata);
  const [obituaryImage, setObituaryImage] = useState();
  const [ogObituaryImage, setOgObituaryImage] = useState();
  const [obituaryImageUrl, setObituaryImageUrl] = useState();
  const [progress, setProgress] = useState(0);
  const [errorFileType, setErrorFileType] = useState(false);
  const [errorFileSize, setErrorFileSize] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    (async () => {
      const data = await app.get(`/api/visualcontent-obituary/${userdata.Id}`);
      if (data.data.length > 0) {
        await setObituaryImageUrl(data.data[0].Source);
        await setOgObituaryImage(data.data[0]);
      }
    })();
  }, []);

  //Sets the uploaded image into the image state
  const handleChange = async (e) => {
    setObituaryImageUrl("");
    setObituaryImage(null);
    const img = e.target.files[0];

    //Here you can set the options for the new compressedImg
    const options = {
      maxSizeMb: 20,
    };
    if (img) {
      //Uploaded img max file size set to 20 MB
      //20e6 === 20 MB
      if (
        img.type === "image/jpg" ||
        img.type === "image/png" ||
        img.type === "image/jpeg"
      ) {
        setErrorFileType(false);
        if (img.size > 20e6) {
          setErrorFileSize(true);
        } else {
          setErrorFileSize(false);
          (async () => {
            try {
              const compressedImg = await imageCompression(img, options);
              await setObituaryImage(compressedImg);
              //Get the image url in a different state and show it when uploaded to form
              await setObituaryImageUrl(URL.createObjectURL(compressedImg));
            } catch {}
          })();
        }
      } else {
        setErrorFileType(true);
        setErrorFileSize(false);
      }
    }
  };

  const handleUpload = () => {
    //First check if there is a new image.
    if (obituaryImage !== undefined) {
      //Then check if there is an old image we need to delete
      if (ogObituaryImage !== undefined) {
        const storageRef = storage
          .ref(`${userdata.Id}/obituary/`)
          .child(ogObituaryImage.Name);
        storageRef.delete().then(() => {
          const uploadTask = storage
            .ref(`${userdata.Id}/obituary/${obituaryImage.name}`)
            .put(obituaryImage);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            () => {
              storage
                .ref(`${userdata.Id}/obituary/`)
                .child(obituaryImage.name)
                .getDownloadURL()
                .then((url) => {
                  let visualContent = {
                    Id: ogObituaryImage.Id,
                    Name: obituaryImage.name,
                    Source: url,
                    Route: "Obituary",
                    MemorialPages_Id: userdata.Id,
                    Contribution_Id: null,
                  };
                  //Update visualcontent in database
                  app
                    .put(
                      `/put/updatevisualcontent-Id/${ogObituaryImage.Id}`,
                      visualContent
                    )
                    .then(window.location.reload())
                });
            }
          );
        });
      } else if (ogObituaryImage === undefined) {
        const uploadTask = storage
          .ref(`${userdata.Id}/obituary/${obituaryImage.name}`)
          .put(obituaryImage);
        uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
          },
          (error) => {
            console.log(error);
          },
          () => {
            storage
              .ref(`${userdata.Id}/obituary/`)
              .child(obituaryImage.name)
              .getDownloadURL()
              .then((url) => {
                let visualContent = {
                  Name: obituaryImage.name,
                  Source: url,
                  Route: "Obituary",
                  MemorialPages_Id: userdata.Id,
                  Contribution_Id: null,
                };
                //Update visualcontent in database
                app
                  .post(
                    `/post/visualcontent-memorial/`,
                    visualContent
                  )
                  .then(window.location.reload());
              });
          }
        );
      }
    } else window.location.reload();
  };

  const onSubmit = (data) => {
    console.log(data);

    let updatedMemorialPage = {
      Id: userdata.Id,
      Obituary: data.Obituary,
    }
    
    app.put(
      `/api/update-memorialpage-with-id/${userdata.Id}`,
      updatedMemorialPage
    )
    .then(res => console.log(res))
    .then(() => handleUpload())
    .catch(err => console.log(err))
  };

  return (
    <div>
      {/* When exiting the modal we set the preview image back to the original image.*/}
      <Modal
        {...props}
        onExited={() => setObituaryImageUrl(ogObituaryImage && ogObituaryImage.Source)}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>Overlijdensbericht Wijzigen</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Overlijdensbericht: </p>
            {errors.Overlijdensbericht && errors.Overlijdensbericht.type === "required" && (
              <span style={{ color: "red " }}>Overlijdensbericht is verplicht</span>
            )}
            <textarea
              // name="Obituary"
              defaultValue={userdata !== undefined && userdata.Obituary}
              // ref={register({ required: true })}
              {...register('Obituary', { required: true })}
            />
            <p>Foto toevoegen/vervangen van {userdata.FirstName}: </p>
            <span style={{ color: "red ", display: "inline-block" }}>
              <span style={{ color: "red ", display: "inline-block" }}>
                {errorFileSize &&
                  "Oesje toch, dit bestand is te groot. De maximum bestandsgrootte is 20 MB"}
              </span>
              {errorFileType &&
                "Gelieve een bestand op te laden van het type: .jpg, .jpeg of .png"}
            </span>
            {progress >= 1 && (
              <ProgressBar
                style={{ minWidth: "100%", width: 0 }}
                value={progress}
                now={progress}
                label={`${progress}%`}
              />
            )}
            <input
              type="file"
              onChange={handleChange}
              accept=".jpg, .jpeg, .png, .gif"
            />
            {obituaryImageUrl && (
              <img
                style={{ height: "200px", width: "300px" }}
                src={obituaryImageUrl}
                alt="profile memorial page"
              />
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button type="submit" variant="danger">
              Wijzigen
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default ObituaryModal;
