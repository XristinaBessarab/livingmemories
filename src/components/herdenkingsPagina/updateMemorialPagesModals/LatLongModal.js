import React, { useState, useEffect, useCallback, useMemo } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useForm } from "react-hook-form";
import app from "../../../helpers/axiosConfig";
import Autocomplete from "react-google-autocomplete";
import DataListInput from "react-datalist-input";
import { FiSave } from "react-icons/fi";

import "../../../styles/updateModals/LatLongModal.scss";

//This is the modal for changing the memorial location and of the undertaker
const LatLongModal = (props) => {
  const [userdata, setuserdata] = useState(props.userdata);
  const [adres, setAdres] = useState();
  const [selected, setSelected] = useState();
  const [dataUndertakers, setDataUndertakers] = useState([]);
  const [isChecked, setIsChecked] = useState();

  const {
    handleSubmit,
    formState: { errors },
    register,
  } = useForm();

  const onSelect = useCallback((selectedItem) => {
    setSelected(selectedItem);
  }, []);

  //Get undertaker data
  useEffect(() => {
    (async () => {
      try {
        const undertakers = await app.get(`/api/all-undertakers`);
        setDataUndertakers(undertakers.data);
      } catch {}
    })();
  }, []);

  const items = useMemo(
    () =>
      dataUndertakers.map((oneItem) => ({
        label: oneItem.Name,
        key: oneItem.Id,
        ...oneItem,
      })),
    [dataUndertakers]
  );

  const handleCheckbox = (e) => {
    const checked = e.target.checked;
    if (checked) {
      setIsChecked(true);
      setSelected(false);
    } else {
      setIsChecked(false);
      setSelected(false);
    }
  };

  useEffect(() => {
    setSelected(undefined);
  }, [isChecked]);

  const onSubmit = (data) => {
    if (data.Name) {
      //Undertaker object
      let undertaker = {
        Name: data.Name,
        Street: data.Street,
        Website: data.Website,
        HouseNumber: data.HouseNumber,
        Busnumber: data.Busnumber,
        ZipCode: data.Postcode,
        City: data.City,
        PhoneNumber: data.PhoneNumber,
        Shop: data.WebsiteUrl,
        FlowerShop: data.FlowerWebsiteUrl,
      };
      let newUndertakerId;
      //Route that makes a new undertaker and changes the undertaker of the memorialpage.
      (async () => {
        await app
          .post("/post/form-undertaker", undertaker)
          .then((res) => (newUndertakerId = res.data.insertId));
        let newUndertaker = {
          Id: userdata.Id,
          Undertakers_Id: newUndertakerId,
        };
        await app.put(
          `/api/update-memorialpage-with-id/${userdata.Id}`,
          newUndertaker
        );

        //If there is no adres reload here
        !adres && window.location.reload();
      })();
    } else {
      let newUndertaker = {
        Id: userdata.Id,
        Undertakers_Id: selected && selected.Id,
      };
      //Route that changes the undertaker of the memorialpage.
      app.put(`/api/update-memorialpage-with-id/${userdata.Id}`, newUndertaker);

      //If there is no adres reload here
      !adres && window.location.reload();
    }

    let lat;
    let lng;
    if (adres) {
      lat = adres.geometry.location.lat();
      lng = adres.geometry.location.lng();
      let updatedMemorialPage = {
        Id: userdata.Id,
        Address: adres ? adres.formatted_address : null,
        Latitude: lat,
        Longitude: lng,
      };

      app
        .put(
          `/api/update-memorialpage-with-id/${userdata.Id}`,
          updatedMemorialPage
        )
        .then(window.location.reload());
    }
  };

  return (
    <div>
      <Modal
        backdrop="static"
        {...props}
        size="lg"
        onExit={() => setIsChecked(false)}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <div className="update-form-map d-flex flex-column">
              <div className="d-flex flex-column ">
                <Modal.Title className="modal-title ">Begraafplaats</Modal.Title>
                <div className="d-flex flex-column">
                  <p className="mb-0 mt-4">Adres begraafplaats </p>
                  <Autocomplete
                    name="Adres"
                    className=""
                    defaultValue={userdata != undefined && userdata.Address}
                    //apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                    inputAutocompleteValue="off"
                    language="nl"
                    onPlaceSelected={(place) => {
                      setAdres(place);
                    }}
                    defaultValue={userdata.Address && userdata.Address}
                    options={{
                      types: ["address"],
                      componentRestrictions: { country: ["be", "nl"] },
                    }}
                  />
                </div>
                <Modal.Title className="modal-title mt-4">
                  Begrafenisondernemer
                </Modal.Title>
                <div className="text-wrap mt-3">
                  <span className="d-inline">
                    Wilt u een begrafenisondernemer kiezen / aanpassen?
                  </span>
                </div>
                {props.undertaker.Name && (
                  <p className="mt-4">Huidige begrafenisondernemer: {props.undertaker.Name}</p>
                )}
                <div className="ondernemer-input" style={isChecked ? { display: "none" } : { display: "block" }}>
                  <div className="map-select-container d-flex flex-column mt-2">
                    <span>Zoek de ondernemer:</span>
                    <DataListInput
                      placeholder="Selecteer een ondernemer van de lijst"
                      items={items}
                      onSelect={onSelect}
                      {...register('DataList')}
                      inputClassName="form-inputs-datalist"
                    />
                  </div>
                </div>
              </div>
              <div className="ckeckbox-container d-flex">
                <input
                  type="checkbox"
                  className="checkbox-input col-1 p-0 m-0"
                  {...register('Public')}
                  onChange={handleCheckbox}
                  value={0}
                />
                <label className="ckeckbox-container-lable ">
                  <span>Zelf een ondernemer toevoegen?</span>
                </label>
              </div>
              {/* 2d form */}
              {isChecked && selected === undefined ? (
                <div className="d-flex flex-column second-form mt-3">
                  {/* <div className="d-flex justify-content-around"> */}
                    <div className="mb-3">
                      <p className="mb-0">
                        Naam<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        {...register('Name', { required: true, maxLength: 35 })}
                        className="form-inputs-datalist"
                      />
                      {errors.Name && errors.Name.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Naam is verplicht
                        </p>
                      )}
                      {errors.Name && errors.Name.type === "maxLength" && (
                        <p style={{ color: "red " }} className="">
                          Maximum 35 karakters
                        </p>
                      )}
                    </div>
                    <div className="mb-3">
                      <p className="mb-0">
                        Straat<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        {...register('Street', { required: true })}
                        className="form-inputs-datalist"
                      />
                      {errors.Street && errors.Street.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Straat is verplicht
                        </p>
                      )}
                    </div>
                  {/* </div> */}
                  {/* <div className="d-flex justify-content-around"> */}
                    <div className="mb-3">
                      <p className="mb-0">
                        Huisnummer<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        {...register('HouseNumber', { required: true })}
                        className="form-inputs-datalist"
                      />
                      {errors.HouseNumber &&
                        errors.HouseNumber.type === "required" && (
                          <p style={{ color: "red " }} className="">
                            Huisnummer is verplicht
                          </p>
                        )}
                    </div>
                    <div className="mb-3">
                      <p className="mb-0">Busnummer</p>
                      <input
                        {...register('BusNumber')}
                        className="form-inputs-datalist"
                      />
                    </div>
                  {/* </div> */}
                  {/* <div className="d-flex justify-content-around"> */}
                    <div className="mb-3">
                      <p className="mb-0">
                        Postcode<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        {...register('Postcode', { required: true })}
                        className="form-inputs-datalist"
                      />
                      {errors.Postcode && errors.Postcode.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Postcode is verplicht
                        </p>
                      )}
                    </div>
                    <div className="mb-3">
                      <p className="mb-0">
                        Stad<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        {...register('City', { required: true })}
                        className="form-inputs-datalist"
                      />
                      {errors.City && errors.City.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Stad is verplicht
                        </p>
                      )}
                    </div>
                  {/* </div> */}
                  {/* <div className="d-flex justify-content-around"> */}
                    <div className="mb-3">
                      <p className="mb-0">
                        Telefoonnummer<span style={{ color: "red" }}>*</span>
                      </p>
                      <input
                        type="number"
                        {...register('PhoneNumber', { required: true })}
                        className="form-inputs-datalist"
                      />
                      {errors.PhoneNumber &&
                        errors.PhoneNumber.type === "required" && (
                          <p style={{ color: "red" }} className="">
                            Telefoonnummer is verplicht
                          </p>
                        )}
                    </div>
                    <div className="mb-3">
                      <p className="mb-0">Website</p>
                      <input
                        {...register('Website')}
                        className="form-inputs-datalist"
                      />
                    </div>
                  {/* </div> */}
                  {/* <div className="d-flex justify-content-around"> */}
                    <div className="mb-3">
                      <p className="mb-0">Webwinkel url</p>
                      <input
                        {...register('WebsiteUrl')}
                        className="form-inputs-datalist"
                      />
                    </div>
                    <div className="mb-3">
                      <p className="mb-0">Webwinkel bloemen aankoop url</p>
                      <input
                        {...register('FlowerWebsiteUrl')}
                        className="form-inputs-datalist"
                      />
                    </div>
                  {/* </div> */}
                </div>
              ) : (
                <></>
              )}
            </div>
          </Modal.Body>
          <Modal.Footer>
          <Button type="submit" className="bg-transparent border-0">
              <div className="button-save-change-container d-flex justify-content-center">
                <div className="button-save-change">
                  <FiSave className="save-img"/>
                  <button>opslaan</button>
                </div>
              </div>
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default LatLongModal;
