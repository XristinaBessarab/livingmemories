import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useForm } from "react-hook-form";
import moment from "moment";
import ProgressBar from "react-bootstrap/ProgressBar";
import app from "../../../helpers/axiosConfig";
import imageCompression from "browser-image-compression";
import { storage } from "../../../firebase/firebase-config";
import Autocomplete from "react-google-autocomplete";
import { FiSave } from "react-icons/fi";

import "../../../styles/updateModals/IntroTextModal.scss";

const IntroTextModal = (props) => {
  // used images
  // const save = ''
  const [userdata, setuserdata] = useState(props.userdata);
  const [profileImage, setProfileImage] = useState();
  const [ogProfileImage, setOgProfileImage] = useState();
  const [profileImageUrl, setProfileImageUrl] = useState();
  const [progress, setProgress] = useState(0);
  const [errorFileType, setErrorFileType] = useState(false);
  const [errorFileSize, setErrorFileSize] = useState(false);
  const [woonplaats, setWoonplaats] = useState();
  const [donations, setDonations] = useState(props.userdata.Donations);

  //profileImage = the new foto we get.
  //ogProfileImage = the old image, we use this to check if there is an old image and delete it
  //profileImageUrl = The url of the old and new image used to show the image to the user

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  //This will always be equal to the BirthDate input, with a new date as a default.
  const dateOfBirth = watch("BirthDate", new Date());
  //DateOfBirth default value for the input.
  const dob = moment(userdata.BirthDate).format("YYYY-MM-DD");
  //DateOfDeath default value for the input.
  const dod = moment(userdata.DateOfDeath).format("YYYY-MM-DD");

  useEffect(() => {
    (async () => {
      const data = await app.get(`/api/visualcontent-profile/${userdata.Id}`);
      if (data.data.length > 0) {
        await setProfileImageUrl(data.data[0].Source);
        await setOgProfileImage(data.data[0]);
      }
    })();
  }, []);

  const onSubmit = (data) => {
    let updatedMemorialPage = {
      Id: userdata.Id,
      FirstName: data.FirstName,
      LastName: data.LastName,
      BirthDate: data.BirthDate,
      DateOfDeath: data.DateOfDeath,
      IntroText: data.IntroText,
      Donations: data.Donations == false ? 0 : 1,
      Woonplaats: woonplaats
        ? woonplaats.address_components[0].long_name
        : null,
    };

    app
      .put(
        `/api/update-memorialpage-with-id/${userdata.Id}`,
        updatedMemorialPage
      )
      .then((res) => console.log(res))
      .then(() => handleUpload())
      .catch((err) => console.log(err));
  };

  //Sets the uploaded image into the image state
  const handleChange = async (e) => {
    setProfileImageUrl("");
    setProfileImage(null);
    const img = e.target.files[0];

    //Here you can set the options for the new compressedImg
    const options = {
      maxSizeMb: 20,
    };
    if (img) {
      //Uploaded img max file size set to 20 MB
      //20e6 === 20 MB
      if (
        img.type === "image/jpg" ||
        img.type === "image/png" ||
        img.type === "image/jpeg"
      ) {
        setErrorFileType(false);
        if (img.size > 20e6) {
          setErrorFileSize(true);
        } else {
          setErrorFileSize(false);
          (async () => {
            try {
              const compressedImg = await imageCompression(img, options);
              await setProfileImage(compressedImg);
              //Get the image url in a different state and show it when uploaded to form
              await setProfileImageUrl(URL.createObjectURL(compressedImg));
            } catch {}
          })();
        }
      } else {
        setErrorFileType(true);
        setErrorFileSize(false);
      }
    }
  };

  const handleUpload = () => {
    //First check if there is a new image.
    if (profileImage !== undefined) {
      //Then check if there is an old image we need to delete
      if (ogProfileImage !== undefined) {
        const storageRef = storage
          .ref(`${userdata.Id}/profile/`)
          .child(ogProfileImage.Name);
        storageRef.delete().then(() => {
          const uploadTask = storage
            .ref(`${userdata.Id}/profile/${profileImage.name}`)
            .put(profileImage);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            () => {
              storage
                .ref(`${userdata.Id}/profile/`)
                .child(profileImage.name)
                .getDownloadURL()
                .then((url) => {
                  let visualContent = {
                    Id: ogProfileImage.Id,
                    Name: profileImage.name,
                    Source: url,
                    Route: "Profile",
                    MemorialPages_Id: userdata.Id,
                    Contribution_Id: null,
                  };
                  //Update visualcontent in database
                  app
                    .put(
                      `/put/updatevisualcontent-Id/${ogProfileImage.Id}`,
                      visualContent
                    )
                    .then(window.location.reload());
                });
            }
          );
        });
      } else if (ogProfileImage === undefined) {
        const uploadTask = storage
          .ref(`${userdata.Id}/profile/${profileImage.name}`)
          .put(profileImage);
        uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress = Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
          },
          (error) => {
            console.log(error);
          },
          () => {
            storage
              .ref(`${userdata.Id}/profile/`)
              .child(profileImage.name)
              .getDownloadURL()
              .then((url) => {
                let visualContent = {
                  Name: profileImage.name,
                  Source: url,
                  Route: "Profile",
                  MemorialPages_Id: userdata.Id,
                  Contribution_Id: null,
                };
                //Update visualcontent in database
                app
                  .post(`/post/visualcontent-memorial/`, visualContent)
                  .then(window.location.reload());
              });
          }
        );
      }
    } else window.location.reload();
  };

  const handleDonation = (e) => {
    const checked = e.target.checked;
    console.log("cakal");
    localStorage.setItem("DonationChecked", JSON.stringify(checked));
    if (checked) {
      setDonations(true);
    } else {
      setDonations(false);
    }
  };

  return (
    <div>
      {/* When exiting the modal we set the preview image back to the original image.*/}
      <Modal
        backdrop="static"
        size="lg"
        {...props}
        onExited={() =>
          setProfileImageUrl(ogProfileImage && ogProfileImage.Source)
        }
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            {/* This gives an error because the default value changes from undefined to the data from userdata.
          'A component is changing an controlled input to a uncontrolled this is likely from the value changing.'
          I am now checking and only asigning the default value if the data is not undefined this why it will only get a value once. */}
            <div className="update-form d-flex flex-column justify-content-center align-items-center">
              <Modal.Title className="modal-title">Kerngegevens</Modal.Title>
              <div className="d-flex flex-column">
                <label className="mt-4">
                  Voornaam<span className="text-danger">*</span>
                </label>
                <input
                  type="text"
                  className=""
                  defaultValue={userdata !== undefined && userdata.FirstName}
                  {...register("FirstName", { required: true, maxLength: 30 })}
                />
                {errors.FirstName && errors.FirstName.type === "required" && (
                  <span className="form-error">Voornaam is verplicht</span>
                )}
                {errors.FirstName && errors.FirstName.type === "maxLength" && (
                  <p className="form-error">Maximum 30 karakters</p>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="">
                  Achternaam<span className="text-danger">*</span>
                </label>
                <input
                  type="text"
                  className=""
                  defaultValue={userdata !== undefined && userdata.LastName}
                  {...register("LastName", { required: true, maxLength: 30 })}
                />
                {errors.LastName && errors.LastName.type === "required" && (
                  <span className="form-error">Achternaam is verplicht</span>
                )}
                {errors.LastName && errors.LastName.type === "maxLength" && (
                  <p className="form-error">Maximum 30 karakters</p>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="">
                  Geboortedatum<span className="text-danger">*</span>
                </label>
                <input
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  className=""
                  defaultValue={dob}
                  {...register("BirthDate", { required: true })}
                />
              </div>
              <div className="d-flex flex-column">
                <label className="">
                  Overlijdensdatum<span className="text-danger">*</span>
                </label>
                <input
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  className=""
                  defaultValue={dod}
                  {...register("DateOfDeath", { required: true })}
                />
              </div>
              <div className="d-flex flex-column">
                <label className="">Woonplaats</label>
                <Autocomplete
                  name="Woonplaats"
                  className=""
                  // apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                  // defaultValue={userdata !== undefined && userdata.Woonplaats}
                  inputAutocompleteValue="off"
                  language="nl"
                  onPlaceSelected={(place) => {
                    setWoonplaats(place);
                  }}
                  defaultValue={userdata.Woonplaats && userdata.Woonplaats}
                  options={{
                    types: ["(regions)"],
                    componentRestrictions: { country: ["be", "nl"] },
                  }}
                />
              </div>
              <div className="d-flex flex-column">
                <label className="">
                  Wie was {userdata.FirstName}
                  <span className="text-danger">*</span>
                </label>
                <textarea
                  className=""
                  defaultValue={userdata !== undefined && userdata.IntroText}
                  {...register("IntroText", {
                    required: true,
                    maxLength: 2000,
                    minLength: 200,
                  })}
                />
                {errors.IntroText && errors.IntroText.type === "maxLength" && (
                  <span className="form-error">
                    Maximum lengte van 2000 karakters overschreden
                  </span>
                )}
                {errors.IntroText && errors.IntroText.type === "minLength" && (
                  <span className="form-error">
                    Minimum lengte van 200 karakters
                  </span>
                )}
                <span style={{ color: "red ", display: "inline-block" }}>
                  <span style={{ color: "red ", display: "inline-block" }}>
                    {errorFileSize &&
                      "Oesje toch, dit bestand is te groot. De maximum bestandsgrootte is 20 MB"}
                  </span>
                  {errorFileType &&
                    "Gelieve een bestand op te laden van het type: .jpg, .jpeg of .png"}
                </span>
                {progress >= 1 && (
                  <ProgressBar
                    style={{ minWidth: "100%", width: 0 }}
                    value={progress}
                    now={progress}
                    label={`${progress}%`}
                  />
                )}
              </div>
              <div className="d-flex flex-column">
                <label>Dit komt bovenaan in je herdenkingsruimte</label>
                <input
                  type="file"
                  onChange={handleChange}
                  className="mb-5"
                  accept=".jpg, .jpeg, .png, .gif"
                />
                {profileImageUrl && (
                  <img
                    src={profileImageUrl}
                    alt="profile memorial page"
                    className="align-self-center"
                  />
                )}
              </div>
              <Modal.Title className="modal-title my-5">Donaties</Modal.Title>
              <div className="d-flex flex-collumn mx-5 justify-content-center align-items-center">
                  <input
                    className="pl-0"
                    type="checkbox"
                    value="true"
                    {...register("Donations")}
                    onChange={handleDonation}
                    checked={donations}
                    // style={{ marginRight: -30 }}
                  />
                  <label style={{marginRight:30, marginTop:0}} >
                    Wens je donaties te verzamelen voor een goed doel die nauw
                    aan het hart lag van{" "}
                    {userdata !== undefined && userdata.FirstName}?
                  </label>
                </div>
                {/* className="mt-0 pr-5" style={{marginLeft:-70, marginRight:50}} */}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button type="submit" className="bg-transparent border-0">
              <div className="button-save-change-container d-flex justify-content-center">
                <div className="button-save-change">
                  <FiSave className="save-img" />
                  <button>Opslaan</button>
                </div>
              </div>
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default IntroTextModal;
