import React, { useState, useEffect, memo, useReducer, useRef } from "react";
// import { Link } from "react-router-dom";
// import { BiEdit } from "react-icons/bi";
// import Spinner from "react-bootstrap/Spinner";
// import moment from "moment";
// import imageHeader from "../../images/HeaderPaula.jpg";
import Slider from "./Slider";
import Mapp from "../map/Mapp";
import app from "../../helpers/axiosConfig";
import useSession from "../../auth/set-session";
import FormContributions from "./FormContributions";
import Geocode from "react-geocode";
import AddAdmin from "./addAdmin/AddAdmin";
import ShowContribution from "./ShowContribution";
import IntroTextModal from "./updateMemorialPagesModals/IntroTextModal";
// import ObituaryModal from "./updateMemorialPagesModals/ObituaryModal";
import LatLongModal from "./updateMemorialPagesModals/LatLongModal";
// import QuoteModal from "./updateMemorialPagesModals/QuoteModal";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import Spinner from "../../components/Spinner";

import "../../styles/herdenking/Mainherdenkings.scss";

//Svg liever hier aanroepen dan van google drive
import Check from "../../images/check.svg";

export default function MainHerdenkings(props) {
  //memorialPageId gets the Id when user has visited the page via a Link
  const [memorialPageId, setMemorialPageId] = useState();
  const [memorialContrubions, setMemorialContrubitions] = useState([]);
  const [dataUndertaker, setDataUndertaker] = useState({});
  const [image, setImage] = useState([]);
  const [userData, setUserData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [loadingContributions, setLoadingContributions] = useState(false);
  const [admin, isAdmin] = useState(false);
  const [adres, setAdres] = useState("");
  const [introModalShow, setIntroModalShow] = useState(false);
  const [latLongModalShow, setLatLongModalShow] = useState(false);
  const [obituaryModalShow, setObituaryModalShow] = useState(false);
  const [quoteModalShow, setQuoteModalShow] = useState(false);
  const [visited, setVisited] = useState();

  //Used images
  const At =
    "https://lh3.googleusercontent.com/u/0/d/1Wpj7Bwx_OOAnbqO5_TXT2OfYoae-BVFl=w1241-h938-iv1";
  const Apartment =
    "https://lh3.googleusercontent.com/u/0/d/1TGVZ12sN12KXxzGwfPBGX3M7atZIXrsr=w1241-h938-iv1";
  const Support =
    "https://lh3.googleusercontent.com/u/0/d/1P1uR6czLSXgddTRtcBDrinn4yhUeF3IO=w1241-h938-iv1";
  const Www =
    "https://lh3.googleusercontent.com/u/0/d/1USn6me4LmqNz92c-DG7AFB43fQEvtQg7=w1241-h938-iv1";
  const Shoppingcart =
    "https://lh3.googleusercontent.com/u/0/d/1hUuQQtG4X_18pYF9l5fnbBtGW5Ru4yOx=w1241-h938-iv1";
  const Flag =
    "https://lh3.googleusercontent.com/u/0/d/1EMu5DgXNOh373UudNZhtTWFejwgEMITR=w1241-h938-iv1";
  const MoreContributions =
    "https://lh3.googleusercontent.com/u/0/d/1EqljEEaxvMt7x0yinyZ1h7XxnDgtQy4c=w1241-h938-iv1";
  const Edit =
    "https://lh3.googleusercontent.com/u/0/d/1Kg46H5mMVHfHjxTJuBVK3y2GBhG1d6Zi=w1241-h938-iv1";

  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  const userId = useSession();

  //This is for the form used for the donations.
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // memorial with the corresponding Id gets fetched
  // when fetched, we wait for it to get stored in the state
  // when stored, loading will be set back to false and the data will be shown
  useEffect(() => {
    console.log(userData);
    (async () => {
      setMemorialPageId(props.props.match.params.Id);
      try {
        setIsLoading(true);

        if (userId) {
          const data = await app.get(`/api/memorialpagesofuser/${userId}`);
          if (data.data) {
            data.data.map((x) => {
              if (x.Id == memorialPageId) {
                return isAdmin(true);
              }
            });
          }
        }
        if (memorialPageId !== undefined) {
          const data = await app.get(`/api/memorialpages/${memorialPageId}`);
          setUserData(data.data[0] !== undefined && data.data[0]);

          // This will load in the posts after the page has been loaded
          // Prevents the page from breaking
          setLoadingContributions(true);
          const construbitionFetch = await app.get(
            `/api/contribution/${memorialPageId}`
          );
          setMemorialContrubitions(
            construbitionFetch !== undefined && [...construbitionFetch.data]
          );
          setLoadingContributions(false);

          //Get the undertaker with the correct memorial page
          //Use the constant variable instead of the state, the state is not always assigned before this code is run
          if (data.data[0].Undertakers_Id) {
            const fetchData = await app.get(
              `/api/undertaker-with-id/${data.data[0].Undertakers_Id}`
            );
            setDataUndertaker(fetchData.data[0]);
          }

          //Get image for memorialpage with id
          const imageData = await app.get(
            `/api/visualcontent/${memorialPageId}`
          );
          setImage([...imageData.data]);
        }
        setIsLoading(false);
      } catch {}

      //Transforming the address so that we get the latitude and longitude
      Geocode.setApiKey(process.env.REACT_APP_GOOGLE_API_KEY);
      Geocode.setLanguage("nl");
      Geocode.setRegion("be");
      //Google geocoder returns more than one address for given lat/lng, according to google docs, ROOFTOP param returns most accurate result
      Geocode.setLocationType("ROOFTOP");
      await Geocode.fromLatLng(userData.Latitude, userData.Longitude).then(
        (response) => {
          const straatNaam =
            response.results[0].address_components[1].long_name;
          const nummer = response.results[0].address_components[0].long_name;
          setAdres(straatNaam + " + " + nummer);
        },
        (error) => {
          //We always get an error from here because it tries to pass the coordinates before we get them, so they are invalid coordinates.
          //The second time the coordinates get passed through correctly, thats why I am going to leave this error commented.
          //console.error(error);
        }
      );
    })();
  }, [memorialPageId, isAdmin]);

  useEffect(() => {
    (async () => {
      let countLink = window.location.pathname.replaceAll("/", "");
      let visit = await app.get(
        `https://api.countapi.xyz/hit/${countLink}/berchem`
      );
      setVisited(visit.data.value);
    })();
  }, []);

  //This will open a new tab for the user to complete his/her donation
  const onSubmit = (data) => {
    var today = new Date();
    var date =
      today.getFullYear() +
      "-" +
      (today.getMonth() + 1) +
      "-" +
      today.getDate();
    console.log(data, userData.FirstName + userData.LastName + date);
    window.open(
      `https://useplink.com/payment/op0e7VTva1ZvMOunzlkJT/EUR${data.Donation}/${
        userData.FirstName + userData.LastName + date
      }`,
      "_blank"
    );
  };

  // If data is still getting fetched
  // Show a spinner untill the data gets stored in the useStates
  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }

  return (
    <div className="">
      {userData.FirstName ? (
        <div className="herdenkingsruimte-container">
          {/* second-navbar */}
          <Slider
            FirstName={userData.FirstName}
            Visited={visited}
            Undertaker={dataUndertaker}
          />
          {/* admin-button */}
          {admin && (
            <div className="admin-button d-flex justify-content-center">
              <AddAdmin MemorialPageId={memorialPageId} />
            </div>
          )}
          {/* --------------------- intro --------------------- */}
          <section id="home" className="home-section">
            {/* Div with basic information about the memorial page */}

            {/* user-image */}
            <div className="photo-container d-flex me-lg-5">
              {image.map((img) => {
                if (img.Route === "Profile") {
                  return (
                    <img
                      key={img.Source}
                      src={img.Source}
                      alt={img.Name}
                      className="image-profile"
                    />
                  );
                }
                return null;
              })}
            </div>
            {/* text-container */}
            <div className="text-container">
              {/* edit knop */}
              {admin && (
                <div className="edit-icon-herdenkingsruimte-container d-flex ">
                  <img
                    src={Edit}
                    alt="Icoon editeren"
                    onClick={() => setIntroModalShow(true)}
                    className="edit-icon-herdenkingsruimte"
                  />
                </div>
              )}
              {/* user-info */}
              <div className="user-info d-flex">
                <span className="user-name">
                  {userData.FirstName} {userData.LastName}
                </span>
                <div className="age-place">
                  <span className="user-age">
                    {userData.DateOfDeath &&
                      userData.DateOfDeath.split("-")[0] -
                        userData.BirthDate.split("-")[0]}{" "}
                    Jaar
                  </span>
                  <span className="user-place"> {userData.Woonplaats}</span>
                </div>
              </div>
              {/* intro-text */}
              <div className="text-intro text-justify">
                <p>{userData.IntroText}</p>
              </div>
              {/* date-date */}
              <div className="user-date">
                <span>
                  {/* Because we use have an object and not a date I use split to give me the year, month and date.
                    I have an array with all the month names, but some of the month have a leading 0 which messes up the system.
                    So turn it into a Number which deletes the leading 0 where needed.
                 */}
                  &#176;
                  {userData.BirthDate &&
                    userData.BirthDate.split("-")[2].split("T")[0] + " "}
                  {userData.BirthDate &&
                    months[Number(userData.BirthDate.split("-")[1] - 1)] + " "}
                  {userData.BirthDate && userData.BirthDate.split("-")[0] + " "}
                  -
                  {userData.DateOfDeath &&
                    " †" +
                      userData.DateOfDeath.split("-")[2].split("T")[0] +
                      " "}
                  {userData.DateOfDeath &&
                    months[Number(userData.DateOfDeath.split("-")[1] - 1)] +
                      " "}
                  {userData.DateOfDeath &&
                    userData.DateOfDeath.split("-")[0] + " "}
                </span>
              </div>
            </div>
          </section>
          {/* --------------------- contributions-cards --------------------- */}
          <section id="verhalen" className="contribution-section">
            <div className="main-contribution">
              <div className="herdenkigs-main-title d-flex justify-content-center">
                <span>Verhalen over {userData.FirstName}</span>
              </div>
              <ShowContribution
                MemorialPages_Id={memorialPageId}
                Image={image}
                MemorialContributions={memorialContrubions}
                Admin={admin}
                LoadingContribution={loadingContributions}
                UserId={userId}
              />
            </div>
          </section>
          {/* --------------------- contributions-making --------------------- */}
          <section id="bijdragen" className="contributions-making-container">
            {/* <div className="bg-danger"> */}
            <div className="herdenkigs-main-title d-flex justify-content-center">
              <span>Schrijf een verhaal over {userData.FirstName}</span>
            </div>
            <div className="make-contribution">
              <FormContributions
                MemorialPages_Id={memorialPageId}
                MemorialPageName={userData.FirstName}
              />
            </div>
            {/* </div> */}
          </section>
          {/* --------------------- donation --------------------- */}
          {userData.Donations === 1 && (
            <section id="steun" className="donation-section">
              <div className="donation-section-image">
                {/* title */}
                <div className="herdenkigs-main-title d-flex justify-content-center">
                  <span>Eerbetoon aan {userData.FirstName}</span>
                </div>
                {/* form */}
                <div className="d-flex justify-content-center">
                  <div className="donation-form-container d-flex column justify-content-center mb-5">
                    <div className="">
                      <form
                        className="donation-container"
                        onSubmit={handleSubmit(onSubmit)}
                      >
                        <div className="donation-form d-flex">
                          <div className="donation-form-input d-flex">
                            <input
                              type="number"
                              {...register("Donation", {
                                required: true,
                                min: "1",
                              })}
                              className="border-0"
                              placeholder="bvb 15"
                              style={{ fontSize: "1.5rem" }}
                            />
                            <span className="euro-sign mx-3">€</span>
                          </div>
                          <button className="button-donation">
                            Doneer
                            <i className="fas fa-heart ml-2 icon-4x" />
                          </button>
                        </div>
                      </form>
                      <div className="error-message">
                        {errors.Donation &&
                          errors.Donation.type === "required" && (
                            <p style={{ color: "red" }} className="">
                              Gelieve een donatiebedrag in te vullen ❤️
                            </p>
                          )}
                        {errors.Donation && errors.Donation.type === "min" && (
                          <p style={{ color: "red" }} className="">
                            Gelieve een donatiebedrag in te vullen ❤️
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                {/* text */}
                <div className="donation-text-container text-center">
                  <div className="donation-text d-inline-block text-left">
                    <p className="fw-bold h2">
                      Wat zal er gebeuren met jouw donatie?
                    </p>
                    <p className="mb-4">
                      Met jouw donatie kan je de nabestaanden steunen op
                      verschillende wijzen:
                    </p>
                    <p className="donatie-doelen">
                      <img
                        src={Check}
                        alt="Check icoon"
                        width="35rem"
                        className="mr-3"
                      />
                      Goed doel die nauw aan het hart lag van{" "}
                      {userData.FirstName}
                    </p>
                    <p className="donatie-doelen">
                      <img
                        src={Check}
                        alt="Check icoon"
                        width="35rem"
                        className="mr-3"
                      />
                      Juridische en administratieve kosten
                    </p>
                    <p className="donatie-doelen">
                      <img
                        src={Check}
                        alt="Check icoon"
                        width="35rem"
                        className="mr-3"
                      />
                      Begrafeniskosten
                    </p>
                    <p className="donatie-doelen">
                      <img
                        src={Check}
                        alt="Check icoon"
                        width="35rem"
                        className="mr-3"
                      />
                      Rouwcoach
                    </p>
                  </div>
                </div>
              </div>
            </section>
          )}
          {/* --------------------- mortician & map  --------------------- */}
          <section id="begrafenisondernemer" className="map-section">
            <div className="mortician-map-container d-flex">
              {/* edit knop */}
              {admin && (
                <div className="mortician-map-edit-icon">
                  <img
                    src={Edit}
                    alt="Icoon editeren"
                    onClick={() => setLatLongModalShow(true)}
                    className="edit-icon-herdenkingsruimte"
                  />
                </div>
              )}
              {/* title */}
              <div className="herdenkigs-main-title d-flex justify-content-center">
                <span className="">
                  {userData.FirstName} rust hier in vrede
                </span>
              </div>
              {/* mortician-map-info */}
              <div className="d-flex begrafenisondernemer-map">
                <div className="begrafenisondernemer-map-container d-flex container">
                  {userData.Undertakers_Id ? (
                    <div className="mortician-contact col-lg-6 pe-0 background-undertaker">
                      <div className="d-flex justify-content-center p-4">
                        <span className="mortician-name">
                          Begrafenisonderneming: {dataUndertaker.Name}
                        </span>
                      </div>
                      <div className="undertaker-text">
                        <p>
                          <img src={Apartment} alt="Apartment icoon" />{" "}
                          {dataUndertaker.Street} {dataUndertaker.HouseNumber},{" "}
                          {dataUndertaker.ZipCode} {dataUndertaker.City}
                        </p>
                        <p>
                          {dataUndertaker.Mail && (
                            <img src={At} alt="@ icoon" />
                          )}{" "}
                          {dataUndertaker.Mail && dataUndertaker.Mail}
                        </p>
                        <p>
                          {dataUndertaker.PhoneNumber && (
                            <img src={Support} alt="Support icoon" />
                          )}{" "}
                          {dataUndertaker.PhoneNumber &&
                            dataUndertaker.PhoneNumber}
                        </p>
                        <p>
                          {dataUndertaker.Website && (
                            <img src={Www} alt="Www teken" width="35rem" />
                          )}{" "}
                          {dataUndertaker.Website && dataUndertaker.Website}
                        </p>
                        <a
                          href={
                            dataUndertaker.Shop
                              ? dataUndertaker.Shop
                              : "https://steun.vivamemoria.be/"
                          }
                          target="_blank"
                          rel="noreferrer"
                        >
                          <button className="btn-webshop mt-1">
                            <img
                              src={Shoppingcart}
                              alt="Winkelwagen icoon"
                              width="30rem"
                              className="mr-2"
                            />
                            Neem een kijkje in de Webwinkel
                          </button>
                        </a>
                        <Link
                          to={{
                            pathname: `/begrafenisondernemer/${dataUndertaker.Id}`,
                            dataUndertaker: dataUndertaker,
                          }}
                          className="text-decoration-none"
                        >
                          <p className="mortician-page-btn">
                            Bekijk alle herdenkingsruimtes van{" "}
                            {dataUndertaker.Name}
                          </p>
                        </Link>
                      </div>
                    </div>
                  ) : (
                    <div className="d-flex justify-content-center col-lg-6 no-info-option">
                      <h3 className="m-auto text-center">
                        Er is geen begrafenisondernemer
                      </h3>
                    </div>
                  )}
                  <div className="mortician-map col-12 col-lg-6 px-0">
                    {userData.Latitude &&
                    userData.Longitude && (
                      <Mapp
                        className=""
                        Latitude={userData.Latitude}
                        Longitude={userData.Longitude}
                      />
                    ) ? (
                      <Mapp
                        className=""
                        Latitude={userData.Latitude}
                        Longitude={userData.Longitude}
                      />
                    ) : (
                      <div className="d-flex no-map-option">
                        <h3 className="m-auto text-center">
                          Er is geen begraafplaats
                        </h3>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            {/* flag */}
            <div className="flag-report">
              <a
                href={
                  "mailto:info@vivamemoria.be?subject=Rapporteren van een herdenkingsruimte (Id: " +
                  props.props.match.params.Id +
                  ")"
                }
                className="text-decoration-none"
              >
                <img src={Flag} alt="Vlag icoon" width="25rem" /> Rapporteer
                deze herdenkingsruimte
              </a>
            </div>
          </section>
          <IntroTextModal
            show={introModalShow}
            onHide={() => setIntroModalShow(false)}
            userdata={userData}
          />
          <LatLongModal
            show={latLongModalShow}
            onHide={() => setLatLongModalShow(false)}
            userdata={userData}
            undertaker={dataUndertaker}
          />
        </div>
      ) : (
        <div>Wij hebben geen herdenkingspagina met dit id!</div>
      )}
    </div>
  );
}
