import React, { useEffect } from "react";
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
// import PopoverBody from 'react-bootstrap/PopoverBody'
import { FacebookShareButton } from "react-share";

import '../../styles/herdenking/Slider.scss'

export default function Slider(props) {

  //Used images
  const story = 'https://lh3.googleusercontent.com/u/0/d/1OHD6i1NTRFpsgBV6Aw74wh1zaTZD_AeY=w1920-h942-iv1';
  const review = 'https://lh3.googleusercontent.com/u/0/d/1P8gBYhWTlXrPlfJm2H5OnrR_OK9wLRr8=w1920-h942-iv1';
  const donation = 'https://lh3.googleusercontent.com/u/0/d/1i9v5QlfLhMk_6zvt7xDN6yKfMNqXry9R=w1920-h942-iv1';
  const mortician = '	https://lh3.googleusercontent.com/u/0/d/1wCH15faZ0tv-aeMmuIpeTGa18Xgehwaj=w1920-h942-iv1';

  const Lotus = "https://lh3.googleusercontent.com/u/0/d/1SyQPtWQm6a5uN4IcZPoS3U-17l_NciXv=w1241-h938-iv1";
  const Facebook = "https://lh3.googleusercontent.com/u/0/d/1_z2Z33p21gziXAyzndl5Drp5WsHozjm1=w1241-h938-iv1";
  const Email = "https://lh3.googleusercontent.com/u/0/d/1_JA-i5zRMBdar2dalnZtKcM-YL4ox4eU=w1919-h969-iv1";
  const Eye = "https://lh3.googleusercontent.com/u/0/d/1TDYOpKFxliN3CGYyLFTN5pVMtOC0HU5j=w1241-h938-iv1";



  // tooltip boxes
  const flower = (props) => (
    <Tooltip id="button-tooltip-2" className="tooltip" {...props}>
        Bestel een bloem
    </Tooltip>
  );

  const share = (props) => (
    <Tooltip id="button-tooltip" className="text-nowrap" {...props}>
        Delen met vrienden en familie
    </Tooltip>
  );

  //Slider nav for HerdenkingsPage
  return (
    <nav
      className="d-flex m-0 slider-memorial-container fixed-element justify-content-between"
      style={{ zIndex: 500, opacity: "0.8" }}
    > 
      {/* tabs in large desktops*/}
      <div className="mr-auto d-none d-lg-block">
        <ul className="slider-memorial-list p-3">
          <a href="#">
            <li className="slider-memorial-item home">{props.FirstName}</li>
          </a>
          <a href="#verhalen">
            <li className="slider-memorial-item overlijdensbericht">
              Verhalen
            </li>
          </a>
          <a href="#bijdragen">
            <li className="slider-memorial-item locatie">Bijdragen</li>
          </a>
          <a href="#steun">
            <li className="slider-memorial-item verhaal">Steun</li>
          </a>
          <a href="#begrafenisondernemer">
            <li className="slider-memorial-item begrafenisondernemer">
              Begrafenisondernemer
            </li>
          </a>
        </ul>
      </div>

      {/* tabs in small display*/}
      {/* <div className="mr-auto d-block d-lg-none">
        <ul className="slider-memorial-list-icons">
          <div className="d-flex flex-column">
            <a href="#">
              <li className="slider-memorial-item home">
                
                
                <span>{props.FirstName}</span>
              </li>
            </a>
          </div>
          <div className="d-flex flex-column">
            <a href="#verhalen">
              <li className="slider-memorial-item overlijdensbericht">
                <img src={story} alt="" className=""/>
                <pan>Verhalen</pan>
              </li>
            </a>
          </div>
          <div className="d-flex flex-column">
            <a href="#bijdragen">
              <li className="slider-memorial-item locatie">
                <img src={review} alt="" className=""/>
                <span>Bijdragen</span>
              </li>
            </a>
          </div>
          <div className="d-flex flex-column">
            <a href="#steun">
              <li className="slider-memorial-item verhaal">
                <img src={donation} alt="" className=""/>
                <span>Steun</span>
              </li>
            </a>
          </div>
          <div className="d-flex flex-column">
            <a href="#begrafenisondernemer">
              <li className="slider-memorial-item begrafenisondernemer">
                <img src={mortician} alt="" className=""/>
                <span>Begrafenisondernemer</span>
              </li>
            </a>
          </div>
        </ul>
      </div> */}

      {/* icons */}
      <div className="slider-icons-container d-flex ">
        <div className="d-flex slider-icons justify-content-evenly my-auto">
          <a
            href={
              props.FlowerShop
                ? props.FlowerShop
                : "https://steun.vivamemoria.be/"
            }
            target="_blank"
            rel="noreferrer"
          >
            <OverlayTrigger 
                placement="top"
                delay={{ show: 100, hide: 150 }}
                overlay={flower}
              >
                <img
                  src={Lotus}
                  className="mr-4 icon-btn"
                  alt="Foto van een bloem"
                />
            </OverlayTrigger>
          </a>
          <FacebookShareButton
            url={`https://www.vivamemoria.be${window.location.pathname}`}
          > 
            <OverlayTrigger 
              placement="top"
              delay={{ show: 100, hide: 150 }}
              overlay={share}
            >   
              <img
                src={Facebook}
                className="mr-4 icon-btn"
                alt="Foto van een facebook logo"
              />    
            </OverlayTrigger>
            
          </FacebookShareButton>
          <a
            href={
              "mailto:?subject=Neem een kijkje naar de herdenkingsruimte van " +
              props.FirstName +
              " &body=Heb jij de herdenkingsruimte van " +
              props.FirstName +
              " al bezocht? Op Viva Memoria kan je gratis herinneringen en verhalen delen. De persoonlijke pagina van " +
              props.FirstName +
              " vind je hier: " +
              window.location.href +
              "%0A%0AVerzonden met veel liefde via Viva Memoria"
            }
          >
            <OverlayTrigger 
              placement="top"
              delay={{ show: 100, hide: 150 }}
              overlay={share}
              className="tooltip"
            >
              <img
                src={Email}
                className="mr-4 icon-btn"
                alt="Foto van een email"
              />
            </OverlayTrigger>
          
          </a>
          <span className="slider-span">
            <img src={Eye} alt="Foto van een oog"/>
          </span>
        </div>
        <span className="visited h-100 me-lg-5">{props.Visited}</span>
      </div>
    </nav>
  );
}
