import React, { useState, useEffect, useMemo, useCallback } from "react";
import DataListInput from "react-datalist-input";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
// import Stepper from "react-stepper-enhanced";
import app from "../../helpers/axiosConfig";
import useSession from "../../auth/set-session";
import { storage } from "../../firebase/firebase-config";
import Autocomplete from "react-google-autocomplete";

export default function FormPage4(props) {
  const [dataUndertakers, setDataUndertakers] = useState([]);
  const [dataMemorial, setDatamemorial] = useState(props.props.location.state);
  const [selected, setSelected] = useState();
  const [imageProfile, setImageProfile] = useState(dataMemorial.ImageProfile);
  const [imageControlProfile, setImageControlProfile] = useState([]);
  const [imageObituary, setImageObituary] = useState(
    dataMemorial.ImageObituary
  );
  const [imageControlObituary, setImageControlObituary] = useState([]);
  const [itemChecked, setItemChecked] = useState(
    JSON.parse(localStorage.getItem("Form4Checked"))
  );
  const [donationChecked, setDonationChecked] = useState(JSON.parse(localStorage.getItem("DonationChecked")))
  const [itemObject, setItemObject] = useState(
    JSON.parse(localStorage.getItem("Form4"))
      ? JSON.parse(localStorage.getItem("Form4"))
      : {}
  );
  const [isChecked, setIsChecked] = useState(itemChecked);
  const [donations, setDonations] = useState(donationChecked);
  const [adres, setAdres] = useState();

  //getUserId through the session
  const userId = useSession();

  //Used images
  const Step =
    "https://lh3.googleusercontent.com/u/0/d/1Gp6nlLjQ-ZMcYhRPAxRqWx_D-4PE74Jv=w1920-h941-iv1";
  const Step2 =
    "https://lh3.googleusercontent.com/u/0/d/1yCNIcsUjBODM2b30EkG6RbnCIKNLkQIu=w1920-h941-iv1";
  const Step3 =
    "https://lh3.googleusercontent.com/u/0/d/1H5PPjgskCXD_pAAMqlVg5vJb5K_kIepH=w1919-h969-iv1";
  const Editing =
    "https://lh3.googleusercontent.com/u/0/d/1VTa2E7UEtZK47Rhx7NxCoThacj6gEJlR=w1919-h969-iv1";
  const Next =
    "https://lh3.googleusercontent.com/u/0/d/1oCRp62uB_UNpUo_F-EyceKGG36DcW0JT=w1919-h969-iv1";
  const Checked =
    "https://lh3.googleusercontent.com/u/0/d/1NanvLwmfs8iCrUZ52XS9EbzVWgr8ooq6=w1919-h969-iv1";
  const Back =
    "https://lh3.googleusercontent.com/u/0/d/1s9DrVy0Th-BmJIjePKQmEVA3OmZuwNS0=w1241-h938-iv1";
  const Add =
    "https://lh3.googleusercontent.com/u/0/d/1uPKUFDPOsO73SibyGjnKiY6PJXTwkpp5=w1280-h885-iv1";

  let idUndertaker = null;
  let propsData = null;

  //Object with props for admin
  let adminMemorialPage = {
    Admin_Id: 0,
    MemorialPages_Id: 0,
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const history = useHistory();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  useEffect(() => {
    setSelected(undefined);
  }, [isChecked]);

  const handleCheckbox = (e) => {
    const checked = e.target.checked;
    localStorage.setItem("Form4Checked", JSON.stringify(checked));
    if (checked) {
      setIsChecked(true);
    } else {
      setIsChecked(false);
    }
  };

  const handleDonation = (e) =>{
    const checked = e.target.checked;
    localStorage.setItem("DonationChecked", JSON.stringify(checked))
    if (checked){
      setDonations(true)
    } else{
      setDonations(false)
    }
  }

  const onSelect = useCallback((selectedItem) => {
    setSelected(selectedItem);
  }, []);

  const items = useMemo(
    () =>
      dataUndertakers.map((oneItem) => ({
        label: oneItem.Name,
        key: oneItem.Id,
        ...oneItem,
      })),
    [dataUndertakers]
  );

  const onChangeData = (data) => {
    itemObject[data.target.name] = data.target.value;
    localStorage.setItem("Form4", JSON.stringify(itemObject));
  };

  const goBack = async () => {
    history.goBack();
  };

  //Handles uploading the profile image to firebase
  //which then gives us the url to store in our database
  const handleUploadProfile = () => {
    if (imageControlProfile.length === 0) {
      const uploadTask = storage
        .ref(
          `${adminMemorialPage.MemorialPages_Id}/profile/${imageProfile.name}`
        )
        .put(imageProfile);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
        },
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref(`${adminMemorialPage.MemorialPages_Id}/profile/`)
            .child(imageProfile.name)
            .getDownloadURL()
            .then((url) => {
              let visualContent = {
                Name: imageProfile.name,
                Source: url,
                Route: "Profile",
                MemorialPages_Id: adminMemorialPage.MemorialPages_Id,
                Contribution_Id: null,
              };
              //Post visualcontent to the database
              app.post(
                `/post/visualcontent-memorial/`,
                visualContent,
                axiosConfig
              );
            });
        }
      );
    }
  };

  //Handles uploading the obituary image to firebase
  //which then gives us the url to store in our database
  // const handleUploadObituary = () => {
  //   if (imageControlObituary.length === 0) {
  //     const uploadTask = storage
  //       .ref(
  //         `${adminMemorialPage.MemorialPages_Id}/obituary/${imageObituary.name}`
  //       )
  //       .put(imageObituary);
  //     uploadTask.on(
  //       "state_changed",
  //       (snapshot) => {
  //         const progress = Math.round(
  //           (snapshot.bytesTransferred / snapshot.totalBytes) * 100
  //         );
  //       },
  //       (error) => {
  //         console.log(error);
  //       },
  //       () => {
  //         storage
  //           .ref(`${adminMemorialPage.MemorialPages_Id}/obituary/`)
  //           .child(imageObituary.name)
  //           .getDownloadURL()
  //           .then((url) => {
  //             let visualContentOb = {
  //               Name: imageObituary.name,
  //               Source: url,
  //               Route: "Obituary",
  //               MemorialPages_Id: adminMemorialPage.MemorialPages_Id,
  //               Contribution_Id: null,
  //             };
  //             //Post visualcontent to the database
  //             app
  //               .post(
  //                 `/post/visualcontent-memorial/`,
  //                 visualContentOb,
  //                 axiosConfig
  //               )
  //               .then((res) => console.log(res));
  //           });
  //       }
  //     );
  //   }
  // };

  const onSubmit = async (data) => {
    localStorage.removeItem("Form1");
    localStorage.removeItem("Form2");
    localStorage.removeItem("Form3");
    localStorage.removeItem("Form4");
    localStorage.removeItem("Form4Checked");
    localStorage.removeItem("DonationChecked");

    let lat;
    let lng;
    if (adres != null) {
      lat = adres.geometry.location.lat();
      lng = adres.geometry.location.lng();
    } else if (itemObject) {
      lat = itemObject.Latitude;
      lng = itemObject.Longitude;
    }

    if (data.IsUndertaker === "true") {
      //Object for undertaker
      let undertaker = {
        Name: data.Name,
        Street: data.Street,
        HouseNumber: data.HouseNumber,
        BusNumber: data.BusNumber,
        City: data.City,
        ZipCode: data.ZipCode,
        PhoneNumber: data.PhoneNumber,
        Website: data.Website,
      };
      //If there is no selected Id from an existing undertaker this will execute
      //Posting undertaker object with await the making a GET request to get the id of the table that we just made to give memorialpage table the id of the undertaker
      if (!selected) {
        await app.post(`/post/form-undertaker/`, undertaker, axiosConfig);
        await app
          .get(
            `/api/undertaker-with-name/${undertaker.Name}`,
            undertaker,
            axiosConfig
          )
          .then((res) => (idUndertaker = res.data[0].Id));
        // }else if (selected.Id) {
        //   idUndertaker = selected.Id;
      }
    }
    //Checks if there is an undertaker
    //if true => 1 (true), else 0 (false)
    if (data.IsUndertaker) {
      data.IsUndertaker = 1;
    } else {
      data.IsUndertaker = 0;
    }

    //Checks if donations is wanted
    //if true => 1 (true), else 0 (false)
    if (data.Donations) {
      data.Donations = 1;
    } else {
      data.Donations = 0;
    }

    //Object for memorialPage
    let memorialPage = {
      FirstName: dataMemorial.FirstName,
      LastName: dataMemorial.LastName,
      BirthDate: dataMemorial.BirthDate,
      DateOfDeath: dataMemorial.DateOfDeath,
      Obituary: dataMemorial.Obituary,
      Quote: dataMemorial.Quote,
      QuoteAuthor: dataMemorial.QuoteAuthor,
      IntroText: dataMemorial.IntroText,
      Latitude: lat,
      Longitude: lng,
      IsUndertaker: data.IsUndertaker,
      Donations: data.Donations,
      // Undertakers_Id: idUndertaker,
      Undertakers_Id: selected ? selected.Id : idUndertaker,
      Address: adres
        ? adres.formatted_address
        : itemObject
        ? itemObject.Address
        : null,
      Woonplaats: dataMemorial.Woonplaats,
    };

    const admin = {
      RightToAct: dataMemorial.RightToAct,
      Users_Id: userId,
    };

    data.Users_Id = admin.Users_Id;
    data.RightToAct = admin.RightToAct;

    const hasAdminId = await app.get(`/api/admintable-with-id/${userId}`);
    if (hasAdminId.data[0]) {
      adminMemorialPage.Admin_Id = hasAdminId.data[0].Id;
      await app
        .post(`/post/form-memorialpage`, memorialPage, axiosConfig)
        .then(
          (res) => (adminMemorialPage.MemorialPages_Id = res.data.insertId)
        );

      //Check if we have a profile picture
      if (imageProfile !== null) {
        await handleUploadProfile();
      }

      //Check if we have a obituary picture
      // if (imageObituary !== null) {
      //   await handleUploadObituary();
      // }

      //Post admin has memorialpage
      await app
        .post(`/post/admin-has-memorialpage/`, adminMemorialPage, axiosConfig)
        .then(
          history.push({
            pathname: `/herdenkings/${adminMemorialPage.MemorialPages_Id}`,
            memorial: adminMemorialPage.MemorialPages_Id,
          })
        );
    } else {
      //Post admin
      await app
        .post(`/post/form-adminpage`, admin, axiosConfig)
        .then((res) => (adminMemorialPage.Admin_Id = res.data.insertId));
      //Post Memorialpage
      await app
        .post(`/post/form-memorialpage`, memorialPage, axiosConfig)
        .then(
          (res) => (adminMemorialPage.MemorialPages_Id = res.data.insertId)
        );

      //Check if we have a profile picture
      if (imageProfile !== null) {
        await handleUploadProfile();
      }

      //Post admin has memorialpage
      await app
        .post(`/post/admin-has-memorialpage/`, adminMemorialPage, axiosConfig)
        .then(
          history.push({
            pathname: `/herdenkings/${adminMemorialPage.MemorialPages_Id}`,
            memorial: adminMemorialPage.MemorialPages_Id,
          })
        );
    }
  };

  //Get props data
  useEffect(() => {
    (async () => {
      try {
        propsData = props.props.location.state;
        setDatamemorial(propsData);

        const undertakers = await app.get(`/api/all-undertakers`, axiosConfig);
        setDataUndertakers(undertakers.data);

        //Check profile image in database
        const imageDataProfile = await app.get(
          `/api/visualcontent/${adminMemorialPage.MemorialPages_Id}/Profile`
        );
        setImageControlProfile([...imageDataProfile.data]);

        //Check obituary image in database
        const imageDataObituary = await app.get(
          `/api/visualcontent/${adminMemorialPage.MemorialPages_Id}/Obituary`
        );
        setImageControlObituary([...imageDataObituary.data]);
      } catch {}
    })();
  }, []);

  return (
    <div className="herdinkingsmaken-form">
      <div className="form-container-one">
        {/* steps */}
        <div className="form-custom-steps border d-flex align-items-center justify-content-center">
          <div className="steps-img d-flex align-items-center">
            <img
              src={Step}
              alt="Icoon editeren"
              className="d-none d-md-block"
            />
            <div className="edit-btn">
              <img
                src={Checked}
                alt="Icoon checked"
                className="d-none d-md-block"
              />
            </div>
            <span className="mx-3 my-2 seperator d-none d-md-block">|</span>
            <img
              src={Step2}
              alt="Icoon editeren"
              className="d-none d-md-block"
            />
            <div className="edit-btn">
              <img
                src={Checked}
                alt="Icoon checked"
                className=" d-none d-md-block"
              />
            </div>
            <span className="mx-3 seperator d-none d-md-block">|</span>
            <img src={Step3} alt="Icoon editeren" />
            <div className="mx-3 mt-3">
              <span className="h5">Stap 3/3</span>
              <p>Begrafenisondernemer</p>
            </div>
            <div className="edit-btn">
              <img src={Editing} alt="Icoon editeren" />
            </div>
          </div>
        </div>
        {/* form */}
        <form
          className="d-flex flex-column justify-content-center align-items-center"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="forms-container">
            {/* Fourth form */}
            <div className="checkbox-container mt-4">
              <input
                className="mt-3"
                type="checkbox"
                value="true"
                {...register("Donations")}
                onChange={handleDonation}
                defaultChecked="checked"
              />
              <label>Wens je donaties te verzamelen voor een goed doel die nauw aan het hart lag van {dataMemorial.FirstName}?</label>
            </div>
            <div className="form-input-container d-flex flex-column">
              <label className="mt-3">
                Als u graag heeft dat er een link is naar de locatie van de
                begraafplaats gelieve de naam van de begraafplaats of het adres{" "}
                van de begraafplaats in te vullen.
              </label>
              <div className="d-flex flex-column">
                <span className="ms-2">Adres begraafplaats: </span>
                {/* Defaultvalue of the Autcomplete inputs work differently compared to standart text/number inputs. 
                You cannot set the adres back to null/empty because we will always take the last working data.
                I can only check if its undefined or not, and empty means undefined. So the newest location you choose will be used.
                But if the location is undefined the defualt will be the last correct location you entered.
                */}
                <Autocomplete
                  name="Adres"
                  className=""
                  apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                  inputAutocompleteValue="off"
                  language="nl"
                  onPlaceSelected={(place) => {
                    setAdres(place);
                  }}
                  options={{
                    types: ["address"],
                    componentRestrictions: { country: ["be", "nl"] },
                  }}
                  defaultValue={itemObject && itemObject.Address}
                />
              </div>
            </div>
            <div className="mt-4 mb-2">
              <span className="h5">
                Is er een begrafenisondernemer betrokken?
              </span>
            </div>
            <div style={isChecked ? { display: "none" } : { display: "block" }}>
              <div className="mt-3">
                <span className="ms-2 ">Zoek de ondernemer: </span>
                {/* DataListInput has no defaultValue property so this has to be selected every time.*/}
                <DataListInput
                  // inputClassName="form-inputs-datalist"
                  placeholder="Zoek de ondernemer"
                  items={items}
                  clearInputOnClick={true}
                  onSelect={onSelect}
                />
              </div>
            </div>
            {/* checkbox */}
            <div className="checkbox-container">
              <input
                className="mt-3"
                type="checkbox"
                value="true"
                {...register("IsUndertaker")}
                onChange={handleCheckbox}
                checked={isChecked}
              />
              <label>Zelf een ondernemer toevoegen?</label>
            </div>
            {/* second form */}
            {isChecked && selected === undefined ? (
              <div className="d-flex flex-column justify-content-center align-items-center">
                <label className="my-4 h5">
                  Indien de naam niet in de lijst staat gelieve de gegevens in
                  te vullen.
                </label>
                <div className="form-input-container align-items-center">
                  <div className="mb-3">
                    <p className="mb-0">
                      Naam<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="Name"
                      // ref={register({ required: true, maxLength: 35 })}
                      {...register("Name", { required: true, maxLength: 35 })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.Name}
                    />
                    {errors.Name && errors.Name.type === "required" && (
                      <p style={{ color: "red " }}>Naam is verplicht</p>
                    )}
                    {errors.Name && errors.Name.type === "maxLength" && (
                      <p style={{ color: "red " }}>Maximum 35 karakters</p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Straat<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="Street"
                      // ref={register({ required: true })}
                      {...register("Street", { required: true })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.Street}
                    />
                    {errors.Street && errors.Street.type === "required" && (
                      <p style={{ color: "red " }}>Straat is verplicht</p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Huisnummer<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="HouseNumber"
                      // ref={register({ required: true })}
                      {...register("HouseNumber", { required: true })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.HouseNumber}
                    />
                    {errors.HouseNumber &&
                      errors.HouseNumber.type === "required" && (
                        <p style={{ color: "red " }}>Huisnummer is verplicht</p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Busnummer</p>
                    <input
                      name="BusNumber"
                      // ref={register}
                      {...register("BusNumber")}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.BusNumber}
                    />
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Postcode<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="Postcode"
                      type="number"
                      maxLength={4}
                      // ref={register({ required: true })}
                      {...register("Postcode", { required: true })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.ZipCode}
                    />
                    {errors.Postcode && errors.Postcode.type === "required" && (
                      <p style={{ color: "red " }}>Postcode is verplicht</p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Stad<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="City"
                      // ref={register({ required: true })}
                      {...register("City", { required: true })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.City}
                    />
                    {errors.City && errors.City.type === "required" && (
                      <p style={{ color: "red " }}>Stad is verplicht</p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Telefoonnummer<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      // name="PhoneNumber"
                      type="number"
                      // ref={register({ required: true })}
                      {...register("PhoneNumber", { required: true })}
                      onChange={(e) => onChangeData(e)}
                      className="form-inputs"
                      defaultValue={itemObject && itemObject.PhoneNumber}
                    />
                    {errors.PhoneNumber &&
                      errors.PhoneNumber.type === "required" && (
                        <p style={{ color: "red" }}>
                          Telefoonnummer is verplicht
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Website</p>
                    <input
                      // name="Website"
                      // ref={register}
                      {...register("Website")}
                      className="form-inputs"
                      onChange={(e) => onChangeData(e)}
                      defaultValue={itemObject && itemObject.Website}
                    />
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Webwinkel</p>
                    <input
                      // name="WebsiteUrl"
                      // ref={register}
                      {...register("WebsiteUrl")}
                      className="form-inputs"
                      onChange={(e) => onChangeData(e)}
                      defaultValue={itemObject && itemObject.WebsiteUrl}
                    />
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Webwinkel aankoop bloemen</p>
                    <input
                      // name="FlowerWebsiteUrl"
                      // ref={register}
                      {...register("FlowerWebsiteUrl")}
                      className="form-inputs"
                      onChange={(e) => onChangeData(e)}
                      defaultValue={itemObject && itemObject.FlowerWebsiteUrl}
                    />
                  </div>
                </div>
              </div>
            ) : (
              <></>
            )}
            {/* buttons */}
            <div className="btn-sets-container">
              <div className="forms-back-button-container">
                <button type="button" onClick={goBack}>
                  <img src={Back} alt="Icoon pijl links" className="" />
                  Terug
                </button>
              </div>
              <div className="add-button-container">
                <button type="submit" className="btn-create-memorial-page">
                  <img
                    src={Add}
                    alt="Icoon voor aanmaken pagina"
                    width="25rem"
                    className="delete-admin-image mr-2"
                  />
                  Pagina aanmaken
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
