import React, { useState } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import imageCompression from "browser-image-compression";

export default function FormPage2(props) {
  const [image, setImage] = useState(null);
  const [imageUrlForm, setImageUrlForm] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [imageControl, setImageControl] = useState([]);
  const [progress, setProgress] = useState(0);
  const [dataMemorial, setDataMemorial] = useState(props.props.location.state);
  //The data we use to give the input a default value when going back.
  const [item, setItem] = useState(JSON.parse(localStorage.getItem("Form2")));

  //Used images
  const Step =
    "https://lh3.googleusercontent.com/u/0/d/1Gp6nlLjQ-ZMcYhRPAxRqWx_D-4PE74Jv=w1920-h941-iv1";
  const Step2 =
    "https://lh3.googleusercontent.com/u/0/d/1yCNIcsUjBODM2b30EkG6RbnCIKNLkQIu=w1920-h941-iv1";
  const Step3 =
    "https://lh3.googleusercontent.com/u/0/d/1H5PPjgskCXD_pAAMqlVg5vJb5K_kIepH=w1919-h969-iv1";
  const Editing =
    "https://lh3.googleusercontent.com/u/0/d/1VTa2E7UEtZK47Rhx7NxCoThacj6gEJlR=w1919-h969-iv1";
  const Next =
    "https://lh3.googleusercontent.com/u/0/d/1oCRp62uB_UNpUo_F-EyceKGG36DcW0JT=w1919-h969-iv1";
  const Checked =
    "https://lh3.googleusercontent.com/u/0/d/1NanvLwmfs8iCrUZ52XS9EbzVWgr8ooq6=w1919-h969-iv1";
  const Back =
    "https://lh3.googleusercontent.com/u/0/d/1s9DrVy0Th-BmJIjePKQmEVA3OmZuwNS0=w1241-h938-iv1";

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const history = useHistory();

  //Sets the uploaded image into the image state
  const handleChange = async (e) => {
    const img = e.target.files[0];
    const options = {
      maxSizeMb: 20,
    };
    console.log(img)
    if (img) {
      const compressedImg = await imageCompression(img, options);
      if (compressedImg) {
        //Uploaded img max file size set to 10 MB
        //10e6 === 10 MB
        if (compressedImg.size > 20e6) {
          window.alert("Upload een bestand kleiner dan 20 MB");
          return false;
        } else {
          await setImage(compressedImg);
          //Get the image url in a different state and show it when uploaded to form
          setImageUrlForm(URL.createObjectURL(compressedImg));
        }
      }
    } else setImageUrlForm(null);
  };

  //On submit sending data to endpoint
  const onSubmit = async (data) => {
    try {
      let memorialPage = {
        FirstName: dataMemorial.FirstName,
        LastName: dataMemorial.LastName,
        BirthDate: dataMemorial.BirthDate,
        DateOfDeath: dataMemorial.DateOfDeath,
        Obituary: null,
        Quote: data.Quote,
        QuoteAuthor: data.QuoteAuthor,
        IntroText: data.IntroText,
        IsUndertaker: null,
        Undertakers_Id: null,
        RightToAct: dataMemorial.RightToAct,
        ImageProfile: image,
        Woonplaats: dataMemorial.Woonplaats,
        WoonplaatsFull: dataMemorial.WoonplaatsFull,
        ImageUrlForm: imageUrlForm,
      };
      //Set the data in a localstorage to use it as defaultvalue for the inputs when the user comes back to this page.
      localStorage.setItem("Form2", JSON.stringify(memorialPage));
      history.push({
        pathname: "/memorialpage-form-4",
        state: memorialPage,
      });
    } catch (error) {
      console.log(error);
    }
  };

  //When the user goes back to form page 1
  //this function will delete the created data from form page 1
  //user can start again in a clean state
  const goBack = async () => {
    history.goBack();
  };

  return (
    <div className="herdinkingsmaken-form">
      <div className="form-container-one">
        {/* steps */}
        <div className="form-custom-steps border d-flex align-items-center justify-content-center">
          <div className="steps-img d-flex flex-row align-items-center">
            <img 
              src={Step} 
              alt="Icoon editeren"
              className="d-none d-md-block" 
            />
            <div className="edit-btn">
              <img
                src={Checked}
                alt="Icoon checked"
                className=" d-none d-md-block"
              />
            </div>
            <span className="mx-3 my-2 seperator d-none d-md-block">|</span>
            <img src={Step2} alt="Icoon editeren" />
            <div className="mx-3 mt-3">
              <span className="h5">Stap 2/3</span>
              <p>Het begin van een verhaal</p>
            </div>
            <div className="edit-btn">
              <img
                src={Editing}
                alt="Icoon editeren" 
                className=" d-none d-md-block"
              />
            </div>
            <span className="mx-3 seperator d-none d-md-block">|</span>
            <img 
              src={Step3} 
              alt="Icoon editeren" 
              className="d-none d-md-block"/>
          </div>
        </div>
        {/* form */}
        <form
          className="d-flex flex-column justify-content-center align-items-center mt-5"
          onSubmit={handleSubmit(onSubmit)}
          autoComplete="off"
        >
          <div>
            <h1 className="mb-3 forms-title">Wie is {dataMemorial.FirstName}?</h1>
            <p>
              Deel een inspirerende, zinvolle herinnering of gedicht. Erken
              belangrijke mensen in hun leven, passies en doelen waar ze om
              gaven,
              of hoe ze een impact hebben. Min 200 karakters
              <span style={{ color: "red " }}>*</span>:
            </p>
            <textarea className="form-textarea-container"
              type="text"
              {...register('IntroText', {
                required: true,
                maxLength: 2000,
                minLength: 200,
              })}
              placeholder="Bvb. Geboren te Zonhoven op 2 juni 1931 en zachtjes van onsheengegaan in woonzorgcentrum Heilige Catharinate Zonhoven op 9 november 2019. Dankbaar om wat je voor ons bent geweest, bewaren wij jou licht in.
                Paula werd in 1931 geboren in Mechelen. Met studiegenoten richtte ze in 1956 het Mechels Miniatuur Theater op. In de jaren zestig speelde ze een roddeltante in het roemruchte Kapitein Zeppos. Ook stond ze tot haar 50ste in theaters op de planken. In de jaren negentig was ze nog te zien als bazige Tante Jos in de soap Wittekerke. Sleyp was getrouwd met regisseur en acteur Bob Dillen, die in 2011 overleed."
              defaultValue={item && item.IntroText}
            />
            {errors.IntroText && errors.IntroText.type === "maxLength" && (
              <p style={{ color: "red " }}>
                Maximum lengte van 2000 karakters overschreden
              </p>
            )}
            {errors.IntroText && errors.IntroText.type === "minLength" && (
              <p style={{ color: "red " }}>Minimum lengte van 200 karakters</p>
            )}
            <div className="mt-5">
              <label>
                Voeg een profiel foto toe voor bovenaan de herdenkingsruimte
                <span style={{ color: "red " }}>*</span>
              </label>
              <div>
                {progress >= 1 && (
                  <ProgressBar
                    style={{ minWidth: "100%", width: 0 }}
                    value={progress}
                    now={progress}
                    label={`${progress}%`}
                  />
                )}

                {/* Image is not allowed to have a defaultvalue due to security reasons, so the user has readd the image. */}
                <input
                  type="file"
                  name="ProfilePicture"
                  {...register('ProfilePicture', { required: true })}
                  onChange={handleChange}
                  accept=".jpg, .jpeg, .png, .gif"
                />
                {errors.ProfilePicture &&
                  errors.ProfilePicture.type === "required" && (
                    <p style={{ color: "red " }} className="">
                      Profielfoto is verplicht
                    </p>
                  )}
                <br />
                {imageUrlForm && (
                  <img
                    src={imageUrlForm}
                    alt="profile memorial page"
                    className="profile-image"
                  />
                )}
              </div>
            </div>
            {/* buttons */}
            <div className="btn-sets-container">
                <div className="forms-back-button-container">
                  <button
                    type="button"
                    onClick={goBack}
                  >
                    <img
                      src={Back}
                      alt="Icoon pijl links"
                      className=""
                    />
                    Terug
                  </button>
                </div>
                <div className="next-button-container">
                <button type="submit" className="">
                  Volgende stap
                  <img
                    src={Next}
                    alt="Icoon pijl rechts"
                  />
                </button>
                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
