import React, { useState } from "react";
import { useForm } from "react-hook-form";
import app from "../../helpers/axiosConfig";
import { Link, useHistory } from "react-router-dom";
import useSession from "../../auth/set-session";
import moment from "moment";
import Autocomplete from "react-google-autocomplete";

import '../../styles/herdenking/HerdenkingsPaginaAanmakenForms.scss'

export default function FormPage1() {
  const [error, setError] = useState(false);
  const [woonplaats, setWoonplaats] = useState();
  //The data we use to give the input a default value when going back.
  const [item, setItem] = useState(JSON.parse(localStorage.getItem("Form1")));

  //Used images
  const Step =
    "https://lh3.googleusercontent.com/u/0/d/1Gp6nlLjQ-ZMcYhRPAxRqWx_D-4PE74Jv=w1920-h941-iv1";
  const Step2 =
    "https://lh3.googleusercontent.com/u/0/d/1yCNIcsUjBODM2b30EkG6RbnCIKNLkQIu=w1920-h941-iv1";
  const Step3 =
    "https://lh3.googleusercontent.com/u/0/d/1H5PPjgskCXD_pAAMqlVg5vJb5K_kIepH=w1919-h969-iv1";
  const Editing =
    "https://lh3.googleusercontent.com/u/0/d/1VTa2E7UEtZK47Rhx7NxCoThacj6gEJlR=w1919-h969-iv1";
  const Next =
    "https://lh3.googleusercontent.com/u/0/d/1oCRp62uB_UNpUo_F-EyceKGG36DcW0JT=w1919-h969-iv1";
  const Delete =
    "https://lh3.googleusercontent.com/u/0/d/17fuztlk4q5kjK23VW_MXG7_wzPXifLn7=w1241-h938-iv1";

  const history = useHistory();
  const userId = useSession();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues:{
      RightToAct: '' 
    }
  });

  //This will always be equal to the BirthDate input, with a new date as a default.
  const dateOfBirth = watch("BirthDate", new Date());

  //On submit sending data to endpoints
  const onSubmit = async (data) => {
    //If there is a memorialpage with the same firstname/lastname/dob/dod then change the error state, this will add an error.
    let exists = await app.get(
      `/api/memorialpage-name/${data.FirstName}/${data.LastName}/${data.BirthDate}/${data.DateOfDeath}`,
      axiosConfig
    );
    if (exists.data.length > 0) {
      setError(true);
    } else {
      const memorialPage = {
        FirstName: data.FirstName,
        LastName: data.LastName,
        BirthDate: data.BirthDate,
        DateOfDeath: data.DateOfDeath,
        RightToAct: data.RightToAct,
        Woonplaats: woonplaats
          ? woonplaats.address_components[0].long_name
          : item
          ? item.Woonplaats
          : null,
        WoonplaatsFull: woonplaats
          ? woonplaats.formatted_address
          : item
          ? item.WoonplaatsFull
          : null,
      };
      //Set the data in a localstorage to use it as defaultvalue for the inputs when the user comes back to this page.
      localStorage.setItem("Form1", JSON.stringify(memorialPage));
      history.push({
        pathname: "/memorialpage-form-2",
        state: memorialPage,
      });
    }
  };

  return (
    <div className="herdinkingsmaken-form">
      <div className="form-container-one">
        {/* steps */}
        <div className="form-custom-steps border d-flex align-items-center justify-content-center">
          <div className="steps-img d-flex flex-row align-items-center">
            <img src={Step} alt="Icoon voor editeren"/>
            <div className="mx-3 mt-3">
              <span className="h5">Stap 1/3</span>
              <p>Kerngegevens van de herdenkingsruimte</p>
            </div>
            <div className="edit-btn">
              <img
                src={Editing}
                alt="Icoon voor editeren"
              />
            </div>
            <span className="mx-3 my-2 seperator d-none d-md-block">|</span>
            <img src={Step2} alt="Icoon voor editeren" className=" d-none d-md-block"/>
            <span className="mx-3 seperator  d-none d-md-block">|</span>
            <img src={Step3} alt="Icoon voor editeren" className=" d-none d-md-block"/>
          </div>
        </div>
        {/* form */}
        {userId ? (
          <form
            className="d-flex flex-column justify-content-center align-items-center"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="forms-container mt-5">
              <h1 className="mb-0 forms-title">Informatie over de overledene</h1>
              <span style={{ color: "red ", display: "inline-block" }}>
                {error && "Een herdenkingspagina voor deze persoon bestaat al!"}
              </span>
              {/* inputs */}
              <div className="form-input-container">
                <div className="mb-3">
                  <p className="mb-0">
                    Voornaam<span style={{ color: "red" }}>*</span>
                  </p>
                  <input
                    {...register("FirstName", {required: true, maxLength: 30,})}
                    className=""
                    defaultValue={item && item.FirstName}
                  />
                  {errors.FirstName && errors.FirstName.type === "required" && (
                    <p style={{ color: "red " }}>Voornaam is verplicht</p>
                  )}
                  {errors.FirstName &&
                    errors.FirstName.type === "maxLength" && (
                      <p style={{ color: "red " }}>Maximum 30 karakters</p>
                    )}
                </div>
                <div className="mb-3">
                  <p className="mb-0">
                    Achternaam<span style={{ color: "red" }}>*</span>
                  </p>
                  <input
                    type="text"
                    {...register("LastName", { required: true, maxLength: 30 })}
                    className=""
                    defaultValue={item && item.LastName}
                  />
                  {errors.LastName && errors.LastName.type === "required" && (
                    <p style={{ color: "red " }}>Achternaam is verplicht</p>
                  )}
                  {errors.LastName && errors.LastName.type === "maxLength" && (
                    <p style={{ color: "red " }}>Maximum 30 karakters</p>
                  )}
                </div>
                <div className="mb-3">
                  <p className="mb-0">
                    Geboortedatum<span style={{ color: "red" }}>*</span>
                  </p>
                  <input
                    max={moment().format("YYYY-MM-DD")}
                    type="date"
                    {...register("BirthDate",{ required: true })}
                    className=""
                    defaultValue={item && item.BirthDate}
                  />
                  {errors.BirthDate && errors.BirthDate.type === "required" && (
                    <p style={{ color: "red " }} className="ml-3">
                      Geboortedatum is verplicht
                    </p>
                  )}
                </div>
                <div className="mb-3">
                  <p className="mb-0">
                    Overlijdensdatum<span style={{ color: "red" }}>*</span>
                  </p>
                  <input
                    min={dateOfBirth}
                    max={moment().format("YYYY-MM-DD")}
                    type="date"
                    name="DateOfDeath"
                    {...register("DateOfDeath", { required: true })}
                    className=""
                    defaultValue={item && item.DateOfDeath}
                  />
                  {errors.DateOfDeath &&
                    errors.DateOfDeath.type === "required" && (
                      <p style={{ color: "red " }} className="ml-3">
                        Overlijdensdatum is verplicht
                      </p>
                    )}
                </div>
                <div className="mb-3">
                  <p className="mb-0">
                    Wat is jouw band met de overledene:
                    <span style={{ color: "red" }}>*</span>
                  </p>
                  <select
                    defaultValue={item && item.RightToAct}
                    {...register("RightToAct", { required: true })}
                    className=""
                  >
                    <option value="">Kies het verband</option>
                    <option value="Vader">Vader</option>
                    <option value="Moeder">Moeder</option>
                    <option value="Broer">Broer</option>
                    <option value="Zus">Zus</option>
                    <option value="Dochter">Dochter</option>
                    <option value="Zoon">Zoon</option>
                    <option value="Vriend">Vriend(in)</option>
                    <option value="Tante">Tante</option>
                    <option value="Oom">Oom</option>
                    <option value="Kennis">Kennis</option>
                    <option value="Collega">Collega</option>
                    <option value="Begrafenisondernemer">
                      Begrafenisondernemer
                    </option>
                  </select>
                  {errors.RightToAct &&
                    errors.RightToAct.type === "required" && (
                      <p style={{ color: "red " }}>
                        Verband vermelden is verplicht
                      </p>
                    )}
                </div>
                <div className="mb-3">
                  <p className="mb-0">Woonplaats</p>
                  <Autocomplete
                    name="Woonplaats"
                    //apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                    inputAutocompleteValue="off"
                    language="nl"
                    onPlaceSelected={(place) => {
                      setWoonplaats(place);
                    }}
                    options={{
                      types: ["(regions)"],
                      componentRestrictions: { country: ["be", "nl"] },
                    }}
                    className=""
                    defaultValue={item && item.WoonplaatsFull}
                  />
                </div>
              </div>
              {/* buttons */}
              <div className="btn-sets-container">
                <div className="forms-delete-button-container">
                  <a href="./">
                    <button
                      type="button"
                      className=""
                    >
                      <img
                        src={Delete}
                        alt="Icoon vuilbak"
                        className=""
                      />
                      Annuleren
                    </button>
                  </a>
                </div>
                <div className="next-button-container">
                  <button type="submit" className="">
                    Volgende stap
                    <img
                      src={Next}
                      alt="Icoon pijl rechts"
                      className=""
                    />
                  </button>
                </div>
              </div>
            </div>
          </form>
        ) : (
          <div className="d-flex  justify-content-center mt-5">
            <Link className="lees_meer_button" to="/aanmaken-gegevens">
              <h1>Maak uw gegevens aan</h1>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
}
