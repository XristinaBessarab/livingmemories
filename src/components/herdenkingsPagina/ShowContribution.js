import React, { useState, useEffect, memo } from "react";
import app from "../../helpers/axiosConfig";
import Spinner from "../Spinner";
import DeleteContribution from "./deleteContribution/DeleteContribution";
import DeleteContributionAdmin from "./deleteContribution/DeleteContributionAdmin";
import UpdateContribution from "./updateContribution/UpdateContribution";
import DynamicContributionName from "./DynamicContributionName";
import moment from "moment";
import ProfielFotoBijdragen from "./extra/ProfielFotoBijdragen";

import "../../styles/herdenking/ShowContribution.scss";

//We get all the props from the parent 'MainHerdenkings.js' and put them in states
const ShowContribution = (props) => {
  const [memorialPageId, setMemorialPageId] = useState(props.MemorialPages_Id);
  const [memorialContrubions, setMemorialContrubitions] = useState(
    props.MemorialContributions
  );
  const [loadingContributions, setLoadingContributions] = useState(
    props.LoadingContributions
  );
  const [image, setImage] = useState(props.Image);
  const [admin, isAdmin] = useState(props.Admin);
  const [userId, setUserId] = useState(props.UserId);
  const [userData, setUserData] = useState([]);
  const [allContributions, setAllContributions] = useState([]);
  const [limitedContributions, setLimitedContributions] = useState([]);
  const [aantalVerhalen, setAantalVerhalen] = useState(3);

  //Used images
  const Delete =
    "https://lh3.googleusercontent.com/u/0/d/1b7BtT6lO2iV8L6l-REtnibUG9GAwI8VF=w1241-h938-iv1";
  const MoreContributions =
    "https://lh3.googleusercontent.com/u/0/d/1EqljEEaxvMt7x0yinyZ1h7XxnDgtQy4c=w1241-h938-iv1";

  useEffect(() => {
    (async () => {
      //Get the user has contribution tabel using the logged in user and the memorial page id.
      //We are going to use this to show the user his own contribution even if it is set to private.
      const x = await app.get(
        `/api/user-contribution-mId-uId/${memorialPageId}/${userId}`
      );
      setUserData(x.data);

      console.log(memorialContrubions)

      const arr = memorialContrubions;
      await setAllContributions(arr);

      //This is a new array for the regular users, this will only show contributions that are public and their own
      var newArray = memorialContrubions.filter((x) => checkContribution);
      await setLimitedContributions(newArray);
    })();
  }, []);

  const checkContribution = (x) => {
    if (x.Public == 1 || userData.find((a) => a.Contribution_Id == x.Id)) {
      return x;
    }
  };

  const verwijderBijdrage = (x) => {
    (async () => {
      await app
        .delete(`/delete/user-has-contribution/${x.Id}`)
        .then((res) => console.log(res));
      await app
        .delete(`/delete/visualcontent/${x.Id}`)
        .then((res) => console.log(res));
      await app
        .delete(`/delete/contribution/${x.Id}`)
        .then((res) => console.log(res));
      window.location.reload();
    })();
  };

  if (memorialContrubions !== null) {
    if (loadingContributions) {
      return (
        <div
          style={{
            height: "20vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Spinner />
        </div>
      );
    }
    return (
      <div className="">
        <div className="contribution-cards">
          {admin
            ? allContributions.slice(0, aantalVerhalen).map((x) => {
                return (
                  <div>
                    {/* detail on card */}
                    <div className="show-contribution-admin">
                      {/* buttons */}
                      <div className="herdenkingsruimte-buttons">
                        <DeleteContributionAdmin
                          UserId={userId}
                          UserContribution={x}
                        />
                        <UpdateContribution
                          UserId={userId}
                          UserContribution={x}
                          MemorialPageId={memorialPageId}
                        />
                      </div>
                      {/* body-text-info */}
                      <div className="body-text-info">
                        <div className="d-flex">
                          <ProfielFotoBijdragen Id={x.Id} />
                          <div className="card-info">
                            <h5 className="card-title">
                              <DynamicContributionName contributionId={x.Id} />{" "}
                              {x.Relation !== null && `(${x.Relation})`}
                            </h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                              {moment(x.CreatedAt).format("DD-MM-YYYY")}
                            </h6>
                          </div>
                        </div>
                        <div className="show-contribution-text text-justify">
                          <h4 className="fw-bold mt-3">{x.Title}</h4>
                          <p className="card-text">{x.Description}</p>
                        </div>
                      </div>
                      {/* contribution-img */}
                      <div className="show-contribution-image">
                        {image.map((img) => {
                          if (
                            img.Route === "Contributi" &&
                            img.Contribution_Id === x.Id
                          ) {
                            return (
                              <img
                                key={img.Source}
                                src={img.Source}
                                alt={img.Name}
                                className=""
                              />
                            );
                          }
                          return null;
                        })}
                      </div>
                    </div>
                  </div>
                );
              })
            : limitedContributions.slice(0, aantalVerhalen).map((x) => {
                return (
                  <div className="show-contribution-public">
                    {/* buttons */}
                    <div className="herdenkingsruimte-buttons">
                      <DeleteContribution
                        UserId={userId}
                        UserContribution={x}
                      />
                      <UpdateContribution
                        UserId={userId}
                        UserContribution={x}
                        MemorialPageId={memorialPageId}
                      />
                    </div>
                    {/* body-text-info */}
                    <div className="body-text-info">
                      <div className="d-flex">
                        <ProfielFotoBijdragen Id={x.Id} />
                        <div className="card-info">
                          <h5 className="card-title">
                            <DynamicContributionName contributionId={x.Id} />{" "}
                            {x.Relation !== null && `(${x.Relation})`}
                          </h5>
                          <h6 class="card-subtitle mb-2 text-muted">
                            {moment(x.CreatedAt).format("DD-MM-YYYY")}
                          </h6>
                        </div>
                      </div>
                      <div className="show-contribution-text text-justify">
                        <h4 className="fw-bold mt-3">{x.Title}</h4>
                        <p className="card-text">{x.Description}</p>
                      </div>
                    </div>
                    {/* contribution-img */}
                    <div className="show-contribution-image">
                      {image.map((img) => {
                        if (
                          img.Route === "Contributi" &&
                          img.Contribution_Id === x.Id
                        ) {
                          return (
                            <img
                              key={img.Source}
                              src={img.Source}
                              alt={img.Name}
                              className=""
                            />
                          );
                        }
                        return null;
                      })}
                    </div>
                  </div>
                );
              })}
        </div>
        {/* button-more */}
        <div className="button-more-container d-flex justify-content-center">
          <div className="button-more">
            <img src={MoreContributions} alt="Icoon met hartje" className="" />
            <button onClick={() => setAantalVerhalen(aantalVerhalen + 3)}>
              Meer verhalen {memorialContrubions.length}
            </button>
          </div>
        </div>
      </div>
    );
  } else {
    return false;
  }
};

export default ShowContribution;
