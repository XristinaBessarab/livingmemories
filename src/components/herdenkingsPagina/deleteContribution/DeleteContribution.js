import React, { useState, useEffect } from "react";
import app from "../../../helpers/axiosConfig";
import Modal from "react-bootstrap/Modal";
import { Button } from "react-bootstrap";

const DeleteContribution = (props) => {
  const [admin, isAdmin] = useState(false);
  const [show, setShow] = useState(false);
  const [modalShow, setModalShow] = useState(false);

  //Used images
  const Delete = "https://lh3.googleusercontent.com/u/0/d/1b7BtT6lO2iV8L6l-REtnibUG9GAwI8VF=w1241-h938-iv1";

  //Handles the modal opening and closing
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const verwijderBijdrage = () => {
    (async () => {
      await app.delete(
        `/delete/user-has-contribution/${props.UserContribution.Id}`
      );
      await app.delete(`/delete/visualcontent/${props.UserContribution.Id}`);
      await app.delete(`/delete/contribution/${props.UserContribution.Id}`);
      window.location.reload();
    })();
  };

  //Check if the user is an admin of that contribution.
  useEffect(() => {
    (async () => {
      var userContribution = await app.get(
        `/api/user-contribution/${props.UserContribution.Id}`
      );
      if (userContribution.data[0]) {
        if (userContribution.data[0].Users_Id == props.UserId) {
          isAdmin(true);
        }
      }
    })();
  }, [props.UserContribution.Id]);

  return (
    <div>

      {/* dit is voor de contributor, er is ook dezelfde modal voor de admin*/}
      {admin && (
        <>
          <span className="me-2" onClick={handleShow}>
            <img src={Delete} alt="Icoon met vuilbak" width="75rem" />
          </span>
          <Modal size="lg" centered backdrop="static" show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Bijdrage Verwijderen</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h4>Let op!</h4>
              <p>Eens de bijdrage wordt verwijderd, kan het niet meer hersteld worden. Bent u zeker?</p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="danger" onClick={handleClose}>
                Annuleren
              </Button>
              <Button variant="secondary" onClick={verwijderBijdrage}>
                Verwijderen
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      )}
    </div>
  );
};

export default DeleteContribution;
