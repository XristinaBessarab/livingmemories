import React, {useState, useEffect} from 'react'
import app from "../../helpers/axiosConfig";

//We get the owner of the contribution with the id to give a name reference to the contribution show.
const DynamicContributionName = (Id) => {
    const [data, setData] = useState()

    useEffect(() =>{
        (async() =>{
            var temp = await app.get(`/api/user-from-contribution/${Id.contributionId}`)
            setData(temp.data[0])
        })()
    }, [])
    return (
        <b>
            {data && data.FirstName} {data && data.LastName} {data && " "}
        </b>
    )
}

export default DynamicContributionName
