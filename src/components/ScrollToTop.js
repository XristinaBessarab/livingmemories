import React, {useEffect, useState} from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

import '../styles/ScrollToTop.css'


const ScrollToTop = () => {
    // foto
    const topArrow = 'https://lh3.googleusercontent.com/u/0/d/12Se2QJZxrsqknAhhqLKhVj-BSAvzuNY_=w1280-h885-iv1'

    // tooltip box
    const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
        Terug naar boven
    </Tooltip>
    );

    // button redirect to the top
    const [isVisible, setIsVisible] = useState(false);

    // scroll to top
    const toggleVisibility = () => {
        if (window.pageYOffset > 30) {
        setIsVisible(true);
        } else {
        setIsVisible(false)
        }
    };

    const ScrollToTop = () => {
        window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
        })
    }
    
    useEffect(() => {
        // eventlistener
        window.addEventListener('scroll', toggleVisibility);
        // remove the scroll
        return() => {
        window.removeEventListener('scroll', toggleVisibility)
        }
    }, [isVisible])

    
    return (
        <div>
            <OverlayTrigger
                placement="top"
                delay={{ show: 100, hide: 100 }}
                overlay={renderTooltip}
                >
                
                <img src={topArrow} className='btnToTop' onClick={ScrollToTop} alt='scroll naar boven knop'/>

            </OverlayTrigger> 
        </div>
    )
}

export default ScrollToTop
