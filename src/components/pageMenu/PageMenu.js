import React from "react";
import NavBar from "../NavBar";
import Main from "./Main";
import Footer from '../Footer';
import FooterCopyright from "../FooterCopyright";

export default function PageMenu() {
  return (
    <div>
      <NavBar />
      <Main />
      <Footer />
      <FooterCopyright />
    </div>
  );
}
