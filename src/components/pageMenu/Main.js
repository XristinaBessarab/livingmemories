import React from "react";
import { Link } from "react-router-dom";
import { FaCommentAlt } from "react-icons/fa";
import { RiSearchFill } from "react-icons/ri";
import { SiFiles, SiAddthis } from "react-icons/si";

import '../../styles/pageMenu/pageMenu.css'

export default function Main() {
  return (
    <div className="beheer-pagina-container d-flex justify-content-center">
      <div className="beheer-pagina-content">
        <Link to="/memorialpage-form-1" className="beheer-pagina-link text-decoration-none">
          <div className="beheer-pagina-item">
           <SiAddthis className='icon me-2'/>
           Herdenkingsruimte aanmaken
          </div>
        </Link>

        <Link to="/herdenkingspaginas" className="beheer-pagina-link text-decoration-none">
          <div className="beheer-pagina-item">
           <RiSearchFill className='icon me-2'/>
           Alle herdenkingsruimten
          </div>
        </Link>

        <Link to="/mijn-herdenkingspaginas" className="beheer-pagina-link text-decoration-none">
          <div className="beheer-pagina-item">
           <SiFiles className='icon me-2'/>Mijn herdenkingsruimten
          </div>
        </Link>

        <Link to="/mijn-bijdragen" className="beheer-pagina-link text-decoration-none">
          <div className="beheer-pagina-item">
           <FaCommentAlt className='icon me-2'/>Mijn bijdragen aan herdenkingsruimten
          </div>
        </Link>
      </div>
    </div>

    // <div className="container keuzepage">

    //   <Link to="/memorialpage-form-1" className="page-menu-link">
    //     <div className="page-link-container">
    //      <BsPlusSquareFill className='icoonKeuze'/>
    //       Herdenkingsruimte aanmaken
    //     </div>
    //   </Link>

    //   <Link to="/herdenkingspaginas" className="page-menu-link">
    //     <div className="page-link-container">
    //     <BsSearch className='icoonKeuze'/>
    //       Alle herdenkingsruimten
    //     </div>
    //   </Link>

    //   <Link to="/mijn-herdenkingspaginas" className="page-menu-link">
    //     <div className="page-link-container">
    //     <BsFileText className='icoonKeuze'/>
    //       Mijn herdenkingsruimten
    //     </div>
    //   </Link>

    //   <Link to="/mijn-bijdragen" className="page-menu-link">
    //     <div className="page-link-container">
    //     <FaHandHoldingHeart className='icoonKeuze'/>
    //       Mijn bijdragen aan herdenkingsruimten
    //     </div>
    //   </Link>
      
    // </div>

  )
}
