import React from "react";
import { Link } from "react-router-dom";

import "../styles/Footer.css";

const Footer = () => {

  const logo = 'https://lh3.googleusercontent.com/u/0/d/1pjxtevxX7dkN1SmFulbrzY1r1aJT3w6N=w1280-h885-iv1'

  return (
    //  <div className="container_footer">
    //     <div className="text-white footer-container">
    //         <div className=" px-sm-5 mb-3">
    //             <div className="footer-link-wrapper">
    //                 <div className="footer-link-items pl-5">
    //                     <h4><strong>BELEID</strong></h4>
    //                     <Link to='/' className="text-nowrap">Algemene Voorwaarden</Link>
    //                     <Link to='/privacybeleid' className="text-nowrap">Privacybeleid</Link>
    //                     <Link to='/faq' className="text-nowrap">Veelgestelde vragen</Link>
    //                 </div>
    //                 <div className="footer-link-items pl-5 ml-5 item-2">
    //                     <h4 className="text-nowrap"><strong>VOLG ONS</strong></h4>
    //                     <a href='https://www.facebook.com/VivaMemoriaOnline' target='_blank' rel="noreferrer">Facebook</a>
    //                     <a href="https://www.instagram.com/vivamemoriabe/" target='_blank' rel="noreferrer">Instagram</a>
    //                 </div>
    //                 <div className="footer-link-items pl-5">
    //                     <h4><strong>LOCATIE</strong></h4>
    //                     <p className="mb-0 text-nowrap">Corneel van Reethstraat 17</p>
    //                     <p className="mb-0 text-nowrap">2600 Antwerpen</p>
    //                     <p className="mt-3 text-nowrap">+32 483 44 68 14</p>
    //                     <p className="mb-0 text-nowrap">info@vivamemoria.be</p>
    //                 </div>
    //                 {/* <div className="footer-link-items position-relative ">
    //                     <h2 className="position-absolute text-nowrap">VIVA MEMORIA</h2>
    //                 </div> */}
    //                 <div className="footer-link-items position-relative footer-logo" type="image">
    //                     {/* <a href="/"><img className="position-absolute" src={Logo} alt="logo"/></a> */}
    //                     <image src={Logo} alt="Logo"/>
    //                 </div>
    //             </div>
    //         </div>
    //     </div>
    // </div>
    <div className="footer-container text-white">
      <div className="row d-flex w-100 justify-content-around">
        <div className="col-xl-3 col-lg-4 col-sm-12 col-md-6 p-2 mt-4 text-center">
          <h4>
            <strong>BELEID</strong>
          </h4>
          <Link to="/algemene-voorwaarden" className="text-white d-inline-block mt-2">
            Algemene Voorwaarden
          </Link>{" "}
          <br />
          <Link
            to="/privacybeleid"
            className="text-nowrap text-white d-inline-block mt-2"
          >
            Privacybeleid
          </Link>{" "}
          <br />
          <Link
            to="/faq"
            className="text-nowrap text-white d-inline-block mt-2"
          >
            Veelgestelde vragen
          </Link>
        </div>
        <div className="col-xl-3 col-lg-4 col-sm-12 col-md-6 text-center p-2 mt-4">
          <h4 className="text-nowrap">
            <strong>VOLG ONS</strong>
          </h4>
          <a
            href="https://www.facebook.com/VivaMemoriaOnline"
            target="_blank"
            rel="noreferrer"
            className="d-inline-block mt-2 text-white"
          >
            Facebook
          </a>{" "}
          <br />
          <a
            href="https://www.instagram.com/vivamemoriabe/"
            target="_blank"
            rel="noreferrer"
            className="text-white d-inline-block mt-2"
          >
            Instagram
          </a>
        </div>
        <div className="col-xl-3 col-lg-4 col-sm-12 col-md-6 text-center p-2 mt-4">
          <h4>
            <strong>LOCATIE</strong>
          </h4>
          <p className="mb-0 text-nowrap">Corneel van Reethstraat 17</p>
          <p className="mb-0 text-nowrap">2600 Antwerpen</p>
          <p className="mt-2 text-nowrap">+32 483 44 68 14</p>
          <p className="mb-3 text-nowrap">info@vivamemoria.be</p>
        </div>
        <div className="col-xl-3 col-lg-12 text-center col-md-6 col-sm-12">
          <img src={logo} alt="Logo Viva Memoria" />
        </div>
      </div>
    </div>
  );
};

export default Footer;
