import React, { useState, useEffect } from "react";
import app from "../../helpers/axiosConfig";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import placeholder from "../../images/user.png";
import Pagination from "../Pagination";
import Spinner from "../../components/Spinner";

import '../../styles/BegrafenisondernemerPagina.css'

export default function BegrafenisondernemerPagina(props) {
  const [dataUndertaker, setDataUndertaker] = useState();
  const [memorialPages, setMemorialPages] = useState([]);
  const [images, setImages] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  // Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [cardsPerPage] = useState(12);
  // Get current cards
  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = memorialPages.slice(indexOfFirstCard, indexOfLastCard);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  //Used images
  const At =
    "https://lh3.googleusercontent.com/u/0/d/1Wpj7Bwx_OOAnbqO5_TXT2OfYoae-BVFl=w1241-h938-iv1";
  const Apartment =
    "https://lh3.googleusercontent.com/u/0/d/1TGVZ12sN12KXxzGwfPBGX3M7atZIXrsr=w1241-h938-iv1";
  const Support =
    "https://lh3.googleusercontent.com/u/0/d/1P1uR6czLSXgddTRtcBDrinn4yhUeF3IO=w1241-h938-iv1";
  const Www =
    "https://lh3.googleusercontent.com/u/0/d/1USn6me4LmqNz92c-DG7AFB43fQEvtQg7=w1241-h938-iv1";
  const Shoppingcart =
    "https://lh3.googleusercontent.com/u/0/d/1hUuQQtG4X_18pYF9l5fnbBtGW5Ru4yOx=w1241-h938-iv1";

  // months in word
  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  useEffect(() => {
    (async () => {
      try {
        if (props.props.match.params.Id) {
          setIsLoading(true);
          const fetchData = await app.get(
            `/api/undertaker-with-id/${props.props.match.params.Id}`
          );
          setDataUndertaker(fetchData.data[0]);

          let data = await app.get(
            `/api/memorialpages-undertaker/${props.props.match.params.Id}`
          );
          setMemorialPages([...data.data]);
          //Get all images for memorialpages
          const imageData = await app.get(`/api/visualcontent`);
          setImages([...imageData.data]);
          setIsLoading(false);
        }
      } catch {}
    })();
  }, []);

  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }

  return (
    <div>
      {dataUndertaker != undefined ? (
        <div className="begrafenisondernemer-container d-flex flex-column justify-content-center align-items-center ">
          <div
            className="begrafenisondernemer-contact-container my-5"
          >
            <div className="d-flex justify-content-center align-items-center">
              <span className="my-3 h2 fw-bold">{dataUndertaker.Name}</span>
            </div>
            <div className="">
              <p>
                <img src={Apartment} alt="Apartment icoon" />{" "}
                {dataUndertaker.Street} {dataUndertaker.HouseNumber},{" "}
                {dataUndertaker.ZipCode} {dataUndertaker.City}
              </p>
              <p>
                {dataUndertaker.Mail && (
                  <img src={At} alt="@ icoon" />
                )}{" "}
                {dataUndertaker.Mail && dataUndertaker.Mail}
              </p>
              <p>
                {dataUndertaker.PhoneNumber && (
                  <img src={Support} alt="Support icoon" />
                )}{" "}
                {dataUndertaker.PhoneNumber && dataUndertaker.PhoneNumber}
              </p>
              <p>
                {dataUndertaker.Website && (
                  <img src={Www} alt="Www teken" />
                )}{" "}
                <a href={dataUndertaker.Website}>
                  {dataUndertaker.Website && dataUndertaker.Website}
                </a>
              </p>
              <a
                href={
                  dataUndertaker.Shop
                    ? dataUndertaker.Shop
                    : "https://steun.vivamemoria.be/"
                }
                target="_blank"
                rel="noreferrer"
              >
                <button className="btn-webshop mt-1">
                  <img
                    src={Shoppingcart}
                    alt="Winkelwagen icoon"
                    width="30rem"
                    className="mr-2"
                  />
                  Neem een kijkje in de Webwinkel
                </button>
              </a>
            </div>
          </div>
          <span className="title">Uitvaart georganiseerd door {dataUndertaker.Name}</span>
          <div className="herdenkingsruimte">
            {currentCards &&
              currentCards.map((element) => {
                return (
                  <div>
                    <Link
                      to={{
                        pathname: `/herdenkings/${element.Id}`,
                        memorial: element.Id,
                      }}
                      key={element.Id}
                      className="card-link"
                    >
                      <Card key={element.Id} className="card">
                        {images.map((img) => {
                          if (img.MemorialPages_Id === element.Id) {
                            if (img.Source === "") {
                              return (
                                <Card.Img
                                  src={placeholder}
                                  key={img.Id}
                                  className="img"
                                />
                              );
                            } else if (img.Route === "Profile")
                              return (
                                <Card.Img
                                  src={img.Source}
                                  key={img.Id}
                                  className="img"
                                />
                              );
                          }
                        })}
                        <div className="d-flex row justify-content-center">
                          <p className="card-full-name text-center justify-content-center">
                            {element.FirstName} {element.LastName}
                          </p>
                          <p className="card-date text-center">
                            <span className="fw-bold">&#176;</span>
                            {element.BirthDate &&
                              element.BirthDate.split("-")[2].split("T")[0] +
                                " "}
                            {element.BirthDate &&
                              months[
                                Number(element.BirthDate.split("-")[1] - 1)
                              ] + " "}
                            {element.BirthDate &&
                              element.BirthDate.split("-")[0] + " "}
                            -<span className="fw-bold ms-1">&#8224;</span>
                            {element.DateOfDeath &&
                              element.DateOfDeath.split("-")[2].split("T")[0] +
                                " "}
                            {element.DateOfDeath &&
                              months[
                                Number(element.DateOfDeath.split("-")[1] - 1)
                              ] + " "}
                            {element.DateOfDeath &&
                              element.DateOfDeath.split("-")[0] + " "}
                          </p>
                          <p className="card-woonplaats text-center">
                            {element.Woonplaats && element.Woonplaats
                              ? element.Woonplaats
                              : "Geen woonplaats"}
                          </p>
                        </div>
                      </Card>
                    </Link>
                  </div>
                );
              })}
          </div>
          <div className="d-flex pagination">
            <Pagination
              className="col justify-self-center"
              cardsPerPage={cardsPerPage}
              totalCards={memorialPages && memorialPages.length}
              paginate={paginate}
            />
          </div>
        </div>
      ) : (
        <div>Wij hebben geen begrafenisondernemer met dit id</div>
      )}
    </div>
  );
}
