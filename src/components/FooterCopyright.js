import React from "react";
import "../styles/footer/footer.scss";

const FooterCopyright = () => {
  return (
    <footer>
        <div className="footer-copyright">
          <p>&copy; Copyright 2021 The Sunday Crew</p>
        </div>
    </footer>
  );
};

export default FooterCopyright;
