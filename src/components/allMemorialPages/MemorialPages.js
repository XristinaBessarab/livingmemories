import React, { useState, useEffect } from "react";
import app from "../../helpers/axiosConfig";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import Spinner from '../../components/Spinner';
import placeholder from "../../images/user.png";
import Fade from "react-reveal/Fade";
import Search from "../../images/search.png";
import Pagination from '../Pagination';

import "../../styles/SearchBar.css";
import "../../styles/allMemorialPages/allMemorialPages.css";

export default function MemorialPages() {
  const [memorialPage, setMemorialPage] = useState([]);
  const [images, setImages] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [searchName, setSearchName] = useState("");
  const [search, setSearch] = useState("");

  // Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [cardsPerPage] = useState(12);
  // Get current cards
  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = memorialPage.slice(indexOfFirstCard, indexOfLastCard);
  
  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  // months in word
  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);
        //Get all memorialpages
        const data = await app.get("/api/memorialpages");
        setMemorialPage([...data.data]);

        //Get all images for memorialpages
        const imageData = await app.get(`/api/visualcontent`);
        setImages([...imageData.data]);

        setIsLoading(false);
      } catch {}
    })();

    return () => {
      setMemorialPage([]);
    };
  }, []);

  const newSearch = (e) => {
    const name = e.target.value.toLowerCase();
    setSearchName(name);
    
    let result = [];
    result = memorialPage.filter((data) => {
      if (name === "") {
        return setSearch(null);
      }

      let lastname = data.LastName.toLowerCase().search(name) !== -1;
      let firstname = data.FirstName.toLowerCase().search(name) !== -1;
     
      if (firstname && lastname) {
        return firstname + " " + lastname;
      } else if (firstname) {
        return firstname;
      } else return lastname;
      
    });
    setSearch(result);
  };

  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }


  // console.log(memorialPage);
  return (
    <div className="d-flex row herdenkingsruimten">
      <div className="">
        {/* filter and search --------------------------------------------------------*/}
        
        {/* design on Miro */}
        {/* <div className="search-container ">
          <div className="search-content">
            <div className="search-bar-search mb-2">
              <input
                className="search-bar-input border ps-3"
                placeholder="Vind een herdenkingsruimte"
              />
                <RiSearchFill className="search-icon"/>
                <button className="btn-search">
                  <span className="ms-3">Zoeken</span>
                </button>
            </div>
            <div className="search-bar-filter">
                <div>
                  <button type="button" className="btn btn-outline-secondary dropdown-toggle me-3" data-bs-toggle="dropdown" aria-expanded="false">
                    Gemeente
                  </button>
                  <button type="button" className="btn btn-outline-secondary dropdown-toggle me-3" data-bs-toggle="dropdown" aria-expanded="false">
                    Begrafenisondernemer
                  </button>
                </div>
                <button className="btn-cancle">
                  Annuleren
                </button>
            </div>
          </div>
        </div> */}


        {/* current search -------------------------------------------------------- */}
        {/* <SearchBar enkelZoeken={true}/> */}
        <div className="search-container ">
          <form className="d-flex" action="post" onSubmit={newSearch}>
            <div className="d-flex searchbar-content row">
              <input
                className="p-3 border me-3"
                placeholder="Vind een herdenkingsruimte"
                onChange={newSearch}
              />
              {search &&
                search.map((data) => {
                  return (
                    <Fade duration={500}>
                      <Link
                        to={{
                          pathname: `/herdenkings/${data.Id}`,
                          memorial: data.Id,
                        }}
                        className="text-decoration-none"
                      >
                        <li className="dropdown-item" key={data.Id}>
                          {data.FirstName} {data.LastName}
                        </li>
                      </Link>
                    </Fade>
                  );
              })}
            </div>
              <Link
                to={{
                  pathname: `/search/${searchName ? searchName : undefined}`,
                }}
                style={{ textDecoration: "none"}}
              >
                <div className="btn-search">
                  <button className="btn-search-current-design text-decoration-none">
                    <img
                      src={Search}
                      alt="Zoek knop"
                      style={{ width: 19, marginRight: 8, marginBottom: 4 }}
                    />
                    Zoeken
                  </button>
                </div>
              </Link>
          </form>
        </div>

        {/* cards -------------------------------------------------------- */}
        <div className="herdenkingsruimte">
          {currentCards &&
            currentCards.map((element) => {
              return (
                <div>
                  <Link 
                    to={{
                      pathname: `/herdenkings/${element.Id}`,
                      memorial: element.Id,
                    }}
                    key={element.Id} 
                    className="card-link"
                  >
                    <Card
                      key={element.Id}
                      className="card"
                    >
                      {images.map((img) => {
                      if (img.MemorialPages_Id === element.Id) {
                        if (img.Source === "") {
                          return (
                            <Card.Img
                              src={placeholder}
                              key={img.Id}
                              className="img"
                            />
                          );
                        } else if (img.Route === "Profile")
                          return (
                            <Card.Img
                              src={img.Source}
                              key={img.Id}
                              className="img"
                            />
                          );
                      }
                      })}
                      <div className="d-flex row justify-content-center">
                        <p className="card-full-name text-center justify-content-center">
                          {element.FirstName} {element.LastName}
                        </p>
                        <p className="card-date text-center">
                        <span className="fw-bold">&#176;</span>{element.BirthDate && element.BirthDate.split("-")[2].split("T")[0] + " "}
                          {element.BirthDate && months[Number(element.BirthDate.split("-")[1] - 1)] + " "}
                          {element.BirthDate && element.BirthDate.split("-")[0] + " "}
                          -
                        <span className="fw-bold ms-1">&#8224;</span>{element.DateOfDeath && element.DateOfDeath.split("-")[2].split("T")[0] + " "}
                        {element.DateOfDeath && months[Number(element.DateOfDeath.split("-")[1] - 1)] + " "}
                        {element.DateOfDeath && element.DateOfDeath.split("-")[0] + " "}
                        </p>
                        <p className="card-woonplaats text-center">
                          {element.Woonplaats && element.Woonplaats ? element.Woonplaats : "Geen woonplaats"}
                        </p>
                      </div>
                    </Card>
                  </Link>
                </div>
              );
          })}
        </div>
        
        {/* pagination -------------------------------------------------------- */}
        <div className="d-flex pagination">
          <Pagination className="col justify-self-center" cardsPerPage={cardsPerPage} totalCards={memorialPage.length} paginate={paginate} />
        </div>
      </div>
    </div>
  );
}
