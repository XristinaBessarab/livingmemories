import React from 'react'
import ScrollToTop from '../../components/ScrollToTop'

import '../../styles/faq/faq.css'

export default function MainFAQ() {
  // foto's staan op Google Drive
  const arrow = '	https://lh3.googleusercontent.com/u/0/d/1VGSaJe0sbdUspoTad39utq7Nav7Up6ZE=w1280-h885-iv1';
  const FAQpic1 = 'https://lh3.googleusercontent.com/u/0/d/1HviUHmrPFHdnn8HbgvShcuZWgY1jsyIe=w2000-h2370-iv1';

  return (
    <div class='faq row justify-content-center'>
    {/* button redirect to the top */}
      <ScrollToTop />
      <h1 className='faq-title text-center'>Veelgestelde vragen</h1>
      <div className='faq-inhoud row justify-content-center'>
        <div className='titles col-12 col-lg-4 p-4 p-md-5'>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title1'><p className='icon-text'>Worden mijn gegevens conform de AVG behandeld?</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title2'><p className='icon-text'>Hoe beroep ik me op het recht om vergeten te worden?</p></a>
          </div>
        </div>
        <div className='description col-12  col-lg-7'>
          <div className='row justify-content-center'>
            <img src={FAQpic1} alt=''/>
          </div>
          <div className='description-content p-2 p-md-5'>

            <section id='title1' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3 title-in'>Worden mijn gegevens conform de AVG behandeld?</p>
                          <p className='description-text'>Wij zijn gevestigd in Vlaanderen en hechten ook veel belang aan privacy en conformiteit aan de wetgeving voor Algemene verordening gegevensbescherming (AVG).</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title2' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3 title-in'>Hoe beroep ik me op het recht om vergeten te worden?</p>
                          <p className='description-text'>Conform de Europese AVG wetgeving kan je ons ten allen tijde vragen om jouw gegevens te laten verwijderen. U mag hiervoor mailen naar <a href='mailto:info@vivamemoria.be?subject=Ik wil graag mijn gegevens verwijderen' title="Stuur een email op">info@vivamemoria.be</a>. Wij behandelen dan zo spoedig mogelijk uw vraag  en koppelen terug met een bevestiging van succesvolle behandeling van uw  aanvraag.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

          </div>
        </div>
      </div>
    </div>
  )
}