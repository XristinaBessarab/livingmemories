import React, { useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { useForm } from "react-hook-form";
import app from "../../helpers/axiosConfig";
import moment from "moment";
import { useHistory } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Autocomplete from "react-google-autocomplete";

// this file uses the style of HerdenkingsPaginaAanmakenForms.scss TOO
import '../../styles/AanmakenGegeven.scss'


export default function Form() {
  const [show, setShow] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [isChecked, setIsChecked] = useState();
  const [woonplaats, setWoonplaats] = useState();

  //Get user data from Auth0
  const { user, getAccessTokenSilently } = useAuth0();
  const history = useHistory();

  //Used images
  const Add =
    "https://lh3.googleusercontent.com/u/0/d/1uPKUFDPOsO73SibyGjnKiY6PJXTwkpp5=w1280-h885-iv1";
  const Delete =
    "https://lh3.googleusercontent.com/u/0/d/17fuztlk4q5kjK23VW_MXG7_wzPXifLn7=w1241-h938-iv1";

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Getting only digits from AuthId with substring
  let AuthId = user.sub;

  useEffect(() => {
    (async () => {
      const token = await getAccessTokenSilently();
      const data = await app.get(`/get/${AuthId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      //If the user already exists redirect to update information else show a modal that ask for confirmation of conditions.
      if (data.data.length === 0) {
        setShow(true);
      } else {
        history.push("/update-gegevens");
      }
    })();
  }, []);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      Gender: "",
      Language: "",
      Language2: "",
      FoundUs: "",
    },
  });

  const handleCheckbox = (e) => {
    const checked = e.target.checked;
    if (checked) {
      setIsChecked(true);
    } else {
      setIsChecked(false);
    }
  };

  //On submit sending data to endpoint
  const onSubmit = async (data) => {
    const newUser = {
      FirstName: data.FirstName,
      LastName: data.LastName,
      Mail: user.email,
      BirthDate: data.BirthDate,
      ZipCode: data.ZipCode,
      Gender: data.Gender,
      PhoneNumber: data.PhoneNumber,
      FoundUs: data.FoundUs,
      Language: data.Language,
      Auth_Id: AuthId,
      //The user can only submit if he has accepted the terms of conditions
      Gebruiksvoorwaarden: true,
    };

    //Data for sendinblue, sendinblue doesnt accept every email (for example i@i.be) and it also doesn't accept the same email
    const sendinblueData = {
      email: user.email,
      attributes: { FIRSTNAME: data.FirstName, LASTNAME: data.LastName },
      listIds: [4],
    };
    //Headers needed to post to sendinblue
    const sendinblueHeaders = {
      "api-key": process.env.REACT_APP_AUTH0_SENDINBLUE_API_KEY,
      "content-type": "application/json",
    };
    //We post the name and emailadres of the new user to our Sendinblue account.
    app.post(`https://api.sendinblue.com/v3/contacts`, sendinblueData, {
      headers: sendinblueHeaders,
    });

    const buyerData = {
      CompanyName: data.CompanyName,
      InvoiceMail: data.InvoiceMail,
      VAT_Number: data.VAT_Number,
      Residence: woonplaats && woonplaats.address_components[0].long_name,
      ZipCode: data.ZipCode2,
      StreetName: data.StreetName,
      HouseNumber: data.HouseNumber,
      BusNumber: data.BusNumber,
      Country: data.Country,
    };

    //Post the new user to our db
    await app
      .post(`/post`, newUser, axiosConfig)
      .then((res) => (buyerData.Users_Id = res.data.insertId))
      .catch((err) => console.log(err));

    // // Post buyer data to our db
    app
      .post(`/post/buyer`, buyerData, axiosConfig)
      .then(history.push("/"))
      .catch((err) => console.log(err));
  };

  //Go back to dashboard
  const goBack = () => {
    history.goBack();
  };

  //Close the modal
  const handleClose = () => setShow(false);

  //This function will make the button available depending if the user has accepted the conditions.
  const enableButton = (enabled) => setDisabled(!disabled);

  const onError = (err) => {
    console.log(err);
  };

  return (
    <div className="profile-aanmaken-form-container mt-0">
      <div className="form-container-one">
        <form
          onSubmit={handleSubmit(onSubmit, onError)}
          className="d-flex flex-column justify-content-center align-items-center"
        >
          <div className="forms-container">
            <h1>Vul uw gegevens in</h1>
            <div className="form-input-container">
              <div className="mb-3">
                <p className="mb-0">
                  Voornaam<span style={{ color: "red" }}>*</span>
                </p>
                <input
                  {...register("FirstName", {
                    required: true,
                    maxLength: 35,
                  })}

                  className="form-inputs-create"
                />
                {errors.FirstName && errors.FirstName.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Voornaam is verplicht
                  </p>
                )}
                {errors.FirstName && errors.FirstName.type === "maxLength" && (
                  <p style={{ color: "red " }} className="">
                    Maximum 35 karakters
                  </p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">
                  Achternaam<span style={{ color: "red" }}>*</span>
                </p>
                <input
                  {...register("LastName", { required: true, maxLength: 35 })}

                  className="form-inputs-create"
                />
                {errors.LastName && errors.LastName.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Achternaam is verplicht
                  </p>
                )}
                {errors.LastName && errors.LastName.type === "maxLength" && (
                  <p style={{ color: "red " }} className="">
                    Maximum 35 karakters
                  </p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">
                  Geslacht<span style={{ color: "red" }}>*</span>
                </p>
                <select
                  {...register("Gender", { required: true })}

                  className="form-inputs-create"
                >
                  <option value="">Kies een optie</option>
                  <option value="Man">Man</option>
                  <option value="Vrouw">Vrouw</option>
                  <option value="Andere">Andere</option>
                </select>
                {errors.Gender && errors.Gender.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Een optie kiezen is verplicht
                  </p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">
                  Geboortedatum<span style={{ color: "red" }}>*</span>
                </p>
                <input
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  {...register("BirthDate", {
                    required: true,
                  })}

                  className="form-inputs-create"
                />
                {errors.BirthDate && errors.BirthDate.type === "required" && (
                  <p style={{ color: "red " }}>Geboortedatum is verplicht</p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">Telefoonnummer</p>
                <input
                  type="number"
                  {...register("PhoneNumber")}

                  className="form-inputs-create"
                />
              </div>
              <div className="mb-3">
                <p className="mb-0">
                  Postcode<span style={{ color: "red" }}>*</span>
                </p>
                <input
                  {...register("ZipCode", { required: true })}

                  className="form-inputs-create"
                />
                {errors.ZipCode && errors.ZipCode.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Postcode is verplicht
                  </p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">Taal<span style={{ color: "red" }}>*</span></p>
                <select
                  {...register("Language", { required: true })}

                  className="form-inputs-create"
                >
                  <option value="">Kies een optie</option>
                  <option value="Nederlands">Nederlands</option>
                  <option value="Frans">Frans</option>
                  <option value="Engels">Engels</option>
                  <option value="Andere">Andere</option>
                </select>
                {errors.Language && errors.Language.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Een optie kiezen is verplicht
                  </p>
                )}
              </div>
              <div className="mb-3">
                <p className="mb-0">Ons gevonden<span style={{ color: "red" }}>*</span></p>
                <select
                  {...register("FoundUs", { required: true })}

                  className="form-inputs-create"
                >
                  <option value="">Kies een optie</option>
                  <option value="Eerder gebruikt">Eerder gebruikt</option>
                  <option value="Google">Google</option>
                  <option value="Sociale media">Sociale media</option>
                  <option value="Krant">Krant</option>
                  <option value="Andere">Andere</option>
                </select>
                {errors.FoundUs && errors.FoundUs.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Een optie kiezen is verplicht
                  </p>
                )}
              </div>
              <div className="profile-checkbox d-flex justify-content-start m-0 p-0">
                <input
                  type="checkbox"
                  className="mt-1"
                  {...register("Public")}
                  onChange={handleCheckbox}
                  value={0}
                />
                <label className="">
                  Ik vertegenwoordig een bedrijf
                </label>
              </div>
              {isChecked && (
                <>
                  <h1 className="profile-make-title mt-5">Bedrijfsgegevens</h1>
                  <div className="mb-3">
                    <p className="mb-0">
                      Bedrijfsnaam<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("CompanyName", {
                        required: true,
                        maxLength: 60,
                      })}
    
                      className="form-inputs-create"
                    />
                    {errors.CompanyName &&
                      errors.CompanyName.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Bedrijfsnaam is verplicht
                        </p>
                      )}
                    {errors.CompanyName &&
                      errors.CompanyName.type === "maxLength" && (
                        <p style={{ color: "red " }} className="">
                          Maximum 60 karakters
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Bedrijfsmail<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("InvoiceMail", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.InvoiceMail &&
                      errors.InvoiceMail.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Bedrijfsmail is verplicht
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      BTW - nummer<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("VAT_Number", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.VAT_Number &&
                      errors.VAT_Number.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          BTW - nummer is verplicht
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Postcode<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("ZipCode2", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.ZipCode2 && errors.ZipCode2.type === "required" && (
                      <p style={{ color: "red " }} className="">
                        Postcode is verplicht
                      </p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Straatnaam<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("StreetName", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.StreetName &&
                      errors.StreetName.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Straatnaam is verplicht
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Huisnummer<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("HouseNumber", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.HouseNumber &&
                      errors.HouseNumber.type === "required" && (
                        <p style={{ color: "red " }} className="">
                          Huisnummer is verplicht
                        </p>
                      )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Bus</p>
                    <input
                      {...register("BusNumber")}
    
                      className="form-inputs-create"
                    />
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Woonplaats</p>
                    <Autocomplete
                      name="Woonplaats"
                      //apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                      inputAutocompleteValue="off"
                      language="nl"
                      onPlaceSelected={(place) => {
                        setWoonplaats(place);
                      }}
    
                      options={{
                        types: ["(regions)"],
                        componentRestrictions: { country: ["be", "nl"] },
                      }}
                      className="form-inputs-create"
                    />
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">
                      Land<span style={{ color: "red" }}>*</span>
                    </p>
                    <input
                      {...register("Country", { required: true })}
    
                      className="form-inputs-create"
                    />
                    {errors.Country && errors.Country.type === "required" && (
                      <p style={{ color: "red " }} className="">
                        Land vermelden is verplicht
                      </p>
                    )}
                  </div>
                  <div className="mb-3">
                    <p className="mb-0">Taal</p>
                    <select
                      {...register("Language2")}
    
                      className="form-inputs-create"
                    >
                      <option value="">Kies een optie</option>
                      <option value="Nederlands">Nederlands</option>
                      <option value="Frans">Frans</option>
                      <option value="Engels">Engels</option>
                      <option value="Andere">Andere</option>
                    </select>
                  </div>
                </>
              )}
            </div>
            {/* button */}
            <div className="btn-sets-container">
              <div className="forms-delete-button-container">
                <button
                  onClick={goBack}
                  type="button"
                  className=""
                >
                  <img
                    src={Delete}
                    alt="Icoon vuilbak"
                  />
                  Annuleren
                </button>
              </div>
              <div className="add-button-container">
                <button type="submit">
                  <img
                    src={Add}
                    alt="Icoon pijl rechts"
                  />
                  Account aanmaken
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <Modal
        show={show}
        onHide={() => history.push("/")}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Gebruiksvoorwaarden</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Welkom op Viva Memoria Ons privacybeleid vind je hier. Wij hechten
          veel belang aan privacy en conformiteit aan de wetgeving van de
          Algemene Verordening en Gegevensbescherming (AVG):{" "}
          <a href="privacybeleid">Privacybeleid</a>
          <br />
          <br />
          Onze algemene voorwaarden vind je hier:{" "}
          <a href="algemene-voorwaarden">Algemene voorwaarden</a>
        </Modal.Body>
        <Modal.Body>
          Ik ga akkoord met het privacybeleid en de algemene voorwaarden :{" "}
          <input
            id="check"
            type="checkbox"
            onChange={() => enableButton()}
          ></input>
        </Modal.Body>
        <Modal.Footer>
          <Button
            type="submit"
            id="btn"
            variant="danger"
            disabled={disabled}
            onClick={() => handleClose()}
          >
            Accepteren
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
