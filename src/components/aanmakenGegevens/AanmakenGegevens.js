import React from 'react';
import Form from './Form';
import '../../styles/form/form.scss';

//Dit maakt linkt naar de form om de gegevens aan te maken
export default function AanmakenGegevens() {
  return (
    <div>
      <Form />
    </div>
  )
}
