import React, { useState, useEffect } from "react";
import app from "../../helpers/axiosConfig";
import Card from "react-bootstrap/Card";
import Spinner from '../../components/Spinner';
import placeholder from "../../images/memorial/placeholder.jpg";
// import moment from "moment";
// import { Pagination } from 'react-bootstrap';
import { Link } from "react-router-dom";
import useSession from "../../auth/set-session";
import ModalScreen from "./Modal";

import Pagination from '../Pagination'
import '../../styles/MyMemorialPage.scss'

export default function MemorialPages() {
  //States to keep data when component rerenders
  const [modalShow, setModalShow] = useState(false);
  const [modalData, setModalData] = useState({});
  const [memorialPage, setMemorialPage] = useState([]);
  const [images, setImages] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const userId = useSession();

  // Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [cardsPerPage] = useState(6);
  // Get current cards
  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = memorialPage.slice(indexOfFirstCard, indexOfLastCard);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);


  //Multiple function for modal
  const wrapperFunction = (props) => {
    setModalShow(true);
    setModalData(props)
  }

  // months in word
  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  //Max count of characters per Card
  // const MAX_LENGTH = 250;

  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);
        if (userId) {
          const data = await app.get(`/api/memorialpagesofuser/${userId}`);
          setMemorialPage(data.data);

          //Get all images for memorialpages
          const imageData = await app.get(
            `/api/visualcontent-with-user/${userId}`
          );
          setImages([...imageData.data]);
        }
        setIsLoading(false);
      } catch {}
    })();
  }, [userId]);

  // loading animation
  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }

  return (
    <div className="d-flex row my-herdenkingsruimte">
      <div className="">
        <h1 className="col title text-center fw-bold m-0">
          {memorialPage.length > 0
              ? ""
              : "Geen herdenkingspaginas"}
        </h1>
        <div className="herdenkingsruimte">
        {currentCards &&
          currentCards.map((element) => {
            return (
              <div>
              <Link
                to={{
                  pathname: `/herdenkings/${element.Id}`,
                  memorial: element.Id,
                }}
                className="card-link"
              >
                <Card
                  key={element.Id}
                  xs={12}
                  md={2}
                  className="card"
                >
                  {images.map((img, id) => {
                    if (img.MemorialPages_Id === element.Id) {
                      if (img.Source === "") {
                        return (
                          <Card.Img
                            src={placeholder}
                            className="img"
                            key={id}
                          />
                        );
                      } else if (img.Route === "Profile")
                        return (
                          <Card.Img
                            src={img.Source}
                            className="img"
                            key={id}
                          />
                        );
                    }
                  })}
                  <div className="d-flex row justify-content-center">
                    <p className="card-full-name text-center justify-content-center">
                      {element.FirstName} {element.LastName}
                    </p>
                    <p className="card-date text-center">
                    <span className="fw-bold">&#176;</span>{element.BirthDate && element.BirthDate.split("-")[2].split("T")[0] + " "}
                      {element.BirthDate && months[Number(element.BirthDate.split("-")[1] - 1)] + " "}
                      {element.BirthDate && element.BirthDate.split("-")[0] + " "}
                      -
                    <span className="fw-bold ms-1">&#8224;</span>{element.DateOfDeath && element.DateOfDeath.split("-")[2].split("T")[0] + " "}
                    {element.DateOfDeath && months[Number(element.DateOfDeath.split("-")[1] - 1)] + " "}
                    {element.DateOfDeath && element.DateOfDeath.split("-")[0] + " "}
                    </p>
                    <p className="card-woonplaats text-center">
                      {element.Woonplaats}
                      {/* this is hardcode ==> remove */}
                      Ekeren
                    </p>
                  </div>
                </Card>
              </Link>
              {/* button should be out of Card component otherwise can not be deleted. */}
              <div className="d-flex justify-content-center">
                <button 
                  type="button" 
                  className="verwijderen-button" 
                  onClick={() => wrapperFunction(element)}
                >
                  Verwijderen
                </button>
              </div>
              </div>
            );
          })
        }
        <ModalScreen
          show={modalShow}
          onHide={() => setModalShow(false)}
          memoriapagedata={modalData}
          id={userId}
        />
        </div>
        <div className="d-flex pagination">
{/* 
            <Pagination className="col justify-self-center ">
              <Pagination.First />
              <Pagination.Prev />
              <Pagination.Item>{1}</Pagination.Item>
              <Pagination.Ellipsis />

              <Pagination.Item>{10}</Pagination.Item>
              <Pagination.Item>{11}</Pagination.Item>
              <Pagination.Item active>{12}</Pagination.Item>
              <Pagination.Item>{13}</Pagination.Item>
              <Pagination.Item disabled>{14}</Pagination.Item>

              <Pagination.Ellipsis />
              <Pagination.Item>{20}</Pagination.Item>
              <Pagination.Next />
              <Pagination.Last />
            </Pagination> */}


            <Pagination className="col justify-self-center" cardsPerPage={cardsPerPage} totalCards={memorialPage.length} paginate={paginate} />

        </div>
      </div>
    </div>
  );
}
