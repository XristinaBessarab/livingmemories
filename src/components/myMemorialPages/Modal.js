import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import app from "../../helpers/axiosConfig";
import { useHistory } from "react-router-dom";
import useSession from "../../auth/set-session"

//Shows a Modal screen when the 'Verwijder Account' button gets pressed
export default function ModalScreen(props) {
  const memorialPageData = props.memoriapagedata;
  const memorialPageId = props.memoriapagedata.Id;
  const userId = useSession();
  const history = useHistory();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Node path for user delete with Id
  const onSubmit = async () => {
    await Promise.all([
      app.delete(`/delete/visualcontent-delete/${memorialPageId}`, axiosConfig),
      app.delete(`/delete/admin-has-memorialpage-delete-only-memorialpageid/${memorialPageId}`, axiosConfig),
      app.delete(`/delete/contribution-has-contribution-table/${memorialPageId}/${userId}`, axiosConfig),
      app.delete(`/delete/contribution-delete/${memorialPageId}`, axiosConfig),
      app.delete(
        `/delete/memorialpages/${memorialPageId}`,
        axiosConfig
      ),
    ]).then(res => history.push("/mijn-herdenkingspaginas"))
  };
  
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Verwijder {memorialPageData.FirstName} {memorialPageData.LastName}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Let op!</h4>
        <p>
          Eens u {memorialPageData.FirstName} {memorialPageData.LastName} verwijderd, zal alle bijhorende data
          gewist worden. Bent u zeker?
        </p>
      </Modal.Body>
      <form onSubmit={onSubmit}>
        <Modal.Footer>
          <Button variant="danger" onClick={props.onHide}>
            Annuleren
          </Button>
          <Button variant="secondary" onClick={(props.onHide, onSubmit)}>
            Verwijderen
          </Button>
        </Modal.Footer>
      </form>
    </Modal>
  );
}
