import React, { useState, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import Spinner from '../../components/Spinner';
import app from "../../helpers/axiosConfig";
import ModalScreen from "./Modal";
import useSession from "../../auth/set-session";
import UpdateGegevensModal from "./UpdateGegevensModal";
import UpdateFactuurModal from "./UpdateFactuurModal";

import '../../styles/profile.scss'

export default function Gegevens() {
  //States to keep data when component rerenders
  const [modalShow, setModalShow] = useState(false);
  const [userData, setUserData] = useState({});
  const [buyerData, setBuyerData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [gegevensModalShow, setGegevensModalShow] = useState(false);
  const [factuurModalShow, setFactuurModalShow] = useState(false);
  const userId = useSession();

  //Getting user info from Auth0
  const { user } = useAuth0();

  //We need this to show the month to the user
  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  //Used images
  const Thrash =
    "https://lh3.googleusercontent.com/u/0/d/1qyyNRNlMKX_1d3BDwK9PQDDOpaX5R9lD=w1919-h969-iv1";
  const Edit =
    "https://lh3.googleusercontent.com/u/0/d/1Kg46H5mMVHfHjxTJuBVK3y2GBhG1d6Zi=w1241-h938-iv1";
  //Get userId from session to
  //If userId is present, fetch data from mysql
  //then put the data in states
  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);
        if (userId) {
          const data = await app.get(`get/userdata/${userId}`);
          const buyer = await app.get(`get/buyerdata/${userId}`);
          await setUserData(data.data[0]);
          setBuyerData(buyer.data[0]);
          setIsLoading(false);
        }
      } catch {}
    })();
  }, [userId]);

  if (isLoading && userId) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }
  return (
    <div className="profile-company-container d-flex flex-column justify-content-center">
      {/* profile */}
      <div className="profile-container d-flex flex-column justify-content-center">
        {/* profile-photo and fullname */}
        <div className="profile-photo-container">
          <img
            src={user.picture}
            alt={user.nickname}
          />
          <h1 className="profile-title">
            {userData && userData.FirstName} {userData && userData.LastName}
          </h1>
        </div>
        {/* edit-icon */}
        <div className="profile-edit-icon-container">
          <img
            src={Edit}
            alt="Icoon editeren"
            onClick={() => setGegevensModalShow(true)}
          />
        </div>
        {/* profile-info */}
        <div className="profile-info">
          <div className="profile-content">
            <p><span className="fw-bold">E-mailadres:</span> {userData && userData.Mail}</p>
            <p><span className="fw-bold">Telefoonnummer:</span> {userData && userData.PhoneNumber}</p>
            <p><span className="fw-bold">Postcode:</span> {userData && userData.ZipCode}</p>
            <p>
            <span className="fw-bold">Geboortedatum:</span>{" "}
              {userData.BirthDate &&
                userData.BirthDate.split("-")[2].split("T")[0] + " "}
              {userData.BirthDate &&
                months[Number(userData.BirthDate.split("-")[1] - 1)] + " "}
              {userData.BirthDate && userData.BirthDate.split("-")[0] + " "}
            </p>
            <p><span className="fw-bold">Taal:</span> {userData && userData.Language}</p>
          </div>
        </div>
      </div>
      {/* company */}
      <div className="company-container">
        <div className="title-container">
          <h1>Factuurgegevens</h1>
        </div>
        {/* edit-icon */}
        <div className="company-edit-icon-container">
          <img
            src={Edit}
            alt="Icoon editeren"
            onClick={() => setFactuurModalShow(true)}
          />
        </div>
        {/* company-info */}
        <div className="company-info">
          <div className="company-content">
            <div className="company-name">
              <span className="item"><span className="fw-bold">Bedrijfsnaam:</span> {buyerData && buyerData.CompanyName}</span>
              <span className="item"><span className="fw-bold">Bedrijfsmail:</span> {buyerData && buyerData.InvoiceMail}</span>
              <span className="item"><span className="fw-bold">BTW - nummer:</span> {buyerData && buyerData.VAT_Number}</span>
            </div>
            <div className="company-address">
              <span><span className="fw-bold">Straatnaam:</span> {buyerData && buyerData.StreetName}</span>
              <span><span className="fw-bold">Huisnummer:</span> {buyerData && buyerData.HouseNumber}</span>
              <span><span className="fw-bold">Bus:</span> {buyerData && buyerData.BusNumber}</span>
              <span><span className="fw-bold">Postcode:</span> {buyerData && buyerData.ZipCode}</span>
              <span><span className="fw-bold">Woonplaats:</span>{buyerData && buyerData.Residence}</span>
              <span><span className="fw-bold">Land:</span> {buyerData && buyerData.Country}</span>
            </div>
          </div>
        </div>
      </div>
      {/* delete button */}
      <div className="delete-button-container">
        {userData.FirstName ? (
          <div className="profile-delete-button">
            <button
              onClick={() => setModalShow(true)}
            >
              <img
                src={Thrash}
                alt="Icoon vuilbak"
                width="25rem"
                className=""
              />
              Account verwijderen
            </button>
          </div>
        ) : (
          <>
            <button
              disabled
              onClick={() => setModalShow(true)}
            >
              <img
                src={Thrash}
                alt="Icoon vuilbak"
                width="25rem"
                className=""
              />
              Account verwijderen
            </button>
            <span className="delete-alert">
              U kan uw profiel verwijderen eens er gegevens zijn aangemaakt.
            </span>
          </>
        )}

        <ModalScreen
          show={modalShow}
          onHide={() => setModalShow(false)}
          userdata={userData}
          id={userId}
        />
        <UpdateGegevensModal
          show={gegevensModalShow}
          onHide={() => setGegevensModalShow(false)}
          userdata={userData}
          id={userId}
        />
        <UpdateFactuurModal
          show={factuurModalShow}
          onHide={() => setFactuurModalShow(false)}
          buyerData={buyerData}
          id={userId}
        />
      </div>
    </div>
  );
}
