import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import moment from "moment";
import Autocomplete from "react-google-autocomplete";
import useSession from "../../auth/set-session";
import { useHistory } from "react-router-dom";
import app from "../../helpers/axiosConfig";
import Button from "react-bootstrap/Button";
import { FiSave } from "react-icons/fi";

// this file uses the style of IntroTextModal

const UpdateFactuurModal = (props) => {
  const [buyerData, setBuyerData] = useState(props.buyerData);
  const [woonplaats, setWoonplaats] = useState();

  const history = useHistory();
  const userId = useSession();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Used images
  // const save =
  //   "https://lh3.googleusercontent.com/u/0/d/1de8T0Tpbavgyhk6mDCru1d1zZJVYNhDh=w1919-h969-iv1";

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    //Here I check if the user already has a buyerId or not.
    if (buyerData.Id) {
      data.Residence = woonplaats && woonplaats.address_components[0].long_name;
      data.Id = buyerData.Id;
      app
        .put(`/put/invoice/${data.Id}`, data, axiosConfig)
        .then(history.push("/mijn-gegevens"));
    } else {
        data.Users_Id = userId;
      //Post buyer data to our db
      app
        .post(`/post/buyer`, data, axiosConfig)
        .then(history.push("/mijn-gegevens"))
        .catch((err) => console.log(err));
    }
  };

  return (
    <div>
      <Modal backdrop="static" size="lg" {...props}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <div className="update-form d-flex flex-column justify-content-center align-items-center">
              <Modal.Title className="modal-title">
               Bedrijfsgegevens
              </Modal.Title>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Bedrijfsnaam: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.CompanyName}
                  {...register("CompanyName", { required: true, maxLength: 30 })}
                />
                {errors.CompanyName && errors.CompanyName.type === "required" && (
                  <span style={{ color: "red " }}>Bedrijfsnaam is verplicht</span>
                )}
                {errors.CompanyName &&
                  errors.CompanyName.type === "maxLength" && (
                    <p style={{ color: "red " }}>Maximum 30 karakters</p>
                  )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Bedrijfsmail: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.InvoiceMail}
                  {...register("InvoiceMail", { required: true })}
                />
                {errors.InvoiceMail && errors.InvoiceMail.type === "required" && (
                  <span style={{ color: "red " }}>Bedrijfsmail is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  BTW - nummer: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.VAT_Number}
                  {...register("VAT_Number", { required: true })}
                />
                {errors.VAT_Number && errors.VAT_Number.type === "required" && (
                  <span style={{ color: "red " }}>BTW - nummer is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Straatnaam: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.StreetName}
                  {...register("StreetName", { required: true })}
                />
                {errors.StreetName && errors.StreetName.type === "required" && (
                  <span style={{ color: "red " }}>Straatnaam is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Huisnummer: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.HouseNumber}
                  {...register("HouseNumber", { required: true })}
                />
                {errors.HouseNumber && errors.HouseNumber.type === "required" && (
                  <span style={{ color: "red " }}>Huisnummer is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Bus:
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.BusNumber}
                  {...register("BusNumber")}
                />
                {errors.BusNumber && errors.BusNumber.type === "required" && (
                  <span style={{ color: "red " }}>Bus is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Postcode: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.ZipCode}
                  {...register("ZipCode", { required: true })}
                />
                {errors.ZipCode && errors.ZipCode.type === "required" && (
                  <span style={{ color: "red " }}>Postcode is verplicht</span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4">
                  Woonplaats:
                </label>
                <Autocomplete
                  name="Woonplaats"
                  //apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
                  inputAutocompleteValue="off"
                  language="nl"
                  onPlaceSelected={(place) => {
                    setWoonplaats(place);
                  }}
                  defaultValue={buyerData && buyerData.Residence}
                  options={{
                    types: ["(regions)"],
                    componentRestrictions: { country: ["be", "nl"] },
                  }}
                  className="form-inputs-create"
                />
              </div>
              <div className="d-flex flex-column mb-4">
                <label className="mb-0 mt-4">
                  Land: <span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={buyerData && buyerData.Country}
                  {...register("Country", { required: true })}
                />
                {errors.Country && errors.Country.type === "required" && (
                  <span style={{ color: "red " }}>Land is verplicht</span>
                )}
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button 
              type="submit" 
              className="bg-transparent border-0"
            >
              <div className="button-save-change-container d-flex justify-content-center">
                <div className="button-save-change">
                  <FiSave className="save-img"/>
                  <button>opslaan</button>
                </div>
              </div>
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default UpdateFactuurModal;
