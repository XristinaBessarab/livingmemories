import React, { useState} from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import moment from "moment";
import app from "../../helpers/axiosConfig";
import { useHistory } from "react-router-dom";
import useSession from "../../auth/set-session";
import Button from "react-bootstrap/Button";
import { FiSave } from "react-icons/fi";

// this file uses the style of IntroTextModal 

const UpdateGegevensModal = (props) => {
  const [userdata, setuserdata] = useState(props.userdata);

  const history = useHistory();
  const userId = useSession();

  //DateOfBirth default value for the input.
  const dob = moment(userdata.BirthDate).format("YYYY-MM-DD");

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Used images
  // const save =
  //   "https://lh3.googleusercontent.com/u/0/d/1de8T0Tpbavgyhk6mDCru1d1zZJVYNhDh=w1919-h969-iv1";

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
      defaultValues: {
          Gender: '',
      }
  });

  const onSubmit = (data) => {
      data.Id = userId;
    app
      .put(`/put/${userId}`, data, axiosConfig)
      .then(history.push("/mijn-gegevens"))
    .then(res => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <div>
      <Modal backdrop="static" size="lg" {...props}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <div className="update-form d-flex flex-column justify-content-center align-items-center">
              <Modal.Title className="modal-title">
                Persoonlijke informatie
              </Modal.Title>
              <div className="d-flex flex-column">
                <label className="mt-4">
                  Voornaam<span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={userdata != undefined && userdata.FirstName}
                  {...register("FirstName", { required: true, maxLength: 30 })}
                />
                {errors.FirstName && errors.FirstName.type === "required" && (
                  <span style={{ color: "red " }}>Voornaam is verplicht</span>
                )}
                {errors.FirstName && errors.FirstName.type === "maxLength" && (
                  <p style={{ color: "red " }}>Maximum 30 karakters</p>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mt-4">
                  Achternaam<span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={userdata != undefined && userdata.LastName}
                  {...register("LastName", { required: true, maxLength: 30 })}
                />
                {errors.LastName && errors.LastName.type === "required" && (
                  <span style={{ color: "red " }}>Achternaam is verplicht</span>
                )}
                {errors.LastName && errors.LastName.type === "maxLength" && (
                  <p style={{ color: "red " }}>Maximum 30 karakters</p>
                )}
              </div>
              <div className="select-container d-flex flex-column mb-2">
                <label className="mt-0">
                  Geslacht<span style={{ color: "red" }}>*</span>
                </label>
                <select
                  {...register("Gender", { required: true })}
                  className="update-form-contribution-select"
                  defaultValue={userdata && userdata.Gender}
                >
                  <option value="">Kies een optie</option>
                  <option value="Man">Man</option>
                  <option value="Vrouw">Vrouw</option>
                  <option value="Andere">Andere</option>
                </select>
                {errors.Gender && errors.Gender.type === "required" && (
                  <p style={{ color: "red " }} className="">
                    Een optie kiezen is verplicht
                  </p>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mt-4">
                  Geboortedatum<span style={{ color: "red" }}>*</span>
                </label>
                <input
                  max={moment().format("YYYY-MM-DD")}
                  type="date"
                  className="form-inputs-create"
                  defaultValue={dob}
                  {...register("BirthDate", { required: true })}
                />
                {errors.BirthDate && errors.BirthDate.type === "required" && (
                  <span style={{ color: "red " }}>
                    Geboortedatum is verplicht
                  </span>
                )}
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4" style={{ fontSize: 15 }}>
                  Telefoonnummer
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={userdata != undefined && userdata.PhoneNumber}
                  {...register("PhoneNumber")}
                />
              </div>
              <div className="d-flex flex-column">
                <label className="mb-0 mt-4" style={{ fontSize: 15 }}>
                  Postcode<span style={{ color: "red" }}>*</span>
                </label>
                <input
                  type="text"
                  className="form-inputs-create"
                  defaultValue={userdata != undefined && userdata.ZipCode}
                  {...register("ZipCode", { required: true })}
                />
                {errors.ZipCode && errors.ZipCode.type === "required" && (
                  <span style={{ color: "red " }}>Postcode is verplicht</span>
                )}
              </div>
              <div className="select-container d-flex flex-column">
                <label className="mt-0">Taal</label>
                <select
                  {...register("Language")}
                  className="update-form-contribution-select"
                  defaultValue={userdata && userdata.Language}
                >
                  <option value="">Kies een optie</option>
                  <option value="Nederlands">Nederlands</option>
                  <option value="Frans">Frans</option>
                  <option value="Engels">Engels</option>
                  <option value="Andere">Andere</option>
                </select>
              </div>
              <div className="select-container d-flex flex-column mb-4">
                <label className="mt-0">Ons gevonden</label>
                <select
                  {...register("FoundUs")}
                  className="update-form-contribution-select"
                  defaultValue={userdata && userdata.FoundUs}
                >
                  <option value="">Kies een optie</option>
                  <option value="Eerder gebruikt">Eerder gebruikt</option>
                  <option value="Google">Google</option>
                  <option value="Sociale media">Sociale media</option>
                  <option value="Krant">Krant</option>
                  <option value="Andere">Andere</option>
                </select>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button 
              type="submit" 
              className="bg-transparent border-0"
            >
              <div className="button-save-change-container d-flex justify-content-center">
                <div className="button-save-change">
                  <FiSave className="save-img"/>
                  <button>opslaan</button>
                </div>
              </div>
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default UpdateGegevensModal;
