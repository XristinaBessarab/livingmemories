import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import app from "../../helpers/axiosConfig";
import { useForm } from "react-hook-form"
import { useAuth0 } from "@auth0/auth0-react";
import axios from 'axios';

//Shows a Modal screen when the 'Verwijder Account' button gets pressed
export default function ModalScreen(props) {
  const { logout } = useAuth0();

  //Getting user info from Auth0
  const { user } = useAuth0();

  //Set Data from props
  const userData = props.userdata;

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const {
    handleSubmit,
    register
  } = useForm();

  //Deleting user from Auth0db and changing DeletionReason collumn in mysql db.
  const onSubmit = async (data) => {
    const AuthId = user.sub;
    //Update user auth id so that we know he has deleted his account
    if (data.Reason === ""){
      //We have to change the authId, the gmail and facebook users get the same id.
      //Reregistering gmail/facebook accounts will otherwise use the existing data in the db instead of asking for new information to the user.
      userData.Auth_Id = "Deleted";
      userData.DeletionReason = "Deleted";
    } else{
      userData.Auth_Id = "Deleted";
      userData.DeletionReason = data.Reason
    }
    await app
      .put(`/put/${userData.Id}`, userData, axiosConfig)
      .catch((err) => console.log(err));

    await axios.delete(process.env.REACT_APP_AUTH0_DELETE_USER + AuthId, {
      headers: {
        'Authorization': process.env.REACT_APP_AUTH0_TOKEN
      }
    }).then(res => console.log(res)).catch(err => console.log(err))

    logout({ returnTo: window.location.origin });
    localStorage.removeItem("@id");
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Modal.Header  className="d-flex flex-column align-items-center">
          <Modal.Title  id="contained-modal-title-vcenter">
            Verwijder mijn account
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4 style={{color: "red"}}>Let op!</h4>
          <p>
            Eens u uw profiel verwijderd, zal alle bijhorende data gewist worden.
            Bent u zeker?
          </p>
          <textarea className="w-100" style={{width: 500, height:150}} placeholder="We begrijpen graag hoe we Viva Memoria kunnen verbeteren 😊. Bedankt voor je feedback en heel veel plezier gewenst! Het Viva Memoria Team" {...register('Reason')}></textarea>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" style={{borderRadius: 20}} onClick={props.onHide}>
            Annuleren
          </Button>
          <Button type="submit" style={{borderRadius: 20}}  variant="secondary">
            Account Verwijderen
          </Button>
        </Modal.Footer>
      </form>
    </Modal>
  );
}
