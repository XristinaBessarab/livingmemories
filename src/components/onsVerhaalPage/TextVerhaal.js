import React from "react";

export default function TextVerhaal() {
  //Export default text
  return (
    <div>

    {/*  Originele tekst voor het veranderen van het design
       
      <p>Hoi,</p>
      <p>
        Wij zijn Viva Memoria, en wij willen digitaal afscheid nemen en
        eerbetonen revolutionair veranderen.
      </p>
      <p>
        Platformen zoals Humans of New York hebben bewezen dat mensen zeer diep
        geboeid zijn door verhalen. Elk bericht dat door de maker wordt gepost
        op Instagram krijgt op minder dan één dag tijd miljoenen kijkers en
        likes. Het concept is nochtans zeer eenvoudig. De maker van Humans of
        New York trekt simpelweg enkele foto's en laat mensen iets vertellen
        over hunzelf of kennissen die hen nauw bij het hart zitten.,
      </p>
      <p>
        Verhalen vertellen ons wie we zijn. Ze zijn een middel die we eeuwenlang
        hebben gebruikt om ons identiteit neer te zetten. Elke persoon is in
        essentie niet meer dan wat hij heeft betekend voor zijn medemens.
      </p>
      <p>
        Met onze oplossing dagen we nabestaanden uit om verder te gaan dan
        "sterkte" toe te wensen aan nabestaanden. We wensen dat mensen dieper
        graven in hun verleden om te delen hoe de overledene een impact had.
      </p>
      
        Groetjes,
        <br />


    */}

    <div className="row toprow">
    <div className="col-md-7">
  
          <p>
              Viva Memoria geeft elke bezoeker de kans om hun
              specifieke herinneringen aan de overledene te uiten:
              de gebeurtenissen die ze deelden, de activiteiten die ze leuk vonden,
              de dingen die ze zeiden of geloofden, en hun aanwezigheid
              in verschillende stadia van het leven van bezoekers.
  
          </p>
     </div>
  
      <div className="col-md-5 ">
          
          {/* <img src={foto2} alt="foto van ons verhaal" /> */}
      
      </div>
  
  </div>

<div className="row bottomrow">
  

      <div className="col-md-5">
          
          {/* <img src={foto1} alt="foto van ons verhaal" /> */}
      
      </div>


      <div className="col-md-7">

      <p>
          Viva Memoria geeft elke bezoeker de kans om hun
          specifieke herinneringen aan de overledene te uiten:
          de gebeurtenissen die ze deelden, de activiteiten die ze leuk vonden,
          de dingen die ze zeiden of geloofden, en hun aanwezigheid
          in verschillende stadia van het leven van bezoekers.

      </p>
 </div>

  </div>
       
  


  
 



    <div className="col-md-12 team">
    
    <p>
    Het Viva Memoria team <br />
    Xristina, Yassin, Youssef, Burak, Jessie en Rachid.
  
    </p>
    
    </div>
    
    </div>
  );
}
