import React from 'react'
import TextVerhaal from '../onsVerhaalPage/TextVerhaal'
import "../../styles/onsVerhaal/onsVerhaal.scss";



export default function MainVerhaal() {
  return (
    <div className="verhaal-container">
      <h1 className="title">Ons Verhaal</h1>
      <div className="verhaal">
       <TextVerhaal />
      </div>
    
    </div>
  )
}
