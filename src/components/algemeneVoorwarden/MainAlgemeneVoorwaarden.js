import React from 'react';
import ScrollToTop from '../../components/ScrollToTop'
import { Link } from 'react-router-dom';

import '../../styles/AlgemeneVoorwaarden.css'

export default function MainAlgemeneVoorwaarden() {
  let websiteName = 'VIVA MEMORIA';

  const arrow = 'https://lh3.googleusercontent.com/u/0/d/1VGSaJe0sbdUspoTad39utq7Nav7Up6ZE=w1280-h885-iv1';
  const av1 = 'https://lh3.googleusercontent.com/u/0/d/1tvrRXlt_ViKI2iFBwdkGENMTKDOxx0dO=w2000-h2836-iv1';

  return (
    <div class='algemene-voorwaarde row col-12 justify-content-center'>
    {/* button redirect to the top */}
      <ScrollToTop />
      <h1 className='algemene-voorwaarde-title text-center'>Algemene Voorwaarden</h1>
      <div className='algemene-voorwaarde-inhoud row justify-content-center'>
        <div className='titles col-12 col-lg-4 p-4 p-md-5'>
          <div className='column mb-3 d-flex'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title1'><p className='icon-text'>Inleiding</p></a>
          </div>
          
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title2'><p className='icon-text'>Bedrijfsgegevens</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title3'><p className='icon-text'>Privacybeleid</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title4'><p className='icon-text'>Cookiebeleid</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title5'><p className='icon-text'>Intellectueel Eigendom</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title6'><p className='icon-text'>Toepasselijk recht</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title7'><p className='icon-text'>Offertes en aanbiedingen</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title8'><p className='icon-text'>Uitvoering van de overeenkomst</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title9'><p className='icon-text'>Wijziging van de overeenkomst</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title10'><p className='icon-text'>Gebruik van het resultaat</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title11'><p className='icon-text'>Gebreken en klachttermijnen</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title12'><p className='icon-text'>Bezoldiging</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title13'><p className='icon-text'>Betaling</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title14'><p className='icon-text'>Aansprakelijkheid</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title15'><p className='icon-text'>Overmacht</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title16'><p className='icon-text'>Geschillenregeling</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title17'><p className='icon-text'>Overige bepalingen</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#title18'><p className='icon-text'>Wijzigingen algemene voorwaarden</p></a>
          </div>
        </div>
        <div className='description col-12  col-lg-7'>
          <div className='row justify-content-center'>
            <img src={av1} alt=''/>
          </div>

          <div className='description-content p-2 p-md-5'>
            <span className='text-secondary'>Versie 3.0 – Deze pagina is voor het laatst aangepast op 23/12/2021.</span>

            <section id='title1' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p  className='fw-bold fs-3 '>Inleiding</p>
                          <p className='description-text'>Lees deze algemene voorwaarden zorgvuldig voordat u de website gebruikt. Door gebruik te maken van de website gaat u volledig akkoord met deze servicevoorwaarden. Indien u niet akkoord gaat met de voorwaarden van deze overeenkomst, dan krijgt u geen toegang tot het gebruik van de diensten op de website.</p>
                          <p className='description-text'>Door de site te bezoeken en/of iets te kopen, neemt u deel aan de “Service” en gaat u ermee akkoord gebonden te zijn aan de volgende algemene voorwaarden, inclusief die aanvullende voorwaarden en bepalingen waarnaar hierin wordt verwezen en/of beschikbaar is via een link. Deze algemene voorwaarden zijn van toepassing op alle gebruikers van de site, inclusief maar niet beperkt tot bezoekers, leveranciers, klanten, verkopers en begrafenisondernemers.</p>
                          <p className='description-text'>Het aanvaarden van de algemene voorwaarden is verplicht om een bestelling te plaatsen. Door te bestellen, bevestigt u de algemene voorwaarden van de verkoper te kennen en te accepteren. De algemene voorwaarden zijn altijd beschikbaar en zijn te vinden op deze pagina.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title2' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p  className='fw-bold fs-3'>Bedrijfsgegevens</p>
                          <p className='description-text'>{websiteName} is gevestigd te Corneel van Reethstraat 17, 2600 Berchem, België.</p>
                          <p><span className='fw-bold'>Contactgegevens:</span><br/>
                          <span className='fw-bold'>Website:</span> <a href='https://vivamemoria.be/' rel='noreferrer' target='_blank'>www.vivamemoria.be</a><br/>
                          <span className='fw-bold'>E-mail:</span> <a href='mailto:info@vivamemoria.be' rel='noreferrer' target='_blank'>Info@vivamemoria.be</a><br/>
                          <span className='fw-bold'>Adres: </span>Corneel van Reethstraat 17,<br/>
                          <span className='afstand'>2600 Berchem,</span><br/>
                          <span className='afstand'>België</span>
                          </p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title3' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Privacybeleid</p>
                          <p className='description-text'>{websiteName} respecteert de privacy van alle gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die de consument verschaft, vertrouwelijk wordt behandeld. Daarom werd er een apart privacybeleid opgemaakt dat u hier kan lezen: <Link to='/privacybeleid'>Privacybeleid</Link>.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title4' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Cookiebeleid</p>
                          <p className='description-text'>De website van {websiteName} gebruikt cookies en daarom werd er een apart gedeelte over het cookiebeleid gemaakt waarin beschreven wordt hoe {websiteName} cookies hanteert. Dit beleid kan u hier lezen: <Link to='/privacybeleid'>Cookiebeleid</Link>.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title5' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Intellectueel Eigendom</p>
                          <p className='description-text'>Alle uit de opdracht voortkomende rechten van intellectuele eigendom - waaronder het octrooirecht, merkrecht, tekening- of modelrecht en auteursrecht - op de resultaten uit de opdracht komen toe aan {websiteName}.</p>
                          <p className='description-text'>Voor zover een dergelijk recht slechts verkregen kan worden door een depot of registratie, is uitsluitend {websiteName} daartoe bevoegd, tenzij anders overeengekomen.</p>
                          <p className='description-text'>Partijen kunnen overeenkomen dat de in het eerste en tweede lid bedoelde rechten geheel of gedeeltelijk aan de opdrachtgever worden overgedragen. Deze overdracht en de eventuele voorwaarden waaronder de overdracht plaatsvindt worden altijd schriftelijk vastgelegd. Tot het moment van overdracht wordt een gebruiksrecht verstrekt als geregeld in het gedeelte over het gebruik van het resultaat.</p>
                          <p className='description-text'>{websiteName} heeft te allen tijde het recht om zijn naam op, bij, of in publiciteit rondom het resultaat van de opdracht -op de voor dat resultaat gebruikelijke wijze- te (laten) vermelden of verwijderen. Het is de opdrachtgever niet toegestaan zonder voorafgaande toestemming van {websiteName} het resultaat zonder vermelding van de naam van {websiteName} openbaar te maken of te verveelvoudigen.</p>
                          <p className='description-text'>Tenzij anders beschreven, blijven de in het kader van de opdracht door {websiteName} tot stand gebrachte (originelen van de) resultaten (zoals ontwerpen, ontwerpschetsen, concepten, adviezen, rapporten, begrotingen, ramingen, bestekken, werktekeningen, illustraties, foto’s, prototypes, maquettes, mallen, prototypes, (deel)producten, films, (audio en video) presentaties, broncodes, databases, functionaliteit, software, websiteontwerpen en andere materialen of (elektronische) bestanden e.d.) eigendom van {websiteName}, ongeacht of deze aan de opdrachtgever of aan derden ter beschikking zijn gesteld.</p>
                          <p className='description-text'>Na het voltooien van de opdracht hebben noch de opdrachtgever noch {websiteName} jegens elkaar een bewaarplicht met betrekking tot de gebruikte materialen en gegevens, tenzij anders overeengekomen.</p>
                          <p className='description-text'>Indien de opdrachtgever een reproductie-opdracht geeft aan {websiteName} onder de vorm van een gehele, gedeeltelijke of louter door montage herwerkte productie dan wordt verondersteld dat de opdrachtgever hiertoe over de nodige rechten beschikt en wanneer dit niet het geval zou zijn {websiteName} hiervoor niet aansprakelijk kan worden gesteld.</p>
                          <p className='description-text'>De inhoud en de media die op de site worden aangeboden ter informatie is uitsluitend voor persoonlijk gebruik. Met uitzondering van hetgeen uitdrukkelijk in deze algemene voorwaarden is bepaald, mag geen enkel deel van de site, inhoud of tekens worden gekopieerd, gereproduceerd, geaggregeerd, opnieuw gepubliceerd, geüpload, gepost, publiekelijk weergegeven, gecodeerd, vertaald, gedistribueerd, verkocht of anderszins uitgebuit voor welk commercieel doel dan ook, zonder uitdrukkelijke voorafgaande schriftelijke toestemming.</p>
                          <p className='description-text'>Het gebruik van de informatie op deze website is gratis zolang u deze informatie niet kopieert, verspreidt of op een andere manier gebruikt of misbruikt. U mag de informatie op deze website alleen hergebruiken volgens de regelingen van het dwingend recht.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title6' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Toepasselijk recht</p>
                          <p className='description-text'>Deze algemene voorwaarden en eventuele afzonderlijke overeenkomsten waarbij {websiteName} zijn services aanbiedt, worden beheerst in overeenstemming met de Belgische wetgeving.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title7' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Offertes en aanbiedingen</p>
                          <p className='description-text'>De door {websiteName} gemaakte offertes zijn geldig gedurende 30 kalenderdagen, tenzij anders aangegeven of zolang de aangegeven voorraad strekt.</p>
                          <p className='description-text'>De prijzen in de genoemde offertes zijn exclusief BTW, tenzij anders aangegeven.</p>
                          <p className='description-text'>Indien de offerte binnen de geldigheidstermijn schriftelijk wordt aanvaard ter uitvoering, wordt een voorschotbedrag van 50% op het totaalbedrag incl. BTW geëist.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title8' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Uitvoering van de overeenkomst</p>
                          <p className='description-text'>De verbintenissen van {websiteName} betreffen middelenverbintenissen, dewelke naar beste inzicht en vermogen en overeenkomstig de eisen van goed vakmanschap worden uitgevoerd en op grond van de op dat moment bekende stand der wetenschap met inachtneming van de eventuele wensen van de opdrachtgever.</p>
                          <p className='description-text'>Indien en voor zover een goede uitvoering van de overeenkomst dit vereist, heeft {websiteName} het recht bepaalde werkzaamheden te laten verrichten door derden.</p>
                          <p className='description-text'>De opdrachtgever draagt er zorg voor dat alle gegevens, waarvan {websiteName} aangeeft dat deze noodzakelijk zijn of waarvan de opdrachtgever redelijkerwijs behoort te begrijpen dat deze noodzakelijk zijn voor het uitvoeren van de overeenkomst, tijdig aan {websiteName} worden verstrekt. Indien de voor de uitvoering van de overeenkomst benodigde gegevens niet tijdig aan {websiteName} zijn verstrekt, heeft {websiteName} het recht de uitvoering van de overeenkomst op te schorten en/of de uit de vertraging voortvloeiende extra kosten volgens de gebruikelijke tarieven aan de opdrachtgever in rekening te brengen.</p>
                          <p className='description-text'>Indien is overeengekomen dat de overeenkomst in fasen zal worden uitgevoerd, kan {websiteName} de uitvoering van die onderdelen die tot een volgende fase behoren opschorten totdat de opdrachtgever de resultaten van de daaraan voorafgaande fase schriftelijk heeft goedgekeurd.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title9' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Wijziging van de overeenkomst</p>
                          <p className='description-text mb-4'>Indien tijdens de uitvoering van de overeenkomst blijkt dat voor een behoorlijke uitvoering het noodzakelijk is om de te verrichten werkzaamheden te wijzigen of aan te vullen, zullen partijen tijdig en in onderling overleg de overeenkomst dienovereenkomstig aanpassen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title10' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Gebruik van het resultaat</p>
                          <p className='description-text'>{websiteName} heeft met inachtneming van de belangen van de opdrachtgever, de vrijheid om de resultaten te gebruiken voor eigen publiciteit, verwerving van opdrachten, promotie, waaronder begrepen wedstrijden en tentoonstellingen e.d., en om deze in bruikleen te krijgen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title11' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Gebreken en klachttermijnen</p>
                          <p className='description-text'>Indien het verrichten van de overeengekomen dienstverlening niet meer mogelijk of zinvol is, zal {websiteName} slechts aansprakelijk zijn binnen de grenzen vermeld onder aansprakelijkheid.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title12' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Bezoldiging</p>
                          <p className='description-text'>Voor aanbiedingen en overeenkomsten waarin een vaste bezoldiging wordt aangeboden of is overeengekomen gelden de volgende zaken van deze algemene voorwaarden:</p>
                          <ul>
                            <li>Offertes en aanbiedingen</li>
                            <li>Wijziging van de overeenkomst</li>
                          </ul>
                          <p className='description-text'>Indien geen vaste bezoldiging wordt overeengekomen, gelden de volgende zaken:</p>
                          <ul>
                            <li>Uitvoering van de overeenkomst</li>
                            <li>Contractsduur en uitvoeringstermijn</li>
                            <li>Wijziging van de overeenkomst</li>
                          </ul>
                          <p className='description-text'>Partijen kunnen bij het tot stand komen van de overeenkomst een vaste bezoldiging overeenkomen. Het vaste bezoldiging is exclusief BTW.</p>
                          <p className='description-text'>Eventuele kostenramingen zijn excl. BTW, doch houden rekening met eventueel terugvorderbare taksen welke bijgevolg niet terugvorderbaar zijn door de opdrachtgever.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title13' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Betaling</p>
                          <p className='description-text'>Betaling dient te geschieden uiterlijk op de vervaldag van de facturen, bij gebreke waaraan een verwijlintrest verschuldigd is van 1% per maand, alsook een forfaitaire schadevergoeding van 5% van het onbetaald factuurbedrag met een minimum van 40€ per factuur. Daarenboven is de opdrachtgever een forfaitaire vergoeding verschuldigd van 10€ per aanmaning en 15€ per aangetekende aanmaning die {websiteName} stuurt ter betaling van facturen.</p>
                          <p className='description-text'>Van zodra één factuur niet betaald is op de vervaldag, worden alle facturen opeisbaar (ook de facturen die nog niet vervallen zijn).</p>
                          <p className='description-text'>Door de opdrachtgever gedane betalingen worden eerst aangerekend op de verschuldigde intresten en schadevergoedingen en dan pas op de facturen, waarbij de betaling eerst wordt aangerekend op de facturen die het langst openstaan, zelfs al vermeldt de opdrachtgever dat de betaling betrekking heeft op een latere factuur.</p>
                          <p className='description-text'>Voor het afhandelen van de betalingen via het online portaal wordt gebruik gemaakt van het betalingsplatform Mollie. Mollie biedt voor {websiteName} de volgende betalingsmogelijkheden aan:</p>
                          <ul>
                            <li>Bankoverschrijving</li>
                            <li>SEPA-domiciliëring</li>
                            <li>Bancontact/Mister Cash</li>
                            <li>iDeal</li>
                            <li>PayPal</li>
                          </ul>
                          <p className='description-text'>{websiteName} gaat pas over tot levering van het product of service wanneer de betaling goed is ontvangen of wanneer dit anders werd afgesproken. Indien de klant niet tijdig aan zijn betalingsverplichting(en) voldoet, wordt hij door de ondernemer gewezen op de te late betaling en gunt de ondernemer de klant een termijn van 14 dagen om alsnog aan zijn betalingsverplichtingen te voldoen. Na het uitblijven van betaling binnen deze 14-dagentermijn, is de klant over het nog verschuldigde bedrag de wettelijke rente verschuldigd en is de ondernemer gerechtigd de door hem gemaakte buitengerechtelijke incassokosten in rekening te brengen.</p>
                          <p className='description-text'>Deze incassokosten bedragen maximaal: 15% over openstaande bedragen tot € 2.500, = 10% over de daaropvolgende € 2.500, = 5% over de volgende € 5.000, met een minimum van € 40. De ondernemer kan ten voordele van de consument afwijken van genoemde bedragen en percentages.</p>
                          <p className='description-text'>De klant gaat ermee akkoord om actuele, volledige en correcte aankoop- en accountinformatie te verstrekken. Hij gaat er verder mee akkoord dat de account- en betalingsinformatie, inclusief het e-mailadres, de betaalmethode en de vervaldatum van de betaalkaart, onmiddellijk bijgewerkt worden, zodat de ondernemer de transacties kan voltooien en bij een probleem de consument zo nodig kan contacteren. Prijzen kunnen op elk moment zonder aankondiging worden gewijzigd. Alle betalingen worden in Euro (€) uitgevoerd.</p>
                          <p className='description-text'>{websiteName} behoudt zich het recht om eender welk product of service te weigeren. {websiteName} kan naar eigen goeddunken de service of het product per persoon of per bestelling beperken of annuleren. Deze beperkingen kunnen bestaan uit bestellingen geplaatst door of onder dezelfde klantenaccount, dezelfde betaalmethode of bestellingen die hetzelfde factuur- of verzendadres gebruiken</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title14' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Aansprakelijkheid</p>
                          <p className='description-text'>Indien {websiteName} aansprakelijk is, dan is die aansprakelijkheid als volgt begrensd:</p>
                          <ul>
                            <li className='mb-3'>De aansprakelijkheid van {websiteName}, voor zover deze door haar aansprakelijkheidsverzekering wordt gedekt, is beperkt tot het bedrag van de door de verzekeraar gedane uitkering.</li>
                            <li className='mb-3'>Indien de verzekeraar in enig geval niet tot uitkering overgaat of schade niet door de verzekering wordt gedekt, is de aansprakelijkheid van {websiteName} beperkt tot de factuurwaarde van de opdracht, althans dat gedeelte van de opdracht waarop de aansprakelijkheid betrekking heeft</li>
                            <li>{websiteName} is nimmer aansprakelijk voor indirecte schade of gevolgschade.</li>
                          </ul>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title15' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Overmacht</p>
                          <p className='description-text'>Tijdens overmacht worden de verplichtingen van {websiteName} opgeschort. </p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title16' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Geschillenregeling</p>
                          <p className='description-text'>Uitsluitend de rechtbanken te Antwerpen zijn bevoegd kennis te nemen van geschillen met betrekking tot de overeenkomst tussen {websiteName} en enkel het Belgische recht is op deze geschillen van toepassing.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title17' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Overige bepalingen</p>
                          <p className='description-text'>Indien enige bepaling van deze Algemene Voorwaarden nietig is of vernietigd wordt, zullen de overige bepalingen van deze Algemene Voorwaarden onverminderd van kracht blijven. Partijen zullen in dat geval in overleg treden met het doel nieuwe bepalingen ter vervanging van de nietige of vernietigde bepalingen overeen te komen, waarbij zoveel mogelijk het doel en de strekking van de nietige dan wel vernietigde bepalingen in acht wordt genomen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='title18' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Wijzigingen algemene voorwaarden</p>
                          <p className='description-text'>{websiteName} behoudt zich het recht om deze algemene voorwaarden te allen tijde aan te passen.</p>
                          <p className='description-text'>Deze algemene voorwaarden is daarom aangepast aan het gebruik van, en de mogelijkheden op deze website. Alle nieuwe functies of diensten die aan de huidige website worden toegevoegd, zijn eveneens onderworpen aan deze algemene voorwaarden.</p>
                          <p className='description-text'>Het is daarom uw verantwoordelijkheid om deze algemene voorwaarden van de website regelmatig te controleren op wijzigingen. Op deze pagina zal u dan ook altijd de meest recente versie vinden van onze algemene voorwaarden.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>
            
          </div>
        </div>
      </div>
    </div>
  )
}
