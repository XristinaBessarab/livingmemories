import React, { useState, useRef, useEffect } from "react";
import { Nav } from "react-bootstrap";
import LoginButton from "../utils/LoginButton";
import NavDropdown from "../utils/NavDropdown";

import "../styles/navBar/navBar.css";

const NavBar = ({ children, sticky = "top" }) => {

  const logo = 'https://lh3.googleusercontent.com/u/0/d/1pjxtevxX7dkN1SmFulbrzY1r1aJT3w6N=w1280-h885-iv1'

  // changes transparency of nav background when scrolling
  const [navBackground, setNavBackground] = useState(false);
  //useRef gets used in order for the variable navBackground to stay alive for the full lifetime of the component NavBar
  //navBackground changes the background transparent
  const navRef = useRef();
  navRef.current = navBackground;

  useEffect(() => {
    //When scrolling, the navbar becomes transparent
    const handleScroll = () => {
      const show = window.scrollY > 50;
      if (navRef.current !== show) {
        setNavBackground(show);
      }
    };
    //using eventListener 'scroll' to trigger the function handleScroll
    document.addEventListener("scroll", handleScroll);
    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // mobile
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <div className="navbar sticky-top pe-4">
      <div className="logo-container">
        <Nav.Link className="" href="/#" onClick={closeMobileMenu}>
        <img className="navbar-logo" src={logo} alt="logo viva memoria" />
          <span className="navbar-logo-text">VIVA MEMORIA</span>
        </Nav.Link>
      </div>
      <div className="navbar-container">
        <div className="menu-icon" onClick={handleClick}>
          <i className={click ? "fas fa-times" : "fas fa-bars"} />
        </div>
        <ul className={click ? "nav-menu active" : "nav-menu"}>
          <li>
            <Nav.Link
              className="navar-item"
              href="/#"
              onClick={closeMobileMenu}
            >
              Home
            </Nav.Link>
          </li>
          {children &&
            children.map((link) => {
              return (
                <li key={link.props.href}>
                  <Nav.Link
                    className="navar-item"
                    href={link.props.href}
                    onClick={closeMobileMenu}
                  >
                    {link.props.children}
                  </Nav.Link>
                </li>
              );
            })
          }
          <li className="navbar-buttons">
            <Nav.Link className="ps-0">
              <LoginButton/>
              <NavDropdown/>
            </Nav.Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
