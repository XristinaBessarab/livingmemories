import React, { useState, useEffect } from "react";
import app from "../helpers/axiosConfig";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
// import moment from "moment";
// import { useHistory } from "react-router-dom";

const SearchBarSearch = (props) => {
  const [search, setSearch] = useState("");
  const [memorialData, setMemorialData] = useState([])
  const [images, setImages] = useState([]);
  // var img = [];
  // const history = useHistory();

  // months in word
  const months = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "December",
  ];

  useEffect(() => {
    (async () => {
      //Get all images for memorialpages
      const imageData = await app.get(`/api/visualcontent`);
      setImages([...imageData.data]);
      const data = await app.get("/api/memorialpages");
      searchPages({searchName: props.props.match.params.Search, memorial: data.data});
      setMemorialData(data.data);

    })();
  }, []);

  const searchPages = ({searchName, memorial}) => {
    let result = [];
    result = memorial.filter((data) => {
      if (searchName === "") {
        return setSearch(null);
      }
      let lastname = data.LastName.toLowerCase().search(searchName) !== -1;
      let firstname = data.FirstName.toLowerCase().search(searchName) !== -1;
      if (firstname && lastname) {
        return firstname + " " + lastname;
      } else if (firstname) {
        return firstname;
      } else return lastname;
    });
    setSearch(result);
  };

  const newSearch = async (e) =>{
    if (e.target.value.toLowerCase() !== "" && e.target.value.toLowerCase() !== " ") {
      searchPages({searchName: e.target.value.toLowerCase(), memorial: memorialData})
    }
  }

  return (
    <div className="search-result-container" style={{minHeight: "100vh", backgroundColor: "#f5f9fc" }}>
      <div className="search-bar text-center pt-5">
        <input
          className="w-75 p-3 border"
          placeholder="Vind een herdenkingsruimte"
          onChange={newSearch}
        />
      </div>
      {search && search.length > 0 ? (
        <div className="herdenkingsruimte">
          {search.map((element) => {
            return (
              <Link
                to={{
                  pathname: `/herdenkings/${element.Id}`,
                  memorial: element.Id,
                }}
                key={element.Id} 
                className="card-link"
              >
                <Card 
                  key={element.Id} 
                  className="card"
                  style={{height: 380}}
                >
                  {images &&
                    images.map((img) => {
                      if (img.MemorialPages_Id === element.Id) {
                        if (img.Route === "Profile") {
                          return (
                            <Card.Img
                              src={img.Source}
                              key={img.Id}
                              className="img"
                            />
                          );
                        }
                      }
                    })}
                  <div className="d-flex row justify-content-center">
                    <p className="card-full-name text-center justify-content-center">
                      {element.FirstName} {element.LastName}
                    </p>
                    <p className="card-date text-center">
                    <span className="fw-bold">&#176;</span>{element.BirthDate && element.BirthDate.split("-")[2].split("T")[0] + " "}
                      {element.BirthDate && months[Number(element.BirthDate.split("-")[1] - 1)] + " "}
                      {element.BirthDate && element.BirthDate.split("-")[0] + " "}
                      -
                    <span className="fw-bold ms-1">&#8224;</span>{element.DateOfDeath && element.DateOfDeath.split("-")[2].split("T")[0] + " "}
                    {element.DateOfDeath && months[Number(element.DateOfDeath.split("-")[1] - 1)] + " "}
                    {element.DateOfDeath && element.DateOfDeath.split("-")[0] + " "}
                    </p>
                    <p className="card-woonplaats text-center">
                      {element.Woonplaats && element.Woonplaats ? element.Woonplaats : "Geen woonplaats"}
                    </p>
                  </div>
                </Card>
              </Link>
            );
          })}
        </div>
      ) : (
        <div className="d-flex justify-content-center pt-5">
          <h1 className="fw-bold px-5">Er zijn geen zoekresultaten</h1>
        </div>
      )}
    </div>
  );
};
export default SearchBarSearch;
