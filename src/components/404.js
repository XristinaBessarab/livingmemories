import React from 'react'
import { Link } from 'react-router-dom'

import '../styles/404.css'

const Pagina404 = () => {
    const error404 = 'https://lh3.googleusercontent.com/u/0/d/1G2k8txq32RVNafURhzSdMdGBjLSfesiL=w1920-h942-iv1'
    const leftArrow = 'https://lh3.googleusercontent.com/u/0/d/1R5g403BLmJRnE1nW-RL2H7scaGbrLXzE=w1920-h942-iv1'
    return (
        <div className='pagina404'>
            <div className='inhoud404 row justify-content-center'>
                <img src={error404} alt='404' />
                <div className='row justify-content-center'>
                    <p className='title-1 text-center'>Oei, oei! 🥺 Er is iets fout</p>
                    <p className='text-center'>De pagina die u zoekt is verplaatst, verwijderd, hernoemd</p>
                    <p className='text-center'>of heeft misschien nooit bestaan!</p>
                </div>
                <div className='btn-home d-flex column justify-content-center mt-4'>
                    <img src={leftArrow} alt='' className='arrow' />
                    <Link to='/' className='text-decoration-none'><button className='button d-inline'><span className='ms-4'>Terug naar home</span></button></Link>
                </div>
            </div>
        </div>
    )
}

export default Pagina404
