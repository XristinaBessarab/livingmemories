import React from "react";
import { Link } from "react-router-dom";

import SearchBar from "../SearchBar";
import "../../styles/Main.scss";

// import { FaArrowRight} from "react-icons/fa";
import { BsCheckLg } from "react-icons/bs";

// import '../../styles/Main.css'

const Main = () => {

  const add = 'https://lh3.googleusercontent.com/u/0/d/1uPKUFDPOsO73SibyGjnKiY6PJXTwkpp5=w1280-h885-iv1';
  const banner = 'https://lh3.googleusercontent.com/u/0/d/1lr5gzCendh-Ru-YoapfTr7-7CYQujmfu=w2000-h2370-iv1';
  
  const signup = 'https://lh3.googleusercontent.com/u/0/d/1wSNaClpfjp2X6heEHrvnNVR5LlDYBrrB=w2000-h2370-iv1';
  const herdenkingsruimte = 'https://lh3.googleusercontent.com/fife/AAWUweVPhBz1P08KGDURoe_UXY0nCz20hobMQIR_XWB85tW8X7Nj5sexKWVky7KFRAzlEVwZoa2HR7UMaT2n2bfKceiC38kJRYXb10Qjpx7v3mntLfFEdgxAMijRpXlUfBoHM9-OIfVoey-Lx-50iW0HHap98689fOnxYpQ4PEErI9p2jnrY1GTgmf7cLKUk9QtQbH6OwZ_tDBZ96IA9Bd6wX94X0b7rgY7U8-Q4YwicldqTIxoYA0yLO0ra0bwzfK46D9JjaWo3eSc7g8ZWUE0yBDCftDuY7Ch8n3CxIx8zgrqKnvkv0zXps4VR8mPmlR0WtlsgnGCC1VYAsfe5V-UGFsfu1UNKRPcCui5Srrc1E6mLi_QKwXUWZmKvdrcwI_l_dBlq3pbIG7r-5Ewz2nMA2RpdKYN-917CuWgvjA3GrqvImkvDptyPB-IumySSogxuc5de5Cd_7pojX5Yj6a2xior7zTVYFSB93NGuE3k45agKN3TqR3lTpw2mvQeVctOH3_WGnZNEGTs6ixJwtQ4DNidTJKlKagY-9S3LAbchAPs2B14A129J0JVGDlv9dDnuDe35dfz-_Q9txJ0dJMODIXmbPA3qve9yVzzQW2-Cay8bz7I_iLHWrGavKr00pWhqyoNHx6vK_gGMXuWhUjjW7f6ePxpk9GwimxL_ccXYvOX0Gt22AHShaZT98IgWqxiv8X0ZgSzfKdbAYAzXDV5nHuU352nTz05bGhY';
  const invite = '	https://lh3.googleusercontent.com/u/0/d/1XVGgrXb2PS2oqMz-52Ja-9DewVfHwa3u=w2000-h2370-iv1';

  
  const Jessie = 'https://lh3.googleusercontent.com/u/0/d/1gEG8k3L_nmQDJRHPHP1N4ersWBt18q49=w1920-h942-iv1';
  const KarimOumars = 'https://lh3.googleusercontent.com/u/0/d/1aA24FcVJPlcvKpUPBdf6eUDCnURtXg9J=w1920-h942-iv1';
  const YannoMaes = ' https://lh3.googleusercontent.com/u/0/d/1yqPcbQNrQ2rYzL9MAM9lTclL5WaWqZvD=w1920-h942-iv1';
  
  const down = 'https://lh3.googleusercontent.com/u/0/d/1U_ARPoDHeOIsci2GCZ44td6ogDW-Z2Na=w1280-h885-iv1';

  const overOns = 'https://lh3.googleusercontent.com/u/0/d/1t9vRt56tLuG6reKjsyeSWiqtof1Gz_q0=w2863-h986-iv1';

  const messenger = 'https://lh3.googleusercontent.com/u/0/d/1WuVYQw9zJlTmQ-b23aufal7TAYd2379o=w1280-h885-iv1'
  const whatsapp = 'https://lh3.googleusercontent.com/u/0/d/12EG-4w1smPrisEFIX3heIY4Zu7vUXITe=w1280-h885-iv1'
  const email = 'https://lh3.googleusercontent.com/u/0/d/1pm7Vg9z3eLlXIE0uE_EUxF7VvqnDgbw6=w2000-h2370-iv1'
  

  return (
    <main className="main">
      {/* --------------------- header --------------------- */}
      <section id="home" class="hero">
        {/* <div class=""> */}
          <div class="hero-container row m-0">
            <div className="d-flex hero-part-one">
              <div class="hero-content">
                <p className="hero-item pg-1">Viva Memoria</p>
                <p className="hero-item pg-2">Herdenken door Verhalen</p>
                <p className="mt-4 pg-3">Een gratis, online herdenkingsruimte gekoppeld aan een unieke QR code</p>
                <ul className="hero-item hero-list">
                 <div className="">
                  <li class="p-0 hero-linst-p">
                      <BsCheckLg className="me-2"/> Deel fijne herinneringen
                    </li>
                    <li class="p-0 hero-linst-p">
                      <BsCheckLg className="me-2" /> Leer van elkaar hoe iemand jou raakt
                    </li>
                    <li class="p-0 hero-linst-p">
                      <BsCheckLg className="me-2" /> Doneer, verras en blijf verbonden
                    </li>
                 </div>
                </ul>
                <div className="d-flex hero-create-button hero-item">
                  <Link
                    to="/memorialpage-form-1"
                    className="text-decoration-none"
                  >
                    <div className="aan-button">
                      <img
                        src={add}
                        alt="herdenkingspagina maken"
                        className=""
                      />
                      <button>Maak nu een herdenkingsruimte</button>
                    </div>
                  </Link>
                </div>
              </div>
              <div class="hero-img">
                <img src={banner} alt="Home page foto"/>
              </div>
            </div>
          </div>

          <SearchBar />

          <div className="scroll-down row row-cols-auto justify-content-center py-0">
            <div class="col scrolldown-wrapper">
              <div class="scrolldown">
                <a href="#aan-de-slag" class="scrollto">
                  <svg height="30" width="10">
                    <circle class="scrolldown-p1" cx="5" cy="15" r="2" />
                    <circle class="scrolldown-p2" cx="5" cy="15" r="2" />
                  </svg>
                </a>
              </div>
            </div>
          </div>
        {/* </div> */}
      </section>
      {/* --------------------- Aan de slag --------------------- */}
      <section className="text-center aan-de-slag" id="aan-de-slag">
        <div class="">
          {/* title */}
          <div class="content-section-heading d-flex justify-content-center">
            <span class="">
              Maak een herdenkingsruimte in drie stappen
            </span>
          </div>
          {/* steps */}
          <div class="staps row justify-content-center mobile mb-4 ">
            <div class="stap">
              <img
                className="stappenimg"
                src={signup}
                alt="sign up"
              />
              <h4 className="">
                <strong>Registratie</strong>
              </h4>
              <p class="">Maak een gratis account aan</p>
            </div>
            <div class="stap">
              <img
                className="stappenimg"
                src={herdenkingsruimte}
                alt="Create"
              />
              <h4>
                <strong>Creatie </strong>
              </h4>
              <p class="">Maak een herdenkingsruimte en ontvang een unieke QR-code die op het graf gezet kan worden</p>
            </div>
            <div class="stap">
              <img
                className="stappenimg"
                src={invite}
                alt="Story's"
              />
              <h4 className="pg-3">
                <strong>Verhalen en herinneringen samenbrengen</strong>
              </h4>
              <p className="mb-0 pg-3">
                Nodig vrienden en familie uit om verhalen te delen
              </p>
            </div>
          </div>
          {/* button */}
          <div className="slag-btn d-flex mt-5">
            <Link
              to="/memorialpage-form-1"
              className="text-decoration-none"
            >
              <div className="aan-button d-flex justify-content-center">
                <img
                  src={add}
                  alt="herdenkingspagina maken"
                  className=""
                />
                <button>Maak nu een herdenkingsruimte</button>
              </div>
            </Link>
         </div>
        </div>
      </section>
      {/* --------------------- Quote --------------------- */}
      <section className="quote d-flex flex-column">
        <h2 className="">
          Ze zeggen dat we twee keer sterven. Eerst, als de laatste adem je
          lichaam verlaat, en nog een keer wanneer de laatste persoon die je
          kent je naam zegt.
        </h2>
        {/* <div className=" imgg">
            <img src={Quote} alt=""/>
            </div> */}
        <p className="">IRVIN D. YALOM</p>
      </section>
      {/* --------------------- Getuigenissen --------------------- */}
      <section
        className="getuigenissen d-flex flex-column text-center"
        id="getuigenissen"
      >
        {/* title */}
        <div class="content-section-heading d-flex justify-content-center">
          <span class="">
          Wat kennissen vinden van dit concept
          </span>
        </div>
        {/* cards */}
        <div className="getuigenissen-container d-flex justify-content-center">
          <div className="card card1">
            <div>
              <img src={Jessie} alt="getuigenis" />
            </div>
            <h5 className="fw-bolder">Jessie Pittoors</h5>
            <p>
            Toen Kristina me voor het eerst vertelde over Viva Memoria, dacht ik ‘waarom bestaat dit nog niet?’ omdat het zo’n prachtig idee is. De tijd van rouwregisters is voorbij. Geen holle, clichématige, troostende woorden meer, maar echte verhalen die mooie herinneringen levende houden.
            </p>
          </div>
          <div className="card card1">
            <div>
              <img src={KarimOumars} alt="getuigenis" />
            </div>
            <h5 className="fw-bolder">Karim Oumars</h5>
            <p>
            Ik vind het een heel mooi project. Ik vind het fijn dat het volledig gratis is. Zo kan iedereen dit vlot in gebruik nemen.  
            </p>
          </div>
          <div className="card card1">
            <div>
              <img src={YannoMaes} alt="getuigenis" />
            </div>
            <h5 className="fw-bolder">Yanno Maes</h5>
            <p>
              Zij die liefhebben worden nooit oud. Het is mogelijk dat zij sterven van ouderdom, maar zij sterven jong. De dood bestaat niet, mensen sterven alleen als ze vergeten worden. Als je me kunt herinneren, zal ik altijd bij je zijn en dit is precies wat deze site doet. Ervoor zorgen dat de herinneringen des te beter bij je blijven.
            </p>
          </div>
        </div>
        <h2 className="mt-4 pt-4 text-nowrap">Heb jij het al geprobeerd?</h2>
        <img
          src={down}
          alt="herdenkingspagina maken"
          width="65rem"
          className="mt-1 pb-1 pr-1 align-self-center"
        />
        <div className="getuigenissen-btn d-flex mt-4 justify-content-center">
          <Link
            to="/memorialpage-form-1"
            className="text-decoration-none"
          >
            <div className="aan-button">
              <img
                src={add}
                alt="herdenkingspagina maken"
                className=""
              />
              <button>Maak nu een herdenkingsruimte</button>
            </div>
          </Link>
        </div>
      </section>
      {/* --------------------- Over ons --------------------- */}
      <section className="over-ons" id="over-ons">
          <div className="over-ons-img">
            <img src={overOns} alt="Over ons" />
          </div>
          <div className="d-flex flex-column over-ons-text">
            <div className="over-ons-title d-flex">
              <span className="h2">Hoi! Kristina en Yassin hier 😊, </span>
            </div>
            <p className="p-text">
            Sinds de uitbraak van corona is afscheid nemen veel moeilijker geworden. Online houden heel veel mensen het vaak enkel op “sterkte” of  “gecondoleerd”. Dit, terwijl er zoveel meer kan gezegd worden over iemand. Wij vinden dit dan een echt gemis voor de nabestaanden, die juist heel graag horen wat iemand heeft betekend. 
            Sinds 2020 bouwen we aan de non-profit Viva Memoria met een eenvoudige missie: Verhalen en herinneringen samenbrengen, zodat nabestaanden een mooi overzicht krijgen van de veelzijdigheid van iemand die hen nauw bij het hart ligt. 
            Daarom hebben we een gratis, online herdenkingsruimte gebouwd die gekoppeld is aan een unieke QR code. Voor de koffietafel kan de herdenkingsruimte worden gedeeld, waarna de nabestaanden de verhalen kunnen lezen. De QR-code kan ook op de grafzerk staan en in het overlijdensbericht in de krant en de rouwkaart. Zo krijgt elk verhaal een plek✨.  
            </p>
          </div>
      </section>
      {/* --------------------- Contact --------------------- */}
      <section class="contact" id="contact">
          {/* text */}
          <div class="text-center">
            <div className="pt-5 pb-4">
              <span class="h3 contact-title">
                Zou je graag de initiatiefnemers willen contacteren?
              </span>
            </div>
            <p class="contact-p">
              Aarzel niet om ons te contacteren via mail, telefoon of onze
              sociale mediakanalen.
            </p>
          </div>
          {/* contact */}
          <div class="contact-items d-flex justify-content-evenly mt-5">
            <div class="mb-5">
              <h4 class="text-center">
                <strong>Messenger</strong>
              </h4>
              <div>
                <img src={messenger} alt="messenger" />
                  <a
                    className="text-decoration-none"
                    href="http://m.me/VivaMemoriaOnline"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <span className="">Viva Memoria</span>
                  </a>
              </div>
            </div>
            <div class=" mb-5">
              <h4 class="text-center">
                <strong>Telefoon</strong>
              </h4>
              <div>
                <img src={whatsapp} alt="phone" className="contact-icon-phone" />
                <a className="text-decoration-none" href='https://wa.me/32483446814?text=Ik heb een vraag:' rel='noreferrer' target='_blank'>
                <span>+32 483 44 68 14</span>
                </a>
              </div>
            </div>
            <div class=" mb-5">
              <h4 class="text-center">
                <strong>Email</strong>
              </h4>
              <div className="">
                <img src={email} alt="email" className="contact-icon-mail" />
                  <a
                    className="text-decoration-none"
                    href="mailto:info@vivamemoria.be"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <span>info@vivamemoria.be</span>
                  </a>
              </div>
            </div>
          </div>
          {/* staff */}
          <div className="staff-container d-flex">
            <div>
              <img 
                src="https://lh3.googleusercontent.com/u/0/d/1YtSA3OQHqj0CTG3lztEuDCrWhacUzTUw=w1920-h942-iv1"
                alt="handen met een hart erop"
                className=""
                style={{width: "10rem", height: "10rem"}}
              />
            </div>
            <div className="">
              <p className="text-justify fw-bold">Dit sociaal project is medemogelijk gemaakt door het vrijwillige engagement van 
              <a 
                href="https://www.linkedin.com/in/kristina-bessarab-8b4041178/" target="_blank"      rel="noreferrer"
                className="text-decoration-none"> Kristina Bessarab
              </a>,
              <a 
                href="https://www.linkedin.com/in/yassinblz/" 
                target="_blank"      
                rel="noreferrer"
                className="text-decoration-none"> Yassin Boullauazan</a>, Mahsa Nasir Hajipour, Abdurrahman Dündar, Jessie Pittoors, Burak Öcbe en Youssef El Hindaz.</p>
            </div>
          </div>
      </section>
    </main>
  );
};

export default Main;
