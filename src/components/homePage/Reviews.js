import React from "react";
import Carousel from "react-elastic-carousel";

export default function Reviews() {
  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 500, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 3 },
  ];
  return (
    <>
      <h2 id="getuigenissen" className="review_titel" >
        Getuigenissen
      </h2>




      {/*  Onderlijning van het eerste design 
      <div className="underline">
        <hr />
      </div>

      */}


      <div className="memorial_reviews">
      
        <Carousel breakPoints={breakPoints}>
          <div className="review_item one">
              <p className="reviewer">-Jessie P.</p>

              <p className='review_text'>
              Toen Kristina me voor het eerst vertelde over Viva Memoria, dacht ik ‘waarom bestaat dit nog niet?’ omdat het zo’n prachtig idee is. De tijd van rouwregisters is voorbij. Geen holle, clichématige, troostende woorden meer, maar echte verhalen die mooie herinneringen levende houden.

             </p>
           
             {/*
             <p className='review_overledene' >Over zijn grootvader <br />
               Franciscus Peeters

             </p>
             */}
          </div>


          <div className="review_item two">
          <p className="reviewer">-Burak O.</p>
            <p className='review_text'>
            Toen Kristina me voor het eerst vertelde over Viva Memoria, dacht ik ‘waarom bestaat dit nog niet?’ omdat het zo’n prachtig idee is. De tijd van rouwregisters is voorbij. Geen holle, clichématige, troostende woorden meer, maar echte verhalen die mooie herinneringen levende houden.

            </p>
            {/*
            <p className='review_overledene' >Over zijn grootvader <br />
            Franciscus Peeters

            </p>
             */}
          </div>


          <div className="review_item three">
          <p className="reviewer">-Selim K.</p>
            <p className='review_text'>
            Toen Kristina me voor het eerst vertelde over Viva Memoria, dacht ik ‘waarom bestaat dit nog niet?’ omdat het zo’n prachtig idee is. De tijd van rouwregisters is voorbij. Geen holle, clichématige, troostende woorden meer, maar echte verhalen die mooie herinneringen levende houden.

            </p>
            {/*
            <p className='review_overledene' >Over zijn grootvader <br />
            Franciscus Peeters

            </p>
             */}
          </div>


          <div className="review_item three">
          <p className="reviewer">-Elie M.</p>
            <p className='review_text'>
            Toen Kristina me voor het eerst vertelde over Viva Memoria, dacht ik ‘waarom bestaat dit nog niet?’ omdat het zo’n prachtig idee is. De tijd van rouwregisters is voorbij. Geen holle, clichématige, troostende woorden meer, maar echte verhalen die mooie herinneringen levende houden.

            </p>
            {/*
            <p className='review_overledene' >Over haar grootvader <br />
            Franciscus Peeters

            </p>
             */}
          </div>
        </Carousel>
      </div>
    </>
  );
}
