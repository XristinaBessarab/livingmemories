import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Spinner from "../Spinner";
import app from "../../helpers/axiosConfig";
import { useHistory } from "react-router-dom";
import moment from "moment";
import useSession from "../../auth/set-session";

const Form = (props) => {
  const [userData, setUserData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const userId = useSession();

  const { register, handleSubmit } = useForm();

  let axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

  //Function go back to homepage
  const routeChange = () => {
    history.push("/");
  };

  //Getting data from id
  useEffect(() => {
    (async () => {
      try {
        setIsLoading(true);
        if (userId) {
          const data = await app.get(`get/userdata/${userId}`);

          setUserData(data.data[0]);
          setIsLoading(false);
        }
      } catch {}
    })();
  }, [userId]);

  //Send the data from FORM to endpoint with axios
  const onSubmit = (data) => {
    for (let index in data) {
      if (data[index] === "" || data[index] === "true") {
        data[index] = userData[index];
      }
    }
    data.Id = userData.Id;
    app
      .put(`/put/${data.Id}`, data, axiosConfig)
      .then((res) => history.push("/"))
      .catch((err) => console.log(err));
  };

  const date =
    userData.BirthDate !== undefined &&
    moment(userData.BirthDate).format("YYYY-MM-DD");

  if (isLoading) {
    return (
      <div
        style={{
          height: "20vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );
  }
  return (
    <div className="form-wrapper">
      <form className="form-info" onSubmit={handleSubmit(onSubmit)}>
        <h1>Wijzig uw gegevens</h1>
        <label>Voornaam</label>
        <input
          type="text"
          // name="FirstName"
          defaultValue={userData.FirstName}
          // ref={register({ maxLength: 45 })}
          {...register('', { maxLength: 45 })}
        />
        <label>Achternaam</label>
        <input
          type="text"
          defaultValue={userData.LastName}
          // name="LastName"
          // ref={register({ maxLength: 45 })}
          {...register('LastName', { maxLength: 45 })}
        />
        <label>Tussenvoegsel</label>
        <input
          type="text"
          defaultValue={userData.Prefix}
          // name="Prefix"
          // ref={register({ maxLength: 45 })}
          {...register('Prefix', { maxLength: 45 })}
        />
        <label>Geboortedatum</label>
        <input
          type="date"
          // name="BirthDate"
          max={moment().format("2009-10-05")}
          // ref={register}
          {...register('BirthDate')}
          defaultValue={date}
        />
        <label>Postcode</label>
        <input
          type="text"
          defaultValue={userData.ZipCode}
          // name="ZipCode"
          // ref={register({ maxLength: 10 })}
          {...register('ZipCode', { maxLength: 10 })}
        />
        <label>Geslacht</label>
        {/* <select name="Gender" ref={register} defaultValue={userData.Gender}> */}
        <select {...register('Gender')} defaultValue={userData.Gender}>
          <option disabled value="">
            {" "}
            -- kies een optie --{" "}
          </option>
          <option value="Man">Man</option>
          <option value="Vrouw">Vrouw</option>
          <option value="Andere">Andere</option>
        </select>
        <label>Telefoonnummer</label>
        <input
          type="text"
          defaultValue={userData.PhoneNumber}
          // name="PhoneNumber"
          // ref={register({ maxLength: 12 })}
          {...register('PhoneNumber', { maxLength: 12 })}
        />
        <label>Ons gevonden</label>
        {/* <select name="FoundUs" ref={register} defaultValue={userData.FoundUs}> */}
        <select {...register('FoundUs')} defaultValue={userData.FoundUs}>
          <option disabled value="">
            {" "}
            -- kies een optie --{" "}
          </option>
          <option value="Eerder Gebruikt">Eerder Gebruikt</option>
          <option value="Google">Google</option>
          <option value="Sociale Media">Sociale Media</option>
          <option value="Krant">Krant</option>
          <option value="Andere">Andere</option>
        </select>
        <label>Taal</label>
        {/* <select name="Language" ref={register} defaultValue={userData.Language}> */}
        <select {...register('Language')} defaultValue={userData.Language}>
          <option disabled value="">
            {" "}
            -- kies een optie --{" "}
          </option>
          <option value="Nederlands">Nederlands</option>
          <option value="Frans">Frans</option>
          <option value="Engels">Engels</option>
          <option value="Andere">Andere</option>
        </select>
        <div className="button-group">
          <button onClick={routeChange} className="cancel-btn">
            Annuleren
          </button>
          <button type="submit" className="submit-btn">
            Wijzigen
          </button>
        </div>
      </form>
    </div>
  );
};

export default Form;
