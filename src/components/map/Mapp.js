import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import ReactLeafletGoogleLayer from 'react-leaflet-google-layer';

//This is an open street map, we get satelite view through the api key
//Setting props for function to use it on different places
//This will be used as a dynamic map component => helps us use just one map over the whole project
export default function Mapp({ address, Latitude, Longitude, linkToMaps, Zoom = 19 }) {
  return (
    <MapContainer 
      center={[Latitude, Longitude]}  
      zoom={Zoom} 
      scrollWheelZoom={false}
    >
      <ReactLeafletGoogleLayer apiKey={process.env.REACT_APP_GOOGLE_API_KEY} type={'satellite'} />
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[Latitude, Longitude]}>
        { address && 
          (
            <Popup>
              <a target="_blank" href={linkToMaps} rel="noopener noreferrer">
                {address}
              </a>
            </Popup>
          )
        }
      </Marker>
   </MapContainer>
  );
}