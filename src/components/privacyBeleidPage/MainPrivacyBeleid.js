import React from 'react';

import '../../styles/privacyBeleid/privacyBeleid.css'

import ScrollToTop from '../../components/ScrollToTop'


export default function MainPrivacyBeleid() {
  let websiteName = 'VIVA MEMORIA';

  const arrow = '	https://lh3.googleusercontent.com/u/0/d/1VGSaJe0sbdUspoTad39utq7Nav7Up6ZE=w1280-h885-iv1';
  const privacyPic1 = '	https://lh3.googleusercontent.com/u/0/d/1LNUsVCccSJwTR90T_yy673ACIOWZwlmK=w2000-h2370-iv1';

  const messenger = 'https://lh3.googleusercontent.com/u/0/d/1WuVYQw9zJlTmQ-b23aufal7TAYd2379o=w1280-h885-iv1'
  const whatsapp = 'https://lh3.googleusercontent.com/u/0/d/12EG-4w1smPrisEFIX3heIY4Zu7vUXITe=w1280-h885-iv1'
  const email = 'https://lh3.googleusercontent.com/u/0/d/1pm7Vg9z3eLlXIE0uE_EUxF7VvqnDgbw6=w2000-h2370-iv1'

  return (
    <div class='privacy row col-12 justify-content-center'>
    {/* button redirect to the top */}
      <ScrollToTop />
      <h1 className='privacy-title text-center fw-bold'>Privacybeleid</h1>
      <div className='privacy-inhoud row justify-content-center'>
        <div className='titles col-12 col-lg-4 p-4 p-md-5'>
          <p className='title1'>Onze inzet voor privacy bij {websiteName}</p>
          <div className='column mb-3 d-flex'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle1'><p className='icon-text'>Privacybeleid {websiteName}</p></a>
          </div>
          
          <p className='title2'>Ons gebruik van verzamelde gegevens</p>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.1'><p className='icon-text'>Communicatie</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.2'><p className='icon-text'>Cookies</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.3'><p className='icon-text'>Doeleinden</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.4'><p className='icon-text'>Derden</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.5'><p className='icon-text'>Veranderingen</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.6'><p className='icon-text'>Keuzes voor persoonsgegevens</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.7'><p className='icon-text'>Aanpassen/uitschrijven dienst nieuwsbrief</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.8'><p className='icon-text'>Aanpassen/uitschrijven communicatie</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.9'><p className='icon-text'>Cookies uitzetten</p></a>
          </div>
          <div className='column mb-3'>
            <img src={arrow} alt='' className='arrow'/>
            <a href='#subtitle2.10'><p className='icon-text'>Vragen en feedback</p></a>
          </div>
        </div>
        <div className='description col-12 col-lg-7'>
          <div className='row justify-content-center'>
            <img src={privacyPic1} alt=''/>
          </div>

          <div className='p-2 p-md-5'>

            <section id='subtitle1' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p  className='fw-bold fs-3 '>Privacybeleid {websiteName}</p>
                          <p className='description-text'>Deze pagina is voor het laatst aangepast op 27/01/2021. Wij zijn er van bewust dat u vertrouwen stelt in ons. Wij zien het dan ook als onze  verantwoordelijkheid om uw privacy te beschermen. Op deze pagina laten  we u weten welke gegevens we verzamelen als u onze website gebruikt,  waarom we deze gegevens verzamelen en hoe we hiermee uw gebruikservaring  verbeteren. Zo snapt u precies hoe wij werken.</p>
                          <p className='description-text'>Dit privacybeleid is van toepassing op de diensten van {websiteName}. U dient zich ervan bewust te zijn dat {websiteName}  niet verantwoordelijk is voor het privacybeleid van andere sites en  bronnen. Door gebruik te maken van deze website geeft u aan het privacy  beleid te accepteren.</p>
                          <p className='description-text'>{websiteName} respecteert de privacy van  alle gebruikers van haar site en draagt er zorg voor dat de persoonlijke  informatie die u ons verschaft vertrouwelijk wordt behandeld.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.1' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Communicatie</p>
                          <p className='description-text'>Wanneer u e-mail of andere berichten naar ons verzendt, is het mogelijk dat we die berichten bewaren. Soms vragen wij u naar uw persoonlijke gegevens die voor de desbetreffende situatie relevant zijn. Dit maakt het mogelijk uw vragen te verwerken en uw verzoeken te beantwoorden. De gegevens worden opgeslagen op eigen beveiligde servers van {websiteName} of die van een derde partij. Wij zullen deze gegevens niet combineren met andere persoonlijke gegevens waarover wij beschikken.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.2' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Cookies</p>
                          <p className='description-text'>Wij verzamelen gegevens voor onderzoek om zo een beter inzicht te krijgen in onze klanten, zodat wij onze diensten hierop kunnen afstemmen. Deze website maakt gebruik van “cookies” (tekstbestandjes die op uw computer worden geplaatst) om de website te helpen analyseren hoe gebruikers de site gebruiken. De door het cookie gegenereerde informatie over uw gebruik van de website kan worden overgebracht naar eigen beveiligde servers van {websiteName} of die van een derde partij. Wij gebruiken deze informatie om bij te houden hoe u de website gebruikt, om rapporten over de website-activiteit op te stellen en andere diensten aan te bieden met betrekking tot website-activiteit en internetgebruik.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.3' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Doeleinden</p>
                          <p className='description-text'>We verzamelen of gebruiken geen informatie voor andere doeleinden dan de doeleinden die worden beschreven in dit privacybeleid tenzij we van tevoren uw toestemming hiervoor hebben verkregen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.4' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Derden</p>
                          <p className='description-text'>De informatie wordt niet met derden gedeeld met uitzondering van webapplicaties welke wij gebruiken ten behoeve van onze webwinkel. Hieronder valt o.a. het WebwinkelKeur beoordelingen systeem. Deze gegevens zullen enkel gebruikt worden ten behoeve van het doel van de betreffende applicatie en zullen niet verder verspreid worden. Verder kan in enkele gevallen de informatie intern gedeeld worden. Onze werknemers zijn verplicht om de vertrouwelijkheid van uw gegevens te respecteren.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.5' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Veranderingen</p>
                          <p className='description-text'>Deze privacyverklaring is afgestemd op het gebruik van en de mogelijkheden op deze site. Eventuele aanpassingen en/of veranderingen van deze site, kunnen leiden tot wijzigingen in deze privacyverklaring. Het is daarom raadzaam om regelmatig deze privacyverklaring te raadplegen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.6' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Keuzes voor persoonsgegevens</p>
                          <p className='description-text'>Wij bieden alle bezoekers de mogelijkheid tot het inzien, veranderen, of verwijderen van alle persoonlijke informatie die op moment aan ons is verstrekt.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <section id='subtitle2.7' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Aanpassen/uitschrijven dienst nieuwsbrief</p>
                          <p className='description-text'>Onderaan iedere mailing vindt u de mogelijkheid om uw gegevens aan te passen of om u af te melden.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>
            
            <section id='subtitle2.8' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Aanpassen/uitschrijven communicatie</p>
                          <p className='description-text mb-4'>Als u uw gegevens aan wilt passen of uzelf uit onze bestanden wilt laten halen, kunt u contact met ons op nemen. Zie onderstaande contactgegevens.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

            <div className='contact p-3 ms-3'>
                <div className='col-12 col-lg-3 p-0 d-inline mb-4'>
                  <img src={messenger} alt='' className='img-list'/>
                  <a href='http://m.me/VivaMemoriaOnline' rel='noreferrer' target='_blank'><p className='icon-text'>messenger</p></a>
                </div>
              
                <div className='col-12 col-lg-4 p-0 mb-4'>
                  <img src={whatsapp} alt='' className='img-list'/>
                  <a href='https://wa.me/32483446814?text=Ik heb een vraag:' rel='noreferrer' target='_blank'><p className='icon-text'>+32 483 44 68 14</p></a>
                </div>
              
                <div className='col-12 col-lg-5 p-0'>
                  <img src={email} alt='' className='img-list'/>
                  <a href='mailto:info@vivamemoria.be' rel='noreferrer' target='_blank'><p className='icon-text'>info@vivamemoria.be</p></a>
                </div>
              </div>

            <section id='subtitle2.9' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Cookies uitzetten</p>
                          <p className='description-text'>De meeste browsers zijn standaard ingesteld om cookies te accepteren, maar u kunt uw browser opnieuw instellen om alle cookies te weigeren of om aan te geven wanneer een cookie wordt verzonden. Het is echter mogelijk dat sommige functies en –services, op onze en andere websites, niet correct functioneren als cookies zijn uitgeschakeld in uw browser.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>
            
            <section id='subtitle2.10' class='subtitle-description'>
                <div class='row'>
                  <div class='mt-4'>
                    <div class='description-inner'>
                        <div class=''>
                          <p className='fw-bold fs-3'>Vragen en feedback</p>
                          <p className='description-text'>We controleren regelmatig of we aan dit privacybeleid voldoen. Als u vragen heeft over dit privacybeleid, kunt u contact met ons opnemen.</p>
                        </div>
                    </div>
                  </div>
                </div>
            </section>

          </div>
        </div>
      </div>
    </div>
  )
}
