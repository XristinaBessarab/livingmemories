import React from 'react'
import '../styles/Pagination.css'

const Pagination = ({cardsPerPage, totalCards, paginate}) => {

    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalCards / cardsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <div>
           <div className='pagination-container row'>
              { pageNumbers.map(number => (
                <div key={number} className='page-items col '>
                   <a href='#' className='page-links' onClick={() => paginate(number)}>
                    {number}
                   </a>
                </div>
              ))}
           </div> 
        </div>
    )
}

export default Pagination
