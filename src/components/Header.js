import React, { useState, useEffect } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import Fade from 'react-reveal/Fade';
import profile from "../images/logoAKapitalenGradient1.png";
import "../styles/header/header.scss";
import app from "../helpers/axiosConfig";
import { Link } from "react-router-dom";

export default function Header(props) {
  const [memorial, setMemorial] = useState([]);
  const [search, setSearch] = useState("");

  const handleChange = (e) => {
    const name = e.target.value.toLowerCase();

    let result = [];

    result = memorial.filter((data) => {
      if (name === "") {
        return setSearch(null);
      }
      return data.FirstName.toLowerCase().search(name) !== -1;
    });

    setSearch(result);
  };

  useEffect(() => {
    (async () => {
      const data = await app.get("/api/memorialpages");
      setMemorial(data.data);
    })();
  }, []);

  return (
    <header style={props.styling}>
      <div className="header_banner" style={{ height: `${props.height}` }}>

         

        <img  src={profile} className="profileImage" alt="profile" />



        <div className="search">
          <input
            type="text"
            className="input"
            onChange={handleChange}
            placeholder="Vind een herdenkingspagina of begrafenisondernemer"
          />
          <button type="submit">
            <AiOutlineSearch />
          </button>
        </div>
        <ul
          style={{
            width: '35%'
          }}
        >
          {search &&
            search.map((data) => {
              return (
                <Fade
                  duration={500}
                >
                  <Link
                    to={{ pathname: `/herdenkings/${data.Id}`, memorial: data.Id }}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    <li className="dropdown-content" key={data.Id}>
                        {data.FirstName} {data.LastName}
                    </li>
                  </Link>
                </Fade>
              );
            })}
        </ul>
      </div>
    </header>
  );
}
