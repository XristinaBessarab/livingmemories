import React from 'react'
import { Link } from "react-router-dom";

//Aside component which renders in the dashboard on the left
export default function Asside(props) {
  return (
    <div>
      <div className="aside-container ">
        <h5>Welkom {props.userData.FirstName && props.userData.FirstName}</h5>
        <ul>
          <li className='lees_meer_button'><Link  to="/mijn-paginas">  Pagina's</Link></li>
          <li className='lees_meer_button'><Link to="/mijn-herdenkingspaginas">Herdenkingspagina's</Link></li>
          <li className='lees_meer_button'><Link to="/">Abonnement</Link></li>
          <li className='lees_meer_button'><Link to="/">Factuur</Link></li>
        </ul>
      </div>
    </div>
  )
}
