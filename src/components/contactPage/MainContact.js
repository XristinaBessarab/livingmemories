import React from "react";
import "../../styles/contact/contact.scss";
import Mapp from "../map/Mapp";
import {FaTwitter, FaFacebook, FaAt, FaInstagramSquare, FaPhone } from 'react-icons/fa';

export default function MainContact() {
  //Props data for the map
  let props = {
    address: "Corneel van Reetstraat 17",
    linkToMaps:
      "https://www.google.com/maps/place/Corneel+van+Reethstraat+17%2Fbus+3,+2600+Antwerpen/@51.1981233,4.4364582,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3f73f862ec047:0xf3446998b3803c43!8m2!3d51.1981233!4d4.4386469",
    Latitude: 51.19829807734209,
    Longitude: 4.4387005426150905,
    Zoom: 16,
  };

  return (
    <div>
      <h3>Zou je graag de initiatiefnemers willen contacteren?</h3>
      <div className="contact-container">
        <div className="contact-form-container">
          <div className="contact-form-text-socials">
            <div className="contact-text">
              <p>
                Aarzel niet om ons te contacteren via mail of de onderstaande
                sociale mediakanalen.
              </p>

              <ul>
              <li> <a href="https://api.whatsapp.com/send?phone=32483446814" target='_blank' ><FaPhone fontSize={'25px'}/></a> </li>
                
              

                <li> <a href="https://www.twitter.com" target='_blank' ><FaTwitter fontSize={'25px'}/></a> </li>
                <li> <a href="https://www.facebook.com" target='_blank'> <FaFacebook fontSize={'25px'}/> </a> </li>
                <li>  <a href="#"> <FaAt fontSize={'25px'} /> </a>  </li>
                <li><a href="https://www.instagram.com" target='_blank'> <FaInstagramSquare fontSize={'25px'} /> </a> </li>
              </ul>


             
            </div>

            
          
          </div>
        </div>
        <div className="contact-map-container">
          <div className="map-container">
            <Mapp {...props} />
          </div>
        </div>
      </div>
    </div>
  );
}
