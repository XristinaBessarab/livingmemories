const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const helmet = require("helmet");
const path = require("path");
const db = require("./api/database/db");
const { checkJwt } = require("./api/authz/check-jwt");

const { clientOrigins } = require("./api/config/env.dev");
const { dbRouter } = require("./api/database/database.router");
const { throws } = require("assert");
const app = express();
const apiRouter = express.Router();

require("dotenv").config({
  path: path.resolve(__dirname, "./api/.env"),
});

const PORT = process.env.PORT || process.env.SERVER_PORT || "3000";
//Check whether the environment is set in prod or dev
const inProduction = process.env.NODE_ENV === "production";

/**
 *  App Configuration
 */

// express will serve up production assets
app.use(express.static(path.join(__dirname, "build")));
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);
app.use(
  cors({
    origin:
      process.env.NODE_ENV === "production"
        ? process.env.SITE_URL
        : clientOrigins,
  })
);
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routers
app.use("/api", apiRouter);
apiRouter.use("/db", dbRouter);

// in development, webpack-dev-server (which the create-react-app hides)
// will serve assets, in production express will now serve both our client-side
// "build" assets AND the API
// express will serve up the front-end index.html file if it doesn't recognize the route
// app.get('*') needs to be the last GET route and before the first POST route!

//Get all non deleted users
app.get("/api/get", (req, res) => {
  const sqlSelect = "SELECT * FROM `Users` WHERE `DeletionReason` IS NULL";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

// User find Auth_Id (Doesn't work correctly)
app.get("/get/:Auth_Id", checkJwt, (req, res) => {
  var auth_Id = req.params.Auth_Id;

  const sqlSelect = "SELECT * FROM `Users` WHERE `Auth_Id` = " + auth_Id;
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

// User find Auth_Id
app.get("/get/auth/:Auth_Id", (req, res) => {
  db.query(
    "SELECT * FROM `Users` WHERE `Auth_Id` = ? ",
    [req.params.Auth_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

// User find with Id
app.get("/get/userdata/:Id", (req, res) => {
  var Id = req.params.Id;

  const sqlSelect = "SELECT * FROM `Users` WHERE `Id` = " + Id;
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

// Buyer find with Id
app.get("/get/buyerdata/:Id", (req, res) => {
  var Id = req.params.Id;

  const query = "SELECT * FROM `Buyers` WHERE `Users_Id` = " + Id;
  db.query(query, (err, result) => {
    res.send(result);
  });
});

//Get MemorialPages
app.get("/api/memorialpages", (req, res) => {
  db.query("SELECT * FROM `MemorialPages`", (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log(err);
    }
  });
});

//Get Memorialpage with id
app.get("/api/memorialpages/:Id", (req, res) => {
  db.query(
    "SELECT * FROM `MemorialPages` WHERE `Id` = " + req.params.Id,
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//JOIN querie to see all memorialpages of user
app.get("/api/memorialpagesofuser/:Id", (req, res) => {
  db.query(
    "SELECT DISTINCT MemorialPages.Id, MemorialPages.FirstName, MemorialPages.LastName, MemorialPages.Obituary, MemorialPages.BirthDate, MemorialPages.DateOfDeath , MemorialPages.Quote, MemorialPages.QuoteAuthor, MemorialPages.IntroText, MemorialPages.Latitude, MemorialPages.Longitude, MemorialPages.Undertakers_Id FROM MemorialPages INNER JOIN Admin_has_MemorialPages ON MemorialPages.Id = Admin_has_MemorialPages.MemorialPages_Id INNER JOIN Admin ON Admin_has_MemorialPages.Admin_Id = Admin.Id INNER JOIN Users ON Admin.Users_Id = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get All undertakers
app.get("/api/all-undertakers", (req, res) => {
  db.query("SELECT * FROM `Undertakers`", (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log(err);
    }
  });
});

//Get undertaker with Name
app.get("/api/undertaker-with-name/:Name", (req, res) => {
  db.query(
    "SELECT * FROM `Undertakers` WHERE `Name` = ? ",
    [req.params.Name],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get undertaker with Id
app.get("/api/undertaker-with-id/:Id", (req, res) => {
  db.query(
    "SELECT * FROM `Undertakers` WHERE `Id` = ? ",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get admintable id with user_id
app.get("/api/admintable-with-id/:Users_Id", (req, res) => {
  db.query(
    "SELECT * FROM `Admin` WHERE `Users_Id` = " + req.params.Users_Id,
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

// Admin table find with user_id and right to act to get id of admin table after
app.get("/get/admin-table-with-id/:Users_Id/:RightToAct", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT `Id` FROM `Admin` WHERE `Users_Id` = ? AND `RightToAct` = ? ",
    [params.Users_Id, params.RightToAct],
    (err, result) => {
      res.send(result);
    }
  );
});

//Get Memorialpage with username, firstname, birthData, DateOfDeath after the first form to get id
app.get(
  "/api/memorialpage-name/:FirstName/:LastName/:BirthDate/:DateOfDeath",
  (req, res) => {
    const params = req.params;
    db.query(
      "SELECT `Id`FROM `MemorialPages` WHERE  `FirstName` = ? AND `LastName` = ? AND `BirthDate` = ? AND `DateOfDeath` = ? ",
      [params.FirstName, params.LastName, params.BirthDate, params.DateOfDeath],
      (err, result) => {
        if (!err) {
          res.send(result);
        } else {
          console.log(err);
        }
      }
    );
  }
);

//Get all visualContent
app.get("/api/visualcontent", (req, res) => {
  const params = req.params;
  db.query("SELECT * FROM `VisualContent`", (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log(err);
    }
  });
});

//Get imageUrl of memorialpage profile with Id
app.get("/api/visualcontent/:MemorialPages_Id", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT * FROM `VisualContent` WHERE  `MemorialPages_Id` = ? ",
    [params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get profile visualcontent with memorialpage id
app.get("/api/visualcontent-profile/:MemorialPages_Id", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT * FROM `VisualContent` WHERE `MemorialPages_Id` = ? and Route = 'Profile'",
    [params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get obituary visualcontent with memorialpage id
app.get("/api/visualcontent-obituary/:MemorialPages_Id", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT * FROM `VisualContent` WHERE `MemorialPages_Id` = ? and Route = 'Obituary'",
    [params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get imageUrl of all memorialpage with user_id
app.get("/api/visualcontent-with-user/:Id", (req, res) => {
  db.query(
    "SELECT DISTINCT VisualContent.Name, VisualContent.Source, VisualContent.Route, VisualContent.MemorialPages_Id FROM VisualContent INNER JOIN MemorialPages ON VisualContent.MemorialPages_Id = MemorialPages.Id INNER JOIN Admin_has_MemorialPages ON MemorialPages.Id = Admin_has_MemorialPages.MemorialPages_Id INNER JOIN Admin ON Admin_has_MemorialPages.Admin_Id = Admin.Id INNER JOIN Users ON Admin.Users_Id = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//COntrol if there is an image in databse for that memorialpage
app.get("/api/visualcontent/:MemorialPages_Id/:Route", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT * FROM `VisualContent` WHERE  `MemorialPages_Id` = ? AND `Route` = ?",
    [params.MemorialPages_Id, params.Route],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Check if there is an image in the database for that contribution with contribution id
app.get("/api/visualcontent-contribution/:Contribution_Id", (req, res) => {
  const params = req.params;
  db.query(
    "SELECT * FROM `VisualContent` WHERE  `Contribution_Id` = ? AND `Route` = 'Contributi'",
    [params.Contribution_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get unique contribution with contribution id
app.get("/api/unique-contribution/:Id", (req, res) => {
  db.query(
    "SELECT * FROM `Contribution` WHERE `Id` = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
})

//Get all contrubitions of memorialpage with Id
app.get("/api/contribution/:MemorialPages_Id", (req, res) => {
  db.query(
    "SELECT * FROM `Contribution` WHERE  `MemorialPages_Id` = ?",
    [req.params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get Contribution id with inner join
app.get("/api/contribution-data/:MemorialPages_Id/:Description/:Nickname", (req, res) => {
  db.query(
    "SELECT DISTINCT Contribution.Id, Contribution.Description, Contribution.Nickname, Contribution.Relation, Contribution.CreatedAt, Contribution.MemorialPages_Id FROM Contribution WHERE Contribution.MemorialPages_Id = ? AND Contribution.Description = ? AND Contribution.Nickname = ?",
    [req.params.MemorialPages_Id, req.params.Description,  req.params.Nickname],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get user has contribution with contribution id
app.get("/api/user-contribution/:Contribution_Id", (req, res) =>{
  db.query(
    "SELECT * FROM `Users_has_Contribution` WHERE `Contribution_Id` = ?",
    [req.params.Contribution_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get user has contribution with memorialpage id
app.get("/api/user-contribution-m/:MemorialPages_Id", (req, res) =>{
  db.query(
    "SELECT * FROM `Users_has_Contribution` WHERE `MemorialPages_Id` = ?",
    [req.params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get user has contribution with memorialpage id and user id
app.get("/api/user-contribution-mId-uId/:MemorialPages_Id/:Users_Id", (req, res) =>{
  db.query(
    "SELECT * FROM `Users_has_Contribution` WHERE `MemorialPages_Id` = ? AND `Users_Id` = ?",
    [req.params.MemorialPages_Id, req.params.Users_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get userid of admin from specific memorialpage using inner joins
app.get("/api/userid-of-all-admins/:MemorialPages_Id", (req, res) =>{
  db.query(
    "SELECT Users.Id, Admin.Id as AdminId, Users.FirstName, Users.LastName FROM `Admin_has_MemorialPages` INNER JOIN Admin ON Admin_has_MemorialPages.Admin_Id = Admin.Id INNER JOIN Users on Admin.Users_Id = Users.Id WHERE `MemorialPages_Id` = ?",
    [req.params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
});

//Get admin has memorialpage with  admin id and memorialpage id.
app.get("/api/admin-has-memorialpage/:Admin_Id/:MemorialPages_Id", (req, res) =>{
  db.query(
    "SELECT * FROM `Admin_has_MemorialPages` WHERE `Admin_Id` = ? AND `MemorialPages_Id` = ?",
    [req.params.Admin_Id, req.params.MemorialPages_Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  );
})

//Get user information with contribution id using inner joins
app.get("/api/user-from-contribution/:Id", (req, res) =>{
  db.query(
    "SELECT Users.FirstName, Users.LastName FROM `Contribution` INNER JOIN Users_has_Contribution on Contribution.Id = Users_has_Contribution.Contribution_Id INNER JOIN Users on Users_has_Contribution.Users_Id = Users.Id WHERE Contribution.Id = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  )
})

//Get user AuthId  with contribution id using inner joins
app.get("/api/user-authId-from-contribution/:Id", (req, res) =>{
  db.query(
    "SELECT Users.Auth_Id FROM `Contribution` INNER JOIN Users_has_Contribution on Users_has_Contribution.Contribution_Id = Contribution.Id INNER JOIN Users on Users.Id = Users_has_Contribution.Users_Id WHERE Contribution.Id = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  )
})

//Get all contributions from a single user
app.get("/api/all-user-contributions/:Id", (req, res) =>{
  db.query(
    "SELECT * FROM `Users_has_Contribution` INNER JOIN Contribution on Users_has_Contribution.Contribution_Id = Contribution.Id WHERE Users_Id = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  )
})

//Get all memorialpages with the same undertaker
app.get("/api/memorialpages-undertaker/:Id", (req, res) =>{
  db.query(
    "SELECT * FROM `MemorialPages` WHERE `Undertakers_Id` = ?",
    [req.params.Id],
    (err, result) => {
      if (!err) {
        res.send(result);
      } else {
        console.log(err);
      }
    }
  )
})

//--POST--//

//POST user data
app.post("/post", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `Users` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST user buyer data
app.post("/post/buyer", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `Buyers` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST making memorialpage
app.post("/post/form-memorialpage", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `MemorialPages` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST making admin table
app.post("/post/form-adminpage", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `Admin` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST making undertakers table
app.post("/post/form-undertaker", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `Undertakers` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST making admin has memorialpage table
app.post("/post/admin-has-memorialpage", (req, res) => {
  const params = req.body;

  db.query(
    "INSERT INTO `Admin_has_MemorialPages` SET ?",
    params,
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//POST making visuacontent table for memorialpage images
app.post("/post/visualcontent-memorial", (req, res) => {
  const params = req.body;
  db.query("INSERT INTO `VisualContent` SET ? ", params, (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log(err);
    }
  });
});

//POST making Contribution
app.post("/post/form-contribution", (req, res) => {
  const params = req.body;

  db.query("INSERT INTO `Contribution` SET ?", params, (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//POST making users has contributuion table
app.post("/post/users-has-contributions", (req, res) => {
  const params = req.body;

  db.query(
    "INSERT INTO `Users_has_Contribution` SET ?",
    params,
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//--PUT--//

// UPDATE Memorial-Page with Id
app.put("/api/update-memorialpage-with-id/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `MemorialPages` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(params);
      } else {
        console.log(err);
      }
    }
  );
});

//Updating Memorial-Page
app.put("/api/update-memorialpages", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `MemorialPages` SET ? WHERE `FirstName` = ? AND `LastName` = ? AND `BirthDate` = ? AND `DateOfDeath` = ? ",
    [
      params,
      params.FirstName,
      params.LastName,
      params.BirthDate,
      params.DateOfDeath,
    ],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Updating Admin table
app.put("/api/update-admin/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `Admin` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Updating Admin table
app.put("/api/update-undertaker/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `Undertakers` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(req.body);
      } else {
        console.log(err);
      }
    }
  );
});

// UPDATE user data
app.put("/put/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `Users` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(req.body);
      } else {
        console.log(err);
      }
    }
  );
});

// UPDATE buyers data
app.put("/put/invoice/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `Buyers` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(req.body);
      } else {
        console.log(err);
      }
    }
  );
});

// UPDATE visualcontent with Id
app.put("/put/updatevisualcontent-Id/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `VisualContent` SET ? WHERE Id = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// UPDATE visualcontent with Contribution Id
app.put("/put/updatevisualcontent-Contribution-Id/:Contribution_Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `VisualContent` SET ? WHERE Contribution_Id = ? ",
    [params, params.Contribution_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Update contribution with contribution id
app.put("/put/update-contribution/:Id", (req, res) => {
  const params = req.body;

  db.query(
    "UPDATE `Contribution` SET ? WHERE `Id` = ? ",
    [params, params.Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//--DELETE--//

// Delete user from admin with Id
app.delete("/delete/admin/:Id", (req, res) => {
  db.query("DELETE FROM `Admin` WHERE Id = ?", [req.params.Id], (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//Delete memorialpage with Id
app.delete("/delete/memorialpages/:Id", (req, res) => {
  db.query(
    "DELETE FROM `MemorialPages` WHERE Id = ?",
    [req.params.Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete admin has memorialpages
app.delete(
  "/delete/admin-has-memorialpage-delete/:Admin_Id/:MemorialPages_Id",
  (req, res) => {
    db.query(
      "DELETE FROM `Admin_has_MemorialPages` WHERE `Admin_has_MemorialPages`.`Admin_Id` = ? AND `Admin_has_MemorialPages`.`MemorialPages_Id` = ?",
      [req.params.Admin_Id, req.params.MemorialPages_Id],
      (err, rows) => {
        if (!err) {
          res.send(rows);
        } else {
          console.log(err);
        }
      }
    );
  }
);

//Delete admin has memorialpages with only memorialpageId
app.delete(
  "/delete/admin-has-memorialpage-delete-only-memorialpageid/:MemorialPages_Id",
  (req, res) => {
    db.query(
      "DELETE FROM `Admin_has_MemorialPages` WHERE `Admin_has_MemorialPages`.`MemorialPages_Id` = ?",
      [req.params.MemorialPages_Id],
      (err, rows) => {
        if (!err) {
          res.send(rows);
        } else {
          console.log(err);
        }
      }
    );
  }
);

//Delete visualcontent of memorialpage
app.delete("/delete/visualcontent-delete/:MemorialPages_Id", (req, res) => {
  db.query(
    "DELETE FROM `VisualContent` WHERE MemorialPages_Id = ?",
    [req.params.MemorialPages_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete all Users_has_Contribution for page
app.delete("/delete/contribution-delete/:MemorialPages_Id", (req, res) => {
  db.query(
    "DELETE FROM `Contribution` WHERE MemorialPages_Id = ?",
    [req.params.MemorialPages_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete Users_has_Contribution of Users_has_Contribution table
app.delete("/delete/contribution-has-contribution-table/:MemorialPages_Id/:Users_Id", (req, res) => {
  db.query(
    "DELETE FROM `Users_has_Contribution` WHERE MemorialPages_Id = ? AND Users_Id = ?",
    [req.params.MemorialPages_Id, req.params.Users_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete contribution  with id
app.delete("/delete/contribution/:Id", (req, res) => {
  db.query(
    "DELETE FROM `Contribution` WHERE `Id` = ?",
    [req.params.Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete user has contribution with contribution id
app.delete("/delete/user-has-contribution/:Contribution_Id", (req, res) => {
  db.query(
    "DELETE FROM `Users_has_Contribution` WHERE `Contribution_Id` = ?",
    [req.params.Contribution_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//Delete visualcontent with contribution id
app.delete("/delete/visualcontent/:Contribution_Id", (req, res) =>{
  db.query(
    "DELETE FROM `VisualContent` WHERE `Contribution_Id` = ?",
    [req.params.Contribution_Id],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});


const distFolder = path.join(process.cwd(), "./build");
const indexHtml = path.join(distFolder, "index.html");

app.get(
  "*.*",
  express.static(distFolder, {
    maxAge: "1y",
  })
);

app.get("*", (req, res) => {
  res.sendFile(indexHtml);
});

//Server listening
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
